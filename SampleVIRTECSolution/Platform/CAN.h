

// Declare a Queue for Tx packets
typedef struct MyCANQueueEntry_S
{
   CAN_Packet_T       Packet;
   ISOBUS_Callback_T  Callback;
} MyCANQueueEntry_T;


struct  CAN_S
{
   // TODO: add network configuration
   Network_T    Network;
   Queue_T      TxQueue;
};


#define MAKE_CAN_S(name_table_entries, send_packet_fn, tx_queue_array, priority)  \
   {\
      {\
         MAKE_Network_PacketHandlerList_T(priority),\
         MAKE_NameTable_T(name_table_entries, Solution_SwTimerList, priority),\
         send_packet_fn\
      },\
      MAKE_Queue_T(tx_queue_array, priority)\
   }

#define MAKE_CAN(name, name_table_size, tx_queue_size, priority) \
   extern struct CAN_S name;\
   static bool_t name##_SendPacket(const CAN_Packet_T *packet, const ISOBUS_Callback_T *callback)\
   {\
      return CAN_SendPacket(packet, callback, &name);\
   }\
   static NameTableEntry_T  name##_NameTableEntries[name_table_size];\
   static MAKE_QUEUE_ARRAY(name##_TxQueueArray, MyCANQueueEntry_T, tx_queue_size);\
   struct CAN_S  name = MAKE_CAN_S(name##_NameTableEntries, name##_SendPacket, name##_TxQueueArray, priority);



extern void CAN_Init(struct CAN_S *can_channel);
extern void CAN_Task(struct CAN_S *can_channel);
extern void CAN_Uninit(struct CAN_S *can_channel);
//extern void CAN_ISR(struct CAN_S *can_channel);

//! Send (or queue to send) a packet on CAN1
extern bool_t CAN_SendPacket(const CAN_Packet_T *packet, const ISOBUS_Callback_T *callback, struct CAN_S *can_channel);
