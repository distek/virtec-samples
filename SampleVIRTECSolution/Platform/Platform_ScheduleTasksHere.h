/******************************************************************************/
/*
   \file
      Schedule the tasks for Platform

   \copyright
      Copyright (C) 2016  DISTek Integration, Inc.  All Rights Reserved.

   \author
      Chris Thatcher
*/
/******************************************************************************/


#ifndef INIT
//! Used for scheduling a function to run before any TASK
#define INIT(fn)
#define UNDEF_INIT
#endif //INIT

#ifndef TASK
//! Used for scheduling a function to execute periodically
#define TASK(fn,period,priority)
#define UNDEF_TASK
#endif //TASK

#ifndef EXIT
//! Used for scheduling tasks to run at shutdown
#define EXIT(fn)
#define UNDEF_EXIT
#endif //EXIT


/******************************************************************************/
// BEGIN:  Task Scheduling
/******************************************************************************/
//! \cond  SKIP

//   Init function
INIT(Display_Init)
INIT(Memory_Init)
INIT(CAN_Init)

//   Task function,  period (ms),  priority
TASK(Display_Task,            10,      PL_0)  // Background task for printing to the screen
TASK(Memory_Task,             10,      PL_5)
//TASK(CAN_RxISR,               INFINITE, PL_14)  // This one never actually returns
TASK(CAN_Task,                1,      PL_2)

//   Exit function
EXIT(CAN_Uninit)

//! \endcond
/******************************************************************************/
// END:    Task Scheduling
/******************************************************************************/


#ifdef UNDEF_INIT
#undef INIT
#undef UNDEF_LIBRARY_INIT
#endif
#ifdef UNDEF_TASK
#undef TASK
#undef UNDEF_LIBRARY_TASK
#endif
#ifdef UNDEF_EXIT
#undef EXIT
#undef UNDEF_LIBRARY_EXIT
#endif


/******************************************************************************/
/*
   Copyright (C) 2016  DISTek Integration, Inc.  All Rights Reserved.

   Developed by:
      DISTek(R) Integration, Inc.
      6612 Chancellor Drive Suite 600
      Cedar Falls, IA 50613
      Tel: 319-859-3600
*/
/******************************************************************************/
