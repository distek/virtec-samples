/******************************************************************************/
/*!
   \file
      Task Manager interface

   \copyright
      Copyright (C) 2012  DISTek Integration, Inc.  All Rights Reserved.

   \author
      Chris Thatcher
*/
/******************************************************************************/


//! Enumeration of scheduled tasks
typedef enum
{
#define TASK(fn,period,priority)   fn##_enum,
#include "ScheduleTasksHere.h"
#undef TASK
   TASK_LASTTASK
} TaskName_T;


//! Returns enum value representing the currently running task
extern TaskName_T TaskScheduler_GetCurrentTaskName(void);


//! Indicates that the program should close
extern bool_t  ExitTriggered;
//! Indicates that the program should close
extern bool_t  PowerDown;
//! Boolean value indicating whether to use AC power state as Switched Power indicator
extern bool_t UseSwitchedPower;
//! Returns whether or not switched power is currently on
extern bool_t SwitchedPower_IsOn(void);

//! Initialize the solution
extern void TaskScheduler_Init(void);


/******************************************************************************/
/*
   Copyright (C) 2012  DISTek Integration, Inc.  All Rights Reserved.

   Developed by:
      DISTek(R) Integration, Inc.
      6612 Chancellor Drive Suite 600
      Cedar Falls, IA 50613
      Tel: 319-859-3600
*/
/******************************************************************************/
