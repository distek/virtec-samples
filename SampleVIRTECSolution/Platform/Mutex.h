/******************************************************************************/
/*!
   \file
      This file defines the priority interface used for Win32

   \copyright
      Copyright (C) 2012  DISTek Integration, Inc.  All Rights Reserved.

   \author
      Chris Thatcher
*/
/******************************************************************************/


//!  Storage class for task Priority
typedef int Priority_T;

// Priority Level Macros used for scheduling tasks:
//! Priority Level 0 (lowest priority)
#define  PL_0   (-15)
//! Priority Level 1
#define  PL_1    (-7)
//! Priority Level 2
#define  PL_2    (-6)
//! Priority Level 3
#define  PL_3    (-5)
//! Priority Level 4
#define  PL_4    (-4)
//! Priority Level 5
#define  PL_5    (-3)
//! Priority Level 6
#define  PL_6    (-2)
//! Priority Level 7
#define  PL_7    (-1)
//! Priority Level 8
#define  PL_8     (0)
//! Priority Level 9
#define  PL_9     (1)
//! Priority Level 10
#define  PL_10    (2)
//! Priority Level 11
#define  PL_11    (3)
//! Priority Level 12
#define  PL_12    (4)
//! Priority Level 13
#define  PL_13    (5)
//! Priority Level 14 (highest priority)
#define  PL_14    (6)

//! Lowest priority level
#define PRIORITY_MIN  PL_0
//! Highest priority level
#define PRIORITY_MAX  PL_14


//! Generic structure for implementing Mutexes
typedef struct Mutex_S
{
   //! Indicates whether or not this mutex is currently locked
   bool_t      Locked;
   //! Enumeration value indicating which thread has the mutex
   TaskName_T  LockedBy;
   //! WIN32 Mutex handle
   HANDLE      Mutex;
   //! Storage for base priority of task that grabs mutex
   Priority_T  Base;
   //! Ceiling priority of tasks that access this mutex
   Priority_T  Ceiling;
} Mutex_T;


/******************************************************************************/
/*!
   \brief    Macro used to initialize a Mutex_T

   \details  This macro is used to create a Mutex_T.

   \param    priority  Priority of highest priority task that accesses this
                       structure
*/
/******************************************************************************/
#define MAKE_Mutex_T(priority)  {FALSE, TASK_LASTTASK, NULL, PL_0, priority}


//! Initialize the priority class of the process
extern void Mutex_Init(Mutex_T *mutex);

//! Initialize the Mutex using information from another Mutex
extern void Mutex_CopyInit(Mutex_T *mutex_to_init, const Mutex_T *copy_from_mutex);

//! Increase the priority of the current task to the ceiling (and returns the base priority)
extern void Mutex_Get(Mutex_T *mutex);

//! Restore the current task to the base priority
extern void Mutex_Release(Mutex_T *mutex);


/******************************************************************************/
/*
   Copyright (C) 2012  DISTek Integration, Inc.  All Rights Reserved.

   Developed by:
      DISTek(R) Integration, Inc.
      6612 Chancellor Drive Suite 600
      Cedar Falls, IA 50613
      Tel: 319-859-3600
*/
/******************************************************************************/
