/******************************************************************************/
/*!
   \file
      This file defines the non-blocking interface for writing to Standard Output.

   \copyright
      Copyright (C) 2012  DISTek Integration, Inc.  All Rights Reserved.

   \author
      Chris Thatcher
*/
/******************************************************************************/


//! Pipe to write to STDOUT in the background
extern Pipe_WriteHandle_T  StandardOutput;

//! Pipe to write to STDERR in the background
extern Pipe_WriteHandle_T  StandardError;


//! Used to create c-strings for insertion into StandardOutput
#ifndef _LIB
extern int sprintf(char*, const char*, ...);
#else
#include <stdio.h>
#endif // _LIB

//! Initialize the StandardOutput pipe
extern void Display_Init(void);

//! Display data written to StandardOutput
extern void Display_Task(void);


/******************************************************************************/
/*
   Copyright (C) 2012  DISTek Integration, Inc.  All Rights Reserved.

   Developed by:
      DISTek(R) Integration, Inc.
      6612 Chancellor Drive Suite 600
      Cedar Falls, IA 50613
      Tel: 319-859-3600
*/
/******************************************************************************/
