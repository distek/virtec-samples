/******************************************************************************/
/*
NOTES: Initialize, task, interrupt handler, and queue data packet functions for the CAN
network.
*/
/******************************************************************************/

#include "Platform.h"
#include "Foundation.h"
#include "CAN.h"
#include "Solution.h"
#include <stdio.h>
#include <winsock.h>

#define MULTICAST

#define CAN_GROUP "239.0.0.223"
#define CAN_PORT 25000
#define MSGBUFSIZE 16

SOCKET conn;
struct sockaddr_in addr;

// Declare everything needed for the Network Instance
static void CAN_RxISR(struct CAN_S *can_channel);
static void InitUdpMulticast();
static bool_t SendMsg(CAN_Packet_T *packet);
static void PrintPacket(const CAN_Packet_T *packet, uint8_t bus_num);

struct CAN_S CAN1;

extern struct CAN_S  CAN1;

DWORD start_time;

/******************************************************************************/
/*!
   \brief   Initializes CAN
*/
/******************************************************************************/
void CAN_Init(struct CAN_S *can_channel)
{
	start_time = GetTickCount();

	// Initialize the Network for CAN1
	//Network_Init(&can_channel->Network);
	Network_Init(&CAN1.Network);

	// Initialize the TX Queue for CAN1
	//Queue_Init(&can_channel->TxQueue);
	Queue_Init(&CAN1.TxQueue);

	InitUdpMulticast();
}

static void InitUdpMulticast(void)
{
	printf("Initializing network...");

	struct ip_mreq mreq;
	WSADATA wsa;

	u_int yes = 1; 
	u_long mode = 1;

	if (WSAStartup(MAKEWORD(2, 2), &wsa) != 0)
	{
		printf("Failed. Error Code : %d", WSAGetLastError());
		exit(EXIT_FAILURE);
	}

	/* create what looks like an ordinary UDP socket */
	if ((conn = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0) {
		perror("socket");
		exit(1);
	}
	
	/* set socket to non-blocking mode */
	if (ioctlsocket(conn, FIONBIO, &mode) != 0) {
		perror("ioctlsocket failed");
		exit(1);
	}

	/* allow multiple sockets to use the same PORT number */
	if (setsockopt(conn, SOL_SOCKET, SO_REUSEADDR, (const char*)&yes, sizeof(yes)) < 0) {
		perror("Reusing ADDR failed");
		exit(1);
	}

	/* set up destination address */
	memset(&addr, 0, sizeof(addr));
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = htonl(INADDR_ANY); /* N.B.: differs from sender */
	addr.sin_port = htons(CAN_PORT);

	/* bind to receive address */
	if (bind(conn, (struct sockaddr *) &addr, sizeof(addr)) < 0) {
		perror("bind");
		exit(1);
	}

	/* use setsockopt() to request that the kernel join a multicast group */
	mreq.imr_multiaddr.s_addr = inet_addr(CAN_GROUP);
	mreq.imr_interface.s_addr = htonl(INADDR_ANY);
	if (setsockopt(conn, IPPROTO_IP, IP_ADD_MEMBERSHIP, (char*)&mreq, sizeof(mreq)) < 0) {
		perror("setsockopt");
		printf("Error - failed to join multicast group: %d\n", WSAGetLastError());
		exit(1);
	}

	// for some reason the first few sends don't seem to work, but this seems to help.
	// maybe we need to wait for an incoming message before we start transmitting?
	CAN_Packet_T p;
	CAN_PacketHeader_T header;
	header.Direction = ISOBUS_TX;
	header.Identifier = 0x18FFFFFF;
	p.Header = header;
	p.DLC = 0;
	//p.Data = { 0x00 };
	for (int i = 0; i < 10; i++)
	{
		SendMsg(&p);
		Sleep(500);
	}

	printf("OK\n");
}

/******************************************************************************/
/*!
   \brief   CAN task
*/
/******************************************************************************/
void CAN_Task(struct CAN_S *can_channel)
{
	MyCANQueueEntry_T  entry;

	// Run the Network task (for CAN1)
	//Network_Task(&can_channel->Network);
	Network_Task(&CAN1.Network);

	// Process received packets
	//CAN_RxISR(can_channel);
	CAN_RxISR(&CAN1);

	// While there are more entries in the TX queue
	//while(Queue_CopyData(&can_channel->TxQueue, &entry, sizeof(MyCANQueueEntry_T)))
	while (Queue_CopyData(&CAN1.TxQueue, &entry, sizeof(MyCANQueueEntry_T)))
	{
		// Convert entry->Packet to hardware type
		if (SendMsg(&entry.Packet))
		{
			// Call the callback
			if (entry.Callback.Function != NULL)
			{
				entry.Callback.Function(&entry.Callback);
			}

			// Pass the TX message back through the handlers
			//Network_PacketHandler(&entry.Packet, &can_channel->Network);
			Network_PacketHandler(&entry.Packet, &CAN1.Network);

			// Remove the TX packet from the queue (since Queue_GetFront worked, we know there's an entry)
			//(void)Queue_Remove(&can_channel->TxQueue, sizeof(MyCANQueueEntry_T));
			(void)Queue_Remove(&CAN1.TxQueue, sizeof(MyCANQueueEntry_T));
		}
		else // send to network failed
		{
			// Quit processing the TX queue (try again later)
			break;
		}
	}
}

static bool_t SendMsg(CAN_Packet_T *packet)
{
	int ret;
	char szMessage[32] = { 0x00 };
	int iMessageLen = 0;
	uint8_t i;

	packet->Header.Direction = ISOBUS_TX;
	szMessage[0] = (uint8_t)((packet->Header.Identifier & 0xFF000000) >> 24);
	szMessage[1] = (uint8_t)((packet->Header.Identifier & 0x00FF0000) >> 16);
	szMessage[2] = (uint8_t)((packet->Header.Identifier & 0x0000FF00) >> 8);
	szMessage[3] = (uint8_t)((packet->Header.Identifier & 0x000000FF));
	szMessage[4] = (uint8_t)packet->DLC;
	for (i = 0; i < packet->DLC; i++)
		szMessage[5 + i] = packet->Data[i];
	iMessageLen = i + 5;

	//ret = sendto(conn, szMessage, iMessageLen, 0, (struct sockaddr *) &send_addr, sizeof(send_addr));
	ret = sendto(conn, szMessage, iMessageLen, 0, (struct sockaddr *) &addr, sizeof(addr));
	PrintPacket(packet, 1);

	if (ret == SOCKET_ERROR)
	{
		printf("Error\nCall to sendto(s, szMessage, iMessageLen, 0, (SOCKADDR_IN *) &addr, sizeof(addr)); failed with:\n%d\n", WSAGetLastError());
		// This is kind of a hack that might cause confusion - if there are no other clients sendto returns 
		// an error, but we don't necessarily care.  we send address claim messages even if no other clients
		// are connected to the bus, and if we return false and dont' remove the message from the queue
		// we get stuck in an infinite loop
		// note that this still doesn't exactly work - if a client starts up and we tell it that it's address
		// claimed message was sent correctly, foundation thinks it claimed an address already.  it the server
		// asks for address claims, the client says it can't claim an address because it already did.  i don't
		// think that sounds correct, but that looks like what's happening
		//return FALSE;
		return TRUE;
	}
	// if we sent the whole message that means it worked
	return ret == iMessageLen;
}

/******************************************************************************/
/*!
   \brief   Initializes CAN
*/
/******************************************************************************/
void CAN_Uninit(struct CAN_S *can_channel)
{
	// Open the handle to Eaton CAN channel
	//capi_can_close(can_channel->can_handle);
}

/******************************************************************************/
/*!
   \brief   Queues CAN1 Tx packets to be sent on bus

   \param   packet      Packet to be queued for transmission
   \param   callback    Callback when packet is sent
   \param   can_channel pointer to CAN_S struct representing CAN bus

   \return  bool_t

   \retval  TRUE     Packet was successfully sent
   \retval  FALSE    Packet failed to send
*/
/******************************************************************************/
bool_t CAN_SendPacket(const CAN_Packet_T *packet, const ISOBUS_Callback_T *callback, struct CAN_S *can_channel)
{
	MyCANQueueEntry_T  queue_entry;

	// Consolidate packet and callback into queue entry
	Utility_MemoryCopy(&queue_entry.Packet, packet, sizeof(CAN_Packet_T));
	Utility_MemoryCopy(&queue_entry.Callback, callback, sizeof(ISOBUS_Callback_T));

	// Insert queue entry into queue
	//return Queue_Insert(&can_channel->TxQueue, &queue_entry, sizeof(MyCANQueueEntry_T));
	return Queue_Insert(&CAN1.TxQueue, &queue_entry, sizeof(MyCANQueueEntry_T));
}

/******************************************************************************/
/*!
\brief   Handles receive interrupt
*/
/******************************************************************************/
static void CAN_RxISR(struct CAN_S *can_channel)
{
	int ret;
	char szMessage[512];
	int iMessageLen = 512;
	uint8_t i;
	CAN_Packet_T  packet;
	int addrlen;

	addrlen = sizeof(addr);
	ret = recvfrom(conn, szMessage, iMessageLen, 0, (struct sockaddr *) &addr, &addrlen);
	if (ret > 0)
	{
		iMessageLen = 512;

		if (ret == SOCKET_ERROR)
		{
			printf("Error\nCall to recvfrom(s, szMessage, iMessageLen, 0, (struct sockaddr *) &remote_addr, &iRemoteAddrLen); failed with:\n%d\n", WSAGetLastError());
		}
		else
		{
			iMessageLen = ret;		// Length of the data received

			packet.Header.Direction = ISOBUS_RX;
			packet.Header.Identifier = ((szMessage[0] & 0x000000FF) << 24) | ((szMessage[1] & 0x000000FF) << 16) | ((szMessage[2] & 0x000000FF) << 8) | (szMessage[3] & 0x000000FF);
			packet.DLC = szMessage[4];
			for (i = 0; i < packet.DLC; i++)
			{
				packet.Data[i] = szMessage[5 + i];
			}
			PrintPacket(&packet, 1);
			//Network_PacketHandler(&packet, &can_channel->Network);
			Network_PacketHandler(&packet, &CAN1.Network);
		}
	}
}

static void PrintPacket(const CAN_Packet_T *packet, uint8_t bus_num)
{
	char                buffer[64];
	char                data[25];
	const char*         direction_text[] = { "RX", "TX", "??" };
	ISOBUS_Direction_T  direction;
	DWORD               Msg_Time = GetTickCount() - start_time;

	// Print the Data
	data[0] = '\0';
	for (Size_T i = 0; i < packet->DLC; i++)
	{
		// Write previous data bytes and current data byte to buffer
		sprintf(buffer, "%s %02X", data, packet->Data[i]);

		// Transfer from buffer back to data
		sprintf(data, "%s", buffer);
	}

	direction = packet->Header.Direction;
	if (direction > ISOBUS_TX)
	{
		direction = (ISOBUS_Direction_T)2;
	}

	// Print the message contents to the screen
	sprintf(buffer, "%6lu %u %s %08X  %X %s\n",
		Msg_Time,
		bus_num,
		direction_text[direction],
		(int)packet->Header.Identifier,
		(int)packet->DLC,
		data);

	//(void)Pipe_InsertString(StandardOutput, buffer);
	printf("%s", buffer);
}
