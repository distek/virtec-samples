/******************************************************************************/
/*!
   \file
      Defines Solution level structures

   \copyright
      Copyright (C) 2012  DISTek Integration, Inc.  All Rights Reserved.

   \author
      Chris Thatcher
*/
/******************************************************************************/


#include "Platform.h"
#include "Foundation.h"
#include "VTClient.h"
#include "Solution.h"


SoftwareTimerList_T  Solution_SwTimerList = MAKE_SoftwareTimerList_T(milliseconds(10), PRIORITY_MAX);

//! declare the Software ID List
SoftwareIdList_T Solution_SoftwareId_List;

//! ECU ID fields for this hardware
EcuIdFields_T Solution_EcuId_Fields =
   {
      "Part Number",
      "Serial Number",
      "Location",
      "Type",
      "Manufacturer Name",
      "Hardware Version ID"
   };

//! Product ID fields for this hardware
ProductIdFields_T Solution_ProductId_Fields =
   {
      "Product identification code",
      "Product identification brand",
      "Product identification model"
   };


static SoftwareId_T Foundation_SoftwareIdEntry = MAKE_SoftwareId_T(FOUNDATION_VERSION);
static SoftwareId_T VTClient_SoftwareIdEntry = MAKE_SoftwareId_T(VTCLIENT_VERSION);


/******************************************************************************/
/*!
   \brief    Solution initalization routine

   \details  Initalize the Solution_SoftwareId_List

*/
/******************************************************************************/
void Solution_SoftwareId_Init(void)
{
   //! initalize the Platform Software ID List
   // The Software ID List must be initialized before any software IDs
   //  are registered
   SoftwareIdList_Init(&Solution_SoftwareId_List);
   // note: VTClient and ISOBUS Foundation IDs must be registered by the application
   (void)SoftwareId_Register(&Solution_SoftwareId_List, &Foundation_SoftwareIdEntry);   // ISOBUS Foundation
   (void)SoftwareId_Register(&Solution_SoftwareId_List, &VTClient_SoftwareIdEntry);     // VTClient
}


/******************************************************************************/
/*!
   \brief    Solution initalization routine

   \details  Initalize the Solution_SoftwareId_List

*/
/******************************************************************************/
void Solution_SoftwareId_Uninit(void)
{
   (void)SoftwareId_Unregister(&Solution_SoftwareId_List, &Foundation_SoftwareIdEntry);   // ISOBUS Foundation
   (void)SoftwareId_Unregister(&Solution_SoftwareId_List, &VTClient_SoftwareIdEntry);     // VTClient
}


/******************************************************************************/
/*!
   \brief    Solution initalization routine

   \details  Initalize the Solution_SwTimerList

*/
/******************************************************************************/
void Timers_Init(void)
{
   //! initalize the Platform Software Timer List
   // The Time List must be initialized before any timers are registered
   // The software timer list is part of the foundation
   SoftwareTimerList_Init(&Solution_SwTimerList);
}


/******************************************************************************/
/*!
   \brief    Solution task

   \details  This task runs once every 5ms with a priority of PL_8, and
             calls the SoftwareTimer_PeriodicTask to update the software timers.

*/
/******************************************************************************/
void Timers_Task(void)
{
   //! Update the platform timers (once every 5ms)
   SoftwareTimer_PeriodicTask(&Solution_SwTimerList);
}

/******************************************************************************/
/*
   Copyright (C) 2012  DISTek Integration, Inc.  All Rights Reserved.

   Developed by:
      DISTek(R) Integration, Inc.
      6612 Chancellor Drive Suite 600
      Cedar Falls, IA 50613
      Tel: 319-859-3600
*/
/******************************************************************************/
