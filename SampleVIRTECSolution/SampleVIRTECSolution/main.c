/******************************************************************************/
/*!
   \file
      Source file to start program

   \copyright
      Copyright (C) 2012  DISTek Integration, Inc.  All Rights Reserved.

   \author
      Chris Thatcher
*/
/******************************************************************************/


#include "Platform.h"


//! Get string from STDIN (used to exit program)
//extern int scanf(const char *format, ...);

/******************************************************************************/
/*!
   \brief    Thread that waits for input
*/
/******************************************************************************/
static DWORD TriggerExit(void *ignore)
{
   (void)ignore;
   int c;

   while ((c = _getch()) != '\r')
   {
      switch (c)
      {
      case '+':
         //   IncrementVolume();
         break;
      case '-':
         //   DecrementVolume();
         break;
      }
   }
   ExitTriggered = TRUE;
   ExitThread(0);
   return 0;
}


/******************************************************************************/
/*!
   \brief    Startup Function

   \details  This is the first function in the program to run

   \return   int

   \retval   0         Clean exit
   \retval   non-zero  failure
*/
/******************************************************************************/
int main(int argc, char **argv)
{
   UseSwitchedPower = FALSE;
   if(argc > 1)
   {
      UseSwitchedPower = TRUE;
   }

   // Dispatch the task priority levels (but they won't run until we exit critical)
   HANDLE thread = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)TriggerExit, NULL, 0, NULL);
   // Set the task priority levels
   (void)SetThreadPriority(thread, PRIORITY_MAX);

   while(!ExitTriggered)
   {
      while(UseSwitchedPower && !SwitchedPower_IsOn())
      {
         Sleep(500);
      }

      TaskScheduler_Init();
   }
   return 0;
}


/******************************************************************************/
/*
   Copyright (C) 2012  DISTek Integration, Inc.  All Rights Reserved.

   Developed by:
      DISTek(R) Integration, Inc.
      6612 Chancellor Drive Suite 600
      Cedar Falls, IA 50613
      Tel: 319-859-3600
*/
/******************************************************************************/
