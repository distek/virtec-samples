#ifndef TYPEDEF
#if (defined(__cplusplus) && defined(STRICT_TYPE))


template <class T>
class BaseType
{
public:
   BaseType();
   BaseType(T value);

   T Cast() const;
};


#define TYPEDEF(from,to)                                                 \
template <class T>                                                       \
class StrictType_##to : public T                                         \
{                                                                        \
public:                                                                  \
   StrictType_##to();                                                    \
   StrictType_##to(T value);                                             \
   StrictType_##to(const StrictType_##to<T> &that);                      \
                                                                         \
   /* Unary Prefix Operators */                                          \
   StrictType_##to<T> &operator+(void);                                  \
   StrictType_##to<T> &operator-(void);                                  \
   StrictType_##to<T> &operator*(void);                                  \
   StrictType_##to<T> *operator&(void);                                  \
   bool operator!(void);                                                 \
   StrictType_##to<T> &operator~(void);                                  \
   StrictType_##to<T> &operator++(void);                                 \
   StrictType_##to<T> &operator--(void);                                 \
                                                                         \
   /* Unary Postfix Operators */                                         \
   StrictType_##to<T> &operator++(int x);                                \
   StrictType_##to<T> &operator--(int x);                                \
                                                                         \
   /* Binary Operators */                                                \
   StrictType_##to<T> operator+(const StrictType_##to<T> &that) const;   \
   StrictType_##to<T> operator-(const StrictType_##to<T> &that) const;   \
   StrictType_##to<T> operator*(const StrictType_##to<T> &that) const;   \
   StrictType_##to<T> operator/(const StrictType_##to<T> &that) const;   \
   StrictType_##to<T> operator%(const StrictType_##to<T> &that) const;   \
   StrictType_##to<T> operator^(const StrictType_##to<T> &that) const;   \
   StrictType_##to<T> operator&(const StrictType_##to<T> &that) const;   \
   StrictType_##to<T> operator|(const StrictType_##to<T> &that) const;   \
                                                                         \
   /* Comparison Operators */                                            \
   bool operator<               (const StrictType_##to<T> &that) const;  \
   bool operator>               (const StrictType_##to<T> &that) const;  \
   bool operator==              (const StrictType_##to<T> &that) const;  \
   bool operator!=              (const StrictType_##to<T> &that) const;  \
   bool operator<=              (const StrictType_##to<T> &that) const;  \
   bool operator>=              (const StrictType_##to<T> &that) const;  \
   StrictType_##to<T> operator<<(const StrictType_##to<T> &that) const;  \
   StrictType_##to<T> operator>>(const StrictType_##to<T> &that) const;  \
   bool operator&&              (const StrictType_##to<T> &that) const;  \
   bool operator||              (const StrictType_##to<T> &that) const;  \
                                                                         \
   /* Assignment Operators */                                            \
   StrictType_##to<T> &operator= (const StrictType_##to<T> &that);       \
   StrictType_##to<T> &operator+=(const StrictType_##to<T> &that);       \
   StrictType_##to<T> &operator-=(const StrictType_##to<T> &that);       \
   StrictType_##to<T> &operator*=(const StrictType_##to<T> &that);       \
   StrictType_##to<T> &operator/=(const StrictType_##to<T> &that);       \
   StrictType_##to<T> &operator%=(const StrictType_##to<T> &that);       \
   StrictType_##to<T> &operator^=(const StrictType_##to<T> &that);       \
   StrictType_##to<T> &operator&=(const StrictType_##to<T> &that);       \
   StrictType_##to<T> &operator|=(const StrictType_##to<T> &that);       \
                                                                         \
   StrictType_##to<T> &operator<<=(const StrictType_##to<T> &that);      \
   StrictType_##to<T> &operator>>=(const StrictType_##to<T> &that);      \
                                                                         \
private:                                                                 \
   /* Binary Operators */                                                \
   StrictType_##to<T> operator+(const T &that) const;                    \
   StrictType_##to<T> operator-(const T &that) const;                    \
   StrictType_##to<T> operator*(const T &that) const;                    \
   StrictType_##to<T> operator/(const T &that) const;                    \
   StrictType_##to<T> operator%(const T &that) const;                    \
   StrictType_##to<T> operator^(const T &that) const;                    \
   StrictType_##to<T> operator&(const T &that) const;                    \
   StrictType_##to<T> operator|(const T &that) const;                    \
                                                                         \
   /* Comparison Operators */                                            \
   bool operator<               (const T &that) const;                   \
   bool operator>               (const T &that) const;                   \
   bool operator==              (const T &that) const;                   \
   bool operator!=              (const T &that) const;                   \
   bool operator<=              (const T &that) const;                   \
   bool operator>=              (const T &that) const;                   \
   StrictType_##to<T> operator<<(const T &that) const;                   \
   StrictType_##to<T> operator>>(const T &that) const;                   \
   bool operator&&              (const T &that) const;                   \
   bool operator||              (const T &that) const;                   \
                                                                         \
   /* Assignment Operators */                                            \
   StrictType_##to<T> &operator= (const T &that);                        \
   StrictType_##to<T> &operator+=(const T &that);                        \
   StrictType_##to<T> &operator-=(const T &that);                        \
   StrictType_##to<T> &operator*=(const T &that);                        \
   StrictType_##to<T> &operator/=(const T &that);                        \
   StrictType_##to<T> &operator%=(const T &that);                        \
   StrictType_##to<T> &operator^=(const T &that);                        \
   StrictType_##to<T> &operator&=(const T &that);                        \
   StrictType_##to<T> &operator|=(const T &that);                        \
                                                                         \
   StrictType_##to<T> &operator<<=(const T &that);                       \
   StrictType_##to<T> &operator>>=(const T &that);                       \
};                                                                       \
                                                                         \
typedef StrictType_##to<from> to


template <class T, class I>
class StrictArray : public T
{
public:
   StrictArray();
   StrictArray(T value);
   StrictArray(const StrictArray<T,I> &that);
   ~StrictArray();

   T &operator[](const I &that) const;
   bool operator==(const T *that) const;
   bool operator!=(const T *that) const;
};


#define BASE_TYPE(from,to)  typedef from raw_##to;  typedef BaseType<from> to
#define RAW_TYPE(type)  raw_##type
#define INTRINSIC(value)  ((value).Cast())
#define CAST(to_type,value)  ((to_type)(value).Cast())
#define CAST_MASK(to_type,value,mask)  ((to_type)((value).Cast() & (mask)))
#define ARRAY(data_type,name,index_type,size)  StrictArray<data_type,index_type> name
#define ARRAY_PTR(data_type,name,index_type)  StrictArray<data_type,index_type> name
#define SWITCH(x)  switch(CAST(unsigned long,x))
#define UNION struct

#ifndef NO_BREAK_LINT_COMMENT
#define NO_BREAK_LINT_COMMENT
#endif //NO_BREAK_LINT_COMMENT


#endif //__cplusplus
#endif //TYPEDEF
