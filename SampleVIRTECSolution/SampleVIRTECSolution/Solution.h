/******************************************************************************/
/*!
   \file
      Defines interface to the Solution

   \copyright
      Copyright (C) 2012  DISTek Integration, Inc.  All Rights Reserved.

   \author
      Chris Thatcher
*/
/******************************************************************************/

//! Software Timer List at the solution level
extern SoftwareTimerList_T  Solution_SwTimerList;

//! Software ID List at the solution level
extern SoftwareIdList_T Solution_SoftwareId_List;

//! ECU ID fields for this hardware
extern EcuIdFields_T Solution_EcuId_Fields;

//! Product ID fields for this hardware
extern ProductIdFields_T Solution_ProductId_Fields;

//! Initialize the SoftwareId list
extern void Solution_SoftwareId_Init(void);

//! Software Timer List initalization
extern void Timers_Init(void);
//! Software Timer Periodic Task
extern void Timers_Task(void);


/******************************************************************************/
/*
   Copyright (C) 2012  DISTek Integration, Inc.  All Rights Reserved.

   Developed by:
      DISTek(R) Integration, Inc.
      6612 Chancellor Drive Suite 600
      Cedar Falls, IA 50613
      Tel: 319-859-3600
*/
/******************************************************************************/
