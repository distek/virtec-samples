/******************************************************************************/
/*!
   \file
      Sample VT interaction

   \copyright
      Copyright (C) 2015  DISTek Integration, Inc.  All Rights Reserved.
*/
/******************************************************************************/


#include "Platform.h"
#include "Foundation.h"
#include "VTClient.h"
#include "Solution.h"
#include "SampleVT_ISOBUS.h"
#include "SampleVT_ObjectPool.h"

#define SAMPLEVT_SCALING         // rescale tank position, height, and width for VT data mask, comment out to disable rescaling

#define SAMPLEVT_OBJECT_POOL_VERSION  "VTPF001"

static void VtSoftKeyPress_Key_Metrics_5000(VTClient_T *vt_client, const VT_T *vt, const SoftKeyActivation_T *cb_data);                   // callback function prototypes
static void VtSoftKeyPress_Key_Tank_5001(VTClient_T *vt_client, const VT_T *vt, const SoftKeyActivation_T *cb_data);                      // callback function prototypes
static void VtSoftKeyPress_Key_Extra_5003(VTClient_T *vt_client, const VT_T *vt, const SoftKeyActivation_T *cb_data);                     // callback function prototypes
static void VtSoftKeyPress_Key_NextVt_5004(VTClient_T *vt_client, const VT_T *vt, const SoftKeyActivation_T *cb_data);                    // callback function prototypes
static void VtButtonPress_Button_FillStop_6002(VTClient_T *vt_client, const VT_T *vt, const ButtonActivation_T *cb_data);                 // callback function prototypes
static void VtButtonPress_Button_DrainStop_6003(VTClient_T *vt_client, const VT_T *vt, const ButtonActivation_T *cb_data);                // callback function prototypes
static void VtButtonPress_Button_MeterInc_6004(VTClient_T *vt_client, const VT_T *vt, const ButtonActivation_T *cb_data);                 // callback function prototypes
static void VtButtonPress_Button_MeterDec_6005(VTClient_T *vt_client, const VT_T *vt, const ButtonActivation_T *cb_data);                 // callback function prototypes
static void VtButtonPress_Button_DrainStart_6007(VTClient_T *vt_client, const VT_T *vt, const ButtonActivation_T *cb_data);               // callback function prototypes
static void VtButtonPress_Button_FillStart_6008(VTClient_T *vt_client, const VT_T *vt, const ButtonActivation_T *cb_data);                // callback function prototypes
static void VtNumericValue_InpNumber_MinLevel_9001(VTClient_T *vt_client, const VT_T *vt, const VtChangeNumericValue_T *cb_data);         // callback function prototypes
static void VtNumericValue_InpNumber_MaxLevel_9003(VTClient_T *vt_client, const VT_T *vt, const VtChangeNumericValue_T *cb_data);         // callback function prototypes
static void VtNumericValue_InpNumber_FillRate_9004(VTClient_T *vt_client, const VT_T *vt, const VtChangeNumericValue_T *cb_data);         // callback function prototypes
static void VtNumericValue_InpNumber_DrainRate_9005(VTClient_T *vt_client, const VT_T *vt, const VtChangeNumericValue_T *cb_data);        // callback function prototypes
static void VtNumericValue_NumberVariable_MeterSet_21003(VTClient_T *vt_client, const VT_T *vt, const VtChangeNumericValue_T *cb_data);   // callback function prototypes
static void VtNumericValue_InpBool_AutoFill_7000(VTClient_T *vt_client, const VT_T *vt, const VtChangeNumericValue_T *cb_data);           // callback function prototypes
static void VtNumericValue_NumberVariable_MsgDisp_21004(VTClient_T *vt_client, const VT_T *vt, const VtChangeNumericValue_T *cb_data);    // callback function prototypes
static void VtStringValue_InputString_Message_8000(VTClient_T *vt_client, const VT_T *vt, VtChangeStringValue_T *cb_data);                // callback function prototypes

// Create Op callback structs
SoftKeyActivation_Callback_T SoftKeyActivation_5000 = MAKE_SoftKeyActivation_Callback_T(VtSoftKeyPress_Key_Metrics_5000, Key_Metrics_5000);
SoftKeyActivation_Callback_T SoftKeyActivation_5001 = MAKE_SoftKeyActivation_Callback_T(VtSoftKeyPress_Key_Tank_5001, Key_Tank_5001);
SoftKeyActivation_Callback_T SoftKeyActivation_5003 = MAKE_SoftKeyActivation_Callback_T(VtSoftKeyPress_Key_Extra_5003, Key_Extra_5003);
SoftKeyActivation_Callback_T SoftKeyActivation_5004 = MAKE_SoftKeyActivation_Callback_T(VtSoftKeyPress_Key_NextVt_5004, Key_NextVt_5004);
ButtonActivation_Callback_T ButtonActivation_6002 = MAKE_ButtonActivation_Callback_T(VtButtonPress_Button_FillStop_6002, Button_FillStop_6002);
ButtonActivation_Callback_T ButtonActivation_6003 = MAKE_ButtonActivation_Callback_T(VtButtonPress_Button_DrainStop_6003, Button_DrainStop_6003);
ButtonActivation_Callback_T ButtonActivation_6004 = MAKE_ButtonActivation_Callback_T(VtButtonPress_Button_MeterInc_6004, Button_MeterInc_6004);
ButtonActivation_Callback_T ButtonActivation_6005 = MAKE_ButtonActivation_Callback_T(VtButtonPress_Button_MeterDec_6005, Button_MeterDec_6005);
ButtonActivation_Callback_T ButtonActivation_6007 = MAKE_ButtonActivation_Callback_T(VtButtonPress_Button_DrainStart_6007, Button_DrainStart_6007);
ButtonActivation_Callback_T ButtonActivation_6008 = MAKE_ButtonActivation_Callback_T(VtButtonPress_Button_FillStart_6008, Button_FillStart_6008);
VtChangeNumericValue_Callback_T VtChangeNumericValue_9001 = MAKE_VtChangeNumericValue_Callback_T(VtNumericValue_InpNumber_MinLevel_9001, InpNumber_MinLevel_9001);
VtChangeNumericValue_Callback_T VtChangeNumericValue_9003 = MAKE_VtChangeNumericValue_Callback_T(VtNumericValue_InpNumber_MaxLevel_9003, InpNumber_MaxLevel_9003);
VtChangeNumericValue_Callback_T VtChangeNumericValue_9004 = MAKE_VtChangeNumericValue_Callback_T(VtNumericValue_InpNumber_FillRate_9004, InpNumber_FillRate_9004);
VtChangeNumericValue_Callback_T VtChangeNumericValue_9005 = MAKE_VtChangeNumericValue_Callback_T(VtNumericValue_InpNumber_DrainRate_9005, InpNumber_DrainRate_9005);
VtChangeNumericValue_Callback_T VtChangeNumericValue_21003 = MAKE_VtChangeNumericValue_Callback_T(VtNumericValue_NumberVariable_MeterSet_21003, NumberVariable_MeterSet_21003);
VtChangeNumericValue_Callback_T VtChangeNumericValue_7000 = MAKE_VtChangeNumericValue_Callback_T(VtNumericValue_InpBool_AutoFill_7000, InpBool_AutoFill_7000);
VtChangeNumericValue_Callback_T VtChangeNumericValue_21004 = MAKE_VtChangeNumericValue_Callback_T(VtNumericValue_NumberVariable_MsgDisp_21004, NumberVariable_MsgDisp_21004);
VtChangeStringValue_Callback_T VtChangeStringValue_8000 = MAKE_VtChangeStringValue_Callback_T(VtStringValue_InputString_Message_8000, InputString_Message_8000);


typedef enum DemoState_E
{
   WAIT_VT,
   CONNECT_VT,
   DELETE_VERSIONS,
   SEND_OP,
   SEND_END_OP,
   OPERATOR_INTERACTION,
   DELETE_OP,
   DISCONNECT_VT,
   DEMO_IDLE
} DemoState_T;


static DemoState_T      SampleVT_DemoState;
static VT_T      *DemoVT;

// Declare and initialize the simulator timer
SoftwareTimer_T Simulator_Timer = MAKE_SoftwareTimer_T();


#define STOP_VT_STEP           0        // IDLE for the VT command sequences
#define START_VT_STEP          1        // first VT command sequence index

// TANK DataMask Info & Warnings (for 200x200 VT)
typedef enum TankDisp_e          // enum of the Tank messages
{
   MSG_PUMP_ON,                  //  0 - fill pump is on
   MSG_PUMP_OFF,                 //  1 - fill pump is off
   MSG_DRAIN_OPEN,               //  2 - drain vale is open
   MSG_DRAIN_CLOSED,             //  3 - drain valve is closed
   MSG_AUTO_FILL_ON,             //  4 - auto fill, pump on
   MSG_AUTO_FILL_OFF,            //  5 - auto fill, pump off
   MSG_AUTO_MODE_OFF,            //  6 - auto mode is disabled
   MSG_FILL_OVERFLOW,            //  7 - pump off to prevent overflow
   MSG_MIN_LOWLIMIT,             //  8 - minimum low level setting
   MSG_MAX_LOWLIMIT,             //  9 - maximum low level setting
   MSG_MIN_HIGHLIMIT,            // 10 - minimum high level setting
   MSG_MAX_HIGHLIMIT,            // 11 - maximum high level setting
   MSG_FILL_BLOCKED,             // 12 - fill blocked, tank is already at 95%
   MSG_MAX_INDEX,                // 13 - number of TankMsgList[13] entries (0-12)
   MSG_BUFFER,                   // 14 - special, display TankMsgBuf[]
   MSG_IDLE                      // 15 - no message to display
} TankDisp_t;

#define MAX_INFO_MSG       MSG_AUTO_MODE_OFF  // info messages are 0 - 6
#define MAX_WARN_MSG       MSG_FILL_BLOCKED   // info messages are 7 - 12

const char *TankMsgList[MSG_MAX_INDEX] = // array of Tank message pointers
{   // info messages
   "Fill pump is ON",                   //  0 - MSG_PUMP_ON
   "Fill pump is OFF",                  //  1 - MSG_PUMP_OFF
   "Drain valve is OPEN",               //  2 - MSG_DRAIN_OPEN
   "Drain valve is CLOSED",             //  3 - MSG_DRAIN_CLOSED
   "Auto Fill pump is On",              //  4 - MSG_AUTO_FILL_ON
   "Auto Fill pump is Off",             //  5 - MSG_AUTO_FILL_OFF
   "Auto mode has been disabled",       //  6 - MSG_AUTO_MODE_OFF
   // alarm messages
   "Fill stopped, prevent overflow",    //  7 - MSG_FILL_OVERFLOW
   "Minimum Min-Level setting, 10%",    //  8 - MSG_MIN_LOWLIMIT
   "Maximum Min-Level setting, 75%",    //  9 - MSG_MAX_LOWLIMIT
   "Minimum Max-Level setting, 20%",    // 10 - MSG_MIN_HIGHLIMIT
   "Maximum Max-Level setting, 90%",    // 11 - MSG_MAX_HIGHLIMIT
   "Fill blocked, tank is at 95%"       // 12 - MSG_FILL_BLOCKED
};

// TANK DataMask defines and variables (for 200x200 VT)
#define TANK_TOP_PIXELS       (Pixel_T)25      // tank top in pixels
#define TAMK_HEIGHT_PIXELS    (Pixel_T)150     // tank height in pixels
#define TANK_LEFT_PIXLES      (Pixel_T)9       // tank left side in pixels
#define TANK_RIGHT_PIXLES     (Pixel_T)72      // tank right side in pixels
#define TANK_POINTER_PIXLES   (Pixel_T)10      // height of limit pointer in pixels
#define INIT_FILL_STATE     FALSE      // fill on(true) or off(false)
#define INIT_DRAIL_STATE    FALSE      // drain open(true) or closed(false)
#define INIT_AUTO_ENABLE    FALSE      // auto enabled(true) or disabled(false)
#define FILL_RATE              30      // fill rate percent/sec (10-80)
#define DRAIN_RATE             20      // drain rate percent/sec (10-80)
#define AUTO_MAX_LEVEL         85      // auto fill max level percent (50-95)
#define AUTO_MIN_LEVEL         75      // auto fill min level percent (20-85)
#define TANK_LEVEL            200      // current tank level (0-1000 = 0.0-100.0%)

static Pixel_T  TankTop;           // tank top in pixels
static Pixel_T  TankHeight;        // tank height in pixels
static Pixel_T  TankLeftSide;      // tank left side in pixels
static Pixel_T  TankRightSide;     // tank right side in pixels
static Pixel_T  LimitPtrHight;     // height of limit pointer in pixels

static bool_t     FillState;       // fill on(true) or off(false)
static bool_t     DrainState;      // drain open(true) or closed(false)
static bool_t     AutoEnable;      // auto enabled(true) or disabled(false)
static bool_t     AutoOn;          // auto with fill On(true) or fill Off(false)
static uint16_t   FillRate;        // fill rate percent/sec (10-80)
static uint16_t   DrainRate;       // drain rate percent/sec (10-80)
static uint16_t   AutoMaxLevel;    // auto fill max level percent (50-95)
static uint16_t   AutoMinLevel;    // auto fill min level percent (20-85)
static uint16_t   TankLevel;       // tank level percent (0-1000 = 0.0-100.0)
static TankDisp_t DispTankMsg;     // indicates that a Tank message should be displayed
static bool_t     TankAlarmActive; // tank message is being displayed as Info or Warning
static char       TankMsgBuf[40];  // special(variable) display string

// METER DataMask variables (for 200x200 VT)
static uint16_t MeterSetting;    // meter setting (0-100 - 0-100%)

// MESSAGE DataMask defines and variables
typedef enum MsgDispType_e     // enum of the message display types
{
   MSG_DROP_IN,                // 0 - drop message into display, one character at a time
   MSG_SHIFT_IN,               // 1 - shift message into display from the right
   MAG_ROLL_IN,                // 2 - shift message into display one character at a time
   MSG_ROLL_THRU,              // 3 - continuously rotate message through the display
   MSG_HOLD                    // 4 - hold the message
} MsgDispType_t;

#define DISP_LENGTH         21      // message display, length of the Output String Object
#define INPUT_LENGTH        70      // message display, length of the Input String Object

static MsgDispType_t Ticker_Action;   // display type
static MsgDispType_t Select_Action;   // select input of the Message display type
static char     Input_String[INPUT_LENGTH+1];  // buffer for the VT's InputString
static char     MsgBuffer[DISP_LENGTH+1];      // buffer for the VT's OutputString
static Size_T   MsgLength;            // length of message
static Size_T   MsgLen;               // length of received message
static uint16_t MsgPtr;               // gef - current buffer pointer
static uint16_t MsgInp;               // input message pointer
static uint16_t MsgOut;               // Output message pointer
static uint16_t MsgState;             // message display state


// display variables
static ObjectID_T ActiveMaskID;      // object ID of the currently active display mask

// function prototypes
static uint16_t ShowFillOnStep;     // step index for the ShowFillOn() function
static uint16_t ShowFillOffStep;    // step index for the ShowFillOff() function
static uint16_t ShowDrainOpenStep;  // step index for the ShowDrainOpen() function
static uint16_t ShowDrainCloseStep; // step index for the ShowDrainClose() function
static uint16_t ChangMaxLevelStep;  // step index for the ChangMaxLevel() function
static uint16_t ChangMinLevelStep;  // step index for the ChangMinLevel() function
static uint16_t AutoEnableStep;     // step index for the AutoEnable() function
static uint16_t AutoDisableStep;    // step index for the AutoDisable() function
static uint16_t ShowMetricsStep;    // step index for the ShowMetrics() function


void Demo_Init(void);               // VT_Demo task init
void Demo_Task(void);               // VT_Demo task
static void InitSimulation(void);   // init VT simulation data values
static void RunSimulation(void);    // run TANK or MESSAGE simulation
static bool_t CalcMaxMinLevel(uint16_t ptrId, uint16_t limit);  // calculate position of the Max(high)/Min(low) level indicator
static void CalcTankLevel(uint16_t percent);    // calculate tank level, set level position and height
static void CalcPieSegement(uint16_t set);      // calculate and set the starting angel for the pie graph

static void ServiceVtSets(void);     // calls each of the VTset functions
static void ShowFillOn(void);        // VTset, show tank fill ON button, hide the OFF button
static void ShowFillOff(void);       // VTset, show tank fill OFF button, hide the ON button
static void ShowDrainOpen(void);     // VTset, show tank drain OPEN button, hide the CLOSE button
static void ShowDrainClose(void);    // VTset, show tank drain CLOSE button, hide the OPEN button
static void ChangMaxLevel(void);     // VTset, display MaxLevel and set position of MaxLevel indicator
static void ChangMinLevel(void);     // VTset, display set MinLevel, set position of MinLevel indicator
static void ShowAutoLevelPtr(void);  // VTset, show the MaxLevel and MinLevel indicators, and set their position
static void HideAutoLevelPtr(void);  // VTset, hide the MaxLevel and MinLevel indicators
static void ShowMetrics(void);       // VTset, display all of the metric data
static void ServiceTankMsg(void);    // display tank message, set or clear the message highlight
static void SetUpBuffer(void);       // initialize the message buffer and pointers to start the message display
static void UpdateBuffer(void);      // handles the character manipulation for the message display
#ifdef SAMPLEVT_SCALING
  static Pixel_T Scale_DataMask_Position(Pixel_T Scale_n, Pixel_T Scale_d, Pixel_T position);  // scales object position/size for the VT resolution
#endif


/******************************************************************************/
/*!
   \brief    Heartbeat task initialization
*/
/******************************************************************************/
void Demo_Init(void)
{
   // Init the object pool
   ObjectPool_Init(&SampleVT_ObjectPool);

   // Register Soft Key Activation object pool callbacks
   (void)SoftKeyActivation_Register(&SampleVT_ObjectPool, &SoftKeyActivation_5000);
   (void)SoftKeyActivation_Register(&SampleVT_ObjectPool, &SoftKeyActivation_5001);
   (void)SoftKeyActivation_Register(&SampleVT_ObjectPool, &SoftKeyActivation_5003);
   (void)SoftKeyActivation_Register(&SampleVT_ObjectPool, &SoftKeyActivation_5004);

   // Register Button Activation object pool callbacks
   (void)ButtonActivation_Register(&SampleVT_ObjectPool, &ButtonActivation_6002);
   (void)ButtonActivation_Register(&SampleVT_ObjectPool, &ButtonActivation_6003);
   (void)ButtonActivation_Register(&SampleVT_ObjectPool, &ButtonActivation_6004);
   (void)ButtonActivation_Register(&SampleVT_ObjectPool, &ButtonActivation_6005);
   (void)ButtonActivation_Register(&SampleVT_ObjectPool, &ButtonActivation_6007);
   (void)ButtonActivation_Register(&SampleVT_ObjectPool, &ButtonActivation_6008);

   // Register Vt Change Numeric Value object pool callbacks
   (void)VtChangeNumericValue_Register(&SampleVT_ObjectPool, &VtChangeNumericValue_7000);
   (void)VtChangeNumericValue_Register(&SampleVT_ObjectPool, &VtChangeNumericValue_9001);
   (void)VtChangeNumericValue_Register(&SampleVT_ObjectPool, &VtChangeNumericValue_9003);
   (void)VtChangeNumericValue_Register(&SampleVT_ObjectPool, &VtChangeNumericValue_9004);
   (void)VtChangeNumericValue_Register(&SampleVT_ObjectPool, &VtChangeNumericValue_9005);
   (void)VtChangeNumericValue_Register(&SampleVT_ObjectPool, &VtChangeNumericValue_21003);
   (void)VtChangeNumericValue_Register(&SampleVT_ObjectPool, &VtChangeNumericValue_21004);

   // Register Vt Change String Value object pool callbacks
   (void)VtChangeStringValue_Register(&SampleVT_ObjectPool, &VtChangeStringValue_8000);

   SampleVT_DemoState = WAIT_VT;

   (void)SoftwareTimer_Register(&Solution_SwTimerList, &Simulator_Timer);
}

/******************************************************************************/
/*!
   \brief    Task to send heartbeat message
*/
/******************************************************************************/
void Demo_Task(void)
{
#if ((!defined(STRICT_TYPE)) && !defined(__LINT__))
   if(ExitTriggered && (DemoVT != NULL))
   {
      SampleVT_DemoState = DISCONNECT_VT;
   }
#endif

   switch(SampleVT_DemoState)
   {
   // wait here until the app has connected to a VT
   case WAIT_VT:
      if(VT_NextVT(&SampleVT_VTClient, &DemoVT))
      {
         SampleVT_DemoState = CONNECT_VT;
      }
      break;
   // wait here until app has sent it's object pool to the VT
   case CONNECT_VT:
      if(VT_Connect(&SampleVT_VTClient, DemoVT))
      {
         SampleVT_DemoState = DELETE_VERSIONS;
      }
      break;
   // wait here until app has sent it's object pool to the VT
   case DELETE_VERSIONS:
//      if(DeleteVersion_Command(&SampleVT_VTClient, DemoVT, NULL, "       "))
      {
      SampleVT_DemoState = SEND_OP;
      }
      break;
   // wait here until app has sent it's object pool to the VT
   case SEND_OP:
      if(VT_SendObjectPool(&SampleVT_VTClient, DemoVT, &SampleVT_ObjectPool))
      {
         SampleVT_DemoState = SEND_END_OP;
      }
      break;
   // wait here until VT has accepted the app's object pool
   case SEND_END_OP:
      if(DemoVT->ObjectPool.State == VT_OP_OPERATOR_INTERACTION)
      {
         SampleVT_DemoState = OPERATOR_INTERACTION;
         InitSimulation();
      }
      else if(DemoVT->ObjectPool.State == VT_OP_IDLE)
      {
         // Move to the next VT to connect with
         SampleVT_DemoState = DELETE_OP;
      }
      break;
   // app's object pool is loaded and available
   case OPERATOR_INTERACTION:
      // If we lost connection with the VT
      if(SoftwareTimer_Get(&DemoVT->Status.Timer) == TIMER_EXPIRED)
      {
         // Search for a new VT to connect with
         SampleVT_DemoState = WAIT_VT;
      }
      else if(DemoVT->ObjectPool.State == VT_OP_IDLE)
      {
         // Move to the next VT to connect with
         SampleVT_DemoState = WAIT_VT;
      }
      else
      {
         RunSimulation();
      }
      break;
   // wait here while the VT delets the object pool,
   // and disconnects from the TV (Next VT)
   case DELETE_OP:
      if(VT_Disconnect(&SampleVT_VTClient, DemoVT))
      {
         SampleVT_DemoState = WAIT_VT;
      }
      break;
   // wait here while the VT deletes the object pool
   // and disconnects from the TV (Not Next VT)
   case DISCONNECT_VT:
      if(VT_Disconnect(&SampleVT_VTClient, DemoVT))
      {
         SampleVT_DemoState = DEMO_IDLE;
      }
      break;
   // wait here for power-down
   case DEMO_IDLE:
   default:
      break;
   }
}

/******************************************************************************/
/*!
   \brief    Initialize the display position and size values if necessary
*/
/******************************************************************************/
static void InitSimulation(void)
{
#ifdef SAMPLEVT_SCALING
   // these values need to be initialized for the VT
   Pixel_T  Scale_n = DemoVT->Metrics.Hardware.DataMask_X_Pixels;  // data mask scaling Numerator
   Pixel_T  Scale_d = DemoVT->ObjectPool.Pool->DataMask_XY;        // data mask scaling Denominator
#endif // SAMPLEVT_SCALING

   ActiveMaskID = DataMask_Tank_1000;    // tank display mask

#ifdef SAMPLEVT_SCALING
   // scale the Tank pixel values for this VT
   TankTop       = Scale_DataMask_Position(Scale_n, Scale_d, TANK_TOP_PIXELS);
   TankHeight    = Scale_DataMask_Position(Scale_n, Scale_d, TAMK_HEIGHT_PIXELS);
   TankLeftSide  = Scale_DataMask_Position(Scale_n, Scale_d, TANK_LEFT_PIXLES);
   TankRightSide = Scale_DataMask_Position(Scale_n, Scale_d, TANK_RIGHT_PIXLES);
   LimitPtrHight = Scale_DataMask_Position(Scale_n, Scale_d, TANK_POINTER_PIXLES);
#else
   TankTop       = TANK_TOP_PIXELS;       // tank top in pixels
   TankHeight    = TAMK_HEIGHT_PIXELS;    // tank height in pixels
   TankLeftSide  = TANK_LEFT_PIXLES;      // tank left side in pixels
   TankRightSide = TANK_RIGHT_PIXLES;     // tank right side in pixels
   LimitPtrHight = TANK_POINTER_PIXLES;   // height of limit pointer in pixels
#endif  // SAMPLEVT_SCALING

   FillState    = INIT_FILL_STATE;       // fill on(true) or off(false)
   DrainState   = INIT_DRAIL_STATE;      // drain open(true) or closed(false)
   AutoEnable   = INIT_AUTO_ENABLE;      // auto enabled(true) or disabled(false)
   AutoOn       = FALSE;                 // auto with fill On(true) or fill Off(false)
   FillRate     = FILL_RATE;             // fill rate percent/sec (10-80)
   DrainRate    = DRAIN_RATE;            // drain rate percent/sec (10-80)
   AutoMaxLevel = AUTO_MAX_LEVEL;        // auto fill max level percent (50-95)
   AutoMinLevel = AUTO_MIN_LEVEL;        // auto fill min level percent (20-85)

   TankLevel    = 10;            // 10% filled, tank level percent (0-100 = 0.0-100.0)
   MeterSetting = 10;            // meters at 10%, meter setting (0-100 - 0-100%)
   DispTankMsg  = MSG_IDLE;
   TankAlarmActive = FALSE;

   (void)VTv4.NumberVariable.ChangeNumericValue(&SampleVT_VTClient, DemoVT, NULL, NumberVariable_MeterSet_21003, ((NumericValue_T)(MeterSetting)));
   CalcPieSegement(MeterSetting);

   SoftwareTimer_Set(&Simulator_Timer, milliseconds(100));  // reset for 100 ms
   ShowMetricsStep = START_VT_STEP;
}


/******************************************************************************/
/*!
   \brief    Run the TANK simulation and the Meter Inc/Dec
*/
/******************************************************************************/
static void RunSimulation(void)
{
   static uint16_t half_sec = 0;
   uint16_t temp_level = TankLevel;

   if(SoftwareTimer_Get(&Simulator_Timer) == TIMER_EXPIRED)
   {
      half_sec++;
      SoftwareTimer_Set(&Simulator_Timer, milliseconds(100));  // reset for 100 ms
      ActiveMaskID = DemoVT->Status.VisibleDataAlarmMask;
      //
      // if TANK is displayed. service the Tank Simulator
      //
      if ((ActiveMaskID == DataMask_Tank_1000) && (half_sec >= 5))
      {
         half_sec = 0;
         // Fill/Drain rate is %(0-100), level is %*10(0-1000)
         // if fill is on, calculate the new level
         if (FillState == TRUE)
         {
            // if auto fill is not on, do an uncontrolled fill
            if (AutoEnable == FALSE)
            {
               temp_level += FillRate;
            }
            else  // auto fill is on, do a controlled fill
            {
               // stop auto fill at its high limit
               if ( (AutoOn == TRUE) && (temp_level >= (AutoMaxLevel*10)) )
               {
                  AutoOn = FALSE;  // stop the auto fill
                  DispTankMsg = MSG_AUTO_FILL_OFF;
               }
               // restart auto fill at its low limit
               if ( (AutoOn == FALSE) && (temp_level <= (AutoMinLevel*10)) )
               {
                  AutoOn = TRUE;
                  DispTankMsg = MSG_AUTO_FILL_ON;
               }
               // fill while AutoOn is true
               if (AutoOn == TRUE)
               {
                  temp_level += FillRate;
               }
            }
         }
         // if drain is open, calculate the new level
         if (DrainState == TRUE)
         {
            if (temp_level < DrainRate)
            {
               temp_level = 0;
            }
            else
            {
               temp_level -= DrainRate;
            }
         }
         // if the tank level goes above 95.0%, stop the fill
         if ( (temp_level > 950) && (FillState == TRUE) )
         {
            FillState = FALSE;   // turn the pump off
            ShowFillOnStep = START_VT_STEP;   // hide FillOff, show FillOn
            DispTankMsg = MSG_FILL_OVERFLOW;  // send overflow message
         }

         // output new tank level, and adjust the level indicator
         if (TankLevel != temp_level)
         {
            TankLevel = temp_level;
            (void)VTv4.NumberVariable.ChangeNumericValue(&SampleVT_VTClient, DemoVT, NULL, NumberVariable_TankLevel_21000, ((NumericValue_T)(TankLevel)));
            CalcTankLevel(TankLevel);
         }
      } // endof if(DataMask_Tank_1000)

      //
      // Message Display mask
      //
      else if ((ActiveMaskID == DataMask_Extra_1003) && (half_sec >= 3))
      {
         half_sec = 0;
         UpdateBuffer();
      }

   } // endof if(Simulator_Timer Expired)
   else
   {
      ServiceVtSets();
      ServiceTankMsg();
   }
}


/******************************************************************************/
/*!
   \brief  Callback to unlock the datamask (if it was locked)

   \param  callback  callback data
   \param  event     Transport event
*/
/******************************************************************************/
static void Tank_Unlock(const ISOBUS_MessageCallback_T *callback, ISOBUS_MessageEvent_T event)
{
   if ((event == MESSAGE_ABORTED) || (event == MESSAGE_COMPLETE))
   {
      if (DemoVT->Metrics.GetMemory.Version >= (VT_Version_T)4)
      {
         LockUnlockMask_Command(&SampleVT_VTClient, DemoVT, NULL, Mask_Unlock, DataMask_Tank_1000, milliseconds(100));
      }
   }
}


/******************************************************************************/
/*!
   \brief    Calculate Top and Height of tank fill level. Send new attributes
             to the VT.
             Level_height = TankHeight(140) * level / 100
             Level_top = TankHeight(140) - Level_height + TankTop(27)
   \param  percent  desired percent of the tank level, range 0 - 1000 (0.0-100.0%)
*/
/******************************************************************************/
static void CalcTankLevel(uint16_t percent)  // percent is 0-1000 => 0.0%-100.0%
{
   ISOBUS_MessageCallback_T  callback;
   uint16_t level = percent / 10;  // level range is 0-100
   Pixel_T height = (TankHeight * level / 100);
   Pixel_T top = TankHeight - height + TankTop;
   height += (Pixel_T)1;

   callback.Function = Tank_Unlock;
   callback.Pointer_1 = &SampleVT_VTClient;
   callback.Pointer_2 = DemoVT;
   if (DemoVT->Metrics.GetMemory.Version >= (VT_Version_T)4)
   {
      LockUnlockMask_Command(&SampleVT_VTClient, DemoVT, NULL, Mask_Lock, DataMask_Tank_1000, milliseconds(500));
   }
   (void)VTv4.Rectangle.ChangeAttribute.Height(&SampleVT_VTClient, DemoVT, NULL, Rectangle_TankLevel_14001, height);
   (void)ChangeChildPosition_Command(&SampleVT_VTClient, DemoVT, &callback, DataMask_Tank_1000, Rectangle_TankLevel_14001, TankLeftSide, top);
}


/******************************************************************************/
/*!
   \brief    Calculate Top of Max & Min level indicators. Send new attributes
             to the VT
             ptr_offset = TankTop(27) - (LimitPtrHight(10) / 2)
             position = TankHeight(140) - (TankHeight(140) * limit / 100) + offset
   \param  ptr_id  object ID of the desired limit pointer
           limit   desired percent of tank level, range 0 - 100 (0-100%)
   \return  TRUE if ChildPosition was successfully sent to VT, else FALSE
*/
/******************************************************************************/
static bool_t CalcMaxMinLevel(uint16_t ptr_id, uint16_t limit)  // limit range, 0-100
{
   Pixel_T limit_ptr;
   Pixel_T offset;
   bool_t rtn;

//   offset = TankTop - (LimitPtrHight / 2) + 1;
   offset = TankTop - (LimitPtrHight / (Pixel_T)2);
   limit_ptr = TankHeight + offset - (TankHeight * limit / 100);

   rtn = ChangeChildPosition_Command(&SampleVT_VTClient, DemoVT, NULL, DataMask_Tank_1000, ptr_id, TankRightSide, limit_ptr);
   return rtn;
}


/******************************************************************************/
/*!
   \brief    Calculate Start & End of pie display segment. Send new attributes
             to the VT.
   \param  set desired percent of pie graph, range 0 - 100 (0-100%)
*/
/******************************************************************************/
static void CalcPieSegement(uint16_t set) // set (0 - 100)
{
   uint16_t utmp = set;  // range is now 0 - 100

   if (utmp >= 100) // >= 100%
   {
      utmp = 91;
   }
   else if (utmp == 0) // >= 100%
   {
      utmp = 89;
   }
   // start = 90 - (90 * utmp / 25)
   else if (utmp <= 25)  // 0% to 25 %
   {
      utmp = 90 - (utmp * 90 / 25);
   }
   // start = 360 - ((utmp - 25) * 270 / 75)
   else  // 26% - 99%
   {
      utmp = 360 - ((utmp - 25) * 270 / 75);
   }
   utmp /= 2;  // actual angle to VT is desired angle / 2
   (void)VTv4.Ellipse.ChangeAttribute.StartAngle(&SampleVT_VTClient, DemoVT, NULL, Ellipse_Segment_15001, (Angle_T)utmp);
}

/******************************************************************************/
/*!
\brief    Callback to service VT SoftKey presses
\param  cb_data pointer to the SoftKeyActivation_T data structure
*/
/******************************************************************************/
static void VtSoftKeyPress_Key_Metrics_5000(VTClient_T *vt_client, const VT_T *vt, const SoftKeyActivation_T *cb_data)
{
   if (cb_data != NULL)
   {
      if (cb_data->key_activation_code == KeyButton_Released)
      {
         ShowMetricsStep = START_VT_STEP;
      }
   }
}

static void VtSoftKeyPress_Key_Tank_5001(VTClient_T *vt_client, const VT_T *vt, const SoftKeyActivation_T *cb_data)
{
   if (cb_data != NULL)
   {
      if (cb_data->key_activation_code == KeyButton_Released)
      {
         // clear the Tank Message display
         (void)VTv4.StringVariable.ChangeStringValue(&SampleVT_VTClient, DemoVT, NULL, OutputString_TankMsg_11015, 1, " ");
      } //endof if(softkey)
   }
}

static void VtSoftKeyPress_Key_Extra_5003(VTClient_T *vt_client, const VT_T *vt, const SoftKeyActivation_T *cb_data)
{
   if (cb_data != NULL)
   {
      if (cb_data->key_activation_code == KeyButton_Released)
      {
         // reset the string buffer
         SetUpBuffer();
      } //endof if(softkey)
   }
}

static void VtSoftKeyPress_Key_NextVt_5004(VTClient_T *vt_client, const VT_T *vt, const SoftKeyActivation_T *cb_data)
{
   if (cb_data != NULL)
   {
      if (cb_data->key_activation_code == KeyButton_Released)
      {
         // delete object pool, and look for another VT
         SampleVT_DemoState = DELETE_OP;
      } //endof if(softkey)
   }
}

/******************************************************************************/
/*!
\brief    Callback to service VT Button presses
\param  cb_data pointer to the ButtonActivation_T data structure
*/
/******************************************************************************/
static void VtButtonPress_Button_FillStop_6002(VTClient_T *vt_client, const VT_T *vt, const ButtonActivation_T *cb_data)
{

   if (cb_data != NULL)
   {
      if (cb_data->key_activation_code == 1)  // tank fill OFF was pressed
      {
         FillState = FALSE;  // stop the fill
      }
      else if (cb_data->key_activation_code == 0) // Fill-OFF button was released
      {
         ShowFillOnStep = START_VT_STEP;
         DispTankMsg = MSG_PUMP_OFF;
      }
   }
}

static void VtButtonPress_Button_DrainStop_6003(VTClient_T *vt_client, const VT_T *vt, const ButtonActivation_T *cb_data)
{

   if (cb_data != NULL)
   {
      if (cb_data->key_activation_code == 1)  // tank drain CLOSE was pressed
      {
         DrainState = FALSE;  // close the drain
      }
      else if (cb_data->key_activation_code == 0) // Drain-Close button was released
      {
         ShowDrainOpenStep = START_VT_STEP;
         DispTankMsg = MSG_DRAIN_CLOSED;
      }
   }
}

static void VtButtonPress_Button_MeterInc_6004(VTClient_T *vt_client, const VT_T *vt, const ButtonActivation_T *cb_data)
{

   if (cb_data != NULL)
   {
      if ((cb_data->key_activation_code == 1) || // button pressed
         (cb_data->key_activation_code == 2))  // button held
      {
         if (MeterSetting < 100)
         {
            MeterSetting++;
            (void)VTv4.NumberVariable.ChangeNumericValue(&SampleVT_VTClient, DemoVT, NULL, NumberVariable_MeterSet_21003, MeterSetting);
            CalcPieSegement(MeterSetting);
         }
      }
   }
}

static void VtButtonPress_Button_MeterDec_6005(VTClient_T *vt_client, const VT_T *vt, const ButtonActivation_T *cb_data)
{

   if (cb_data != NULL)
   {
      if ((cb_data->key_activation_code == 1) ||  // button pressed
         (cb_data->key_activation_code == 2))   // button held
      {
         if (MeterSetting > 0)
         {
            MeterSetting--;
            (void)VTv4.NumberVariable.ChangeNumericValue(&SampleVT_VTClient, DemoVT, NULL, NumberVariable_MeterSet_21003, MeterSetting);
            CalcPieSegement(MeterSetting);
         }
      }
   }
}

static void VtButtonPress_Button_DrainStart_6007(VTClient_T *vt_client, const VT_T *vt, const ButtonActivation_T *cb_data)
{

   if (cb_data != NULL)
   {
      if (cb_data->key_activation_code == 1)  // tank drain OPEN was pressed
      {
         DrainState = TRUE;  // open the drain
      }
      else if (cb_data->key_activation_code == 0) // Drain-Open button was released
      {
         ShowDrainCloseStep = START_VT_STEP;
         DispTankMsg = MSG_DRAIN_OPEN;
      }
   }
}

static void VtButtonPress_Button_FillStart_6008(VTClient_T *vt_client, const VT_T *vt, const ButtonActivation_T *cb_data)
{

   if (cb_data != NULL)
   {
      if (cb_data->key_activation_code == 1)  // tank fill ON was pressed
      {
         if (TankLevel < 950)  // tank is below 95% filled
         {
            FillState = TRUE;  // start the fill
         }
         else
         {
            DispTankMsg = MSG_FILL_BLOCKED;
         }
      }
      else if (cb_data->key_activation_code == 0) // Fill-ON button was released
      {
         if (FillState == TRUE)  // fill pump was turned ON
         {
            ShowFillOffStep = START_VT_STEP;
            DispTankMsg = MSG_PUMP_ON;
         }
      }
   }
}

/******************************************************************************/
/*!
\brief    Callback to service VT Numerical value entry
\param  cb_data pointer to the VtChangeNumericValue_T data structure
*/
/******************************************************************************/
static void VtNumericValue_InpNumber_MinLevel_9001(VTClient_T *vt_client, const VT_T *vt, const VtChangeNumericValue_T *cb_data)
{
   Size_T   length;
   uint16_t itmp;

   if (cb_data != NULL)
   {
#ifdef SAMPLEVT_DEBUG_DISPLAY
      sprintf(buffer, "Number ID(%u) Value(%u)\n", cb_data->object_id, (uint16_t)cb_data->value);
      (void)Pipe_InsertString(StandardOutput, buffer);
      (void)Pipe_InsertString(StandardOutput, "MinLevel\n");
#endif
      AutoMinLevel = (uint16_t)cb_data->value;
      if (AutoMinLevel < 10)
      {
         AutoMinLevel = 10;
         DispTankMsg = MSG_MIN_LOWLIMIT;
      }
      else if (AutoMinLevel > AutoMaxLevel - 5)  // within 5% of Max Limit
      {
         AutoMinLevel = AutoMaxLevel - 5;
         /***************************************************************************/
         length = String_Length(TankMsgList[MSG_MAX_LOWLIMIT]);
         for (itmp = 0; itmp < length; itmp++)
         {
            TankMsgBuf[itmp] = TankMsgList[MSG_MAX_LOWLIMIT][itmp];
         }
         itmp = AutoMinLevel / 10;
         TankMsgBuf[length - 3] = itmp + '0';
         itmp = AutoMinLevel - (itmp * 10);
         TankMsgBuf[length - 2] = itmp + '0';
         /***************************************************************************/
         //               sprintf(TankMsgBuf, "Maximum Min-Level setting, %u%%", AutoMinLevel);
         DispTankMsg = MSG_BUFFER;
      }
      else if (AutoMinLevel > 75)
      {
         AutoMinLevel = 75;
         DispTankMsg = MSG_MAX_LOWLIMIT;
      }
      ChangMinLevelStep = START_VT_STEP;
   }
}

static void VtNumericValue_InpNumber_MaxLevel_9003(VTClient_T *vt_client, const VT_T *vt, const VtChangeNumericValue_T *cb_data)
{
   Size_T   length;
   uint16_t itmp;

   if (cb_data != NULL)
   {
#ifdef SAMPLEVT_DEBUG_DISPLAY
      sprintf(buffer, "Number ID(%u) Value(%u)\n", cb_data->object_id, (uint16_t)cb_data->value);
      (void)Pipe_InsertString(StandardOutput, buffer);
      (void)Pipe_InsertString(StandardOutput, "MaxLevel\n");
#endif
      AutoMaxLevel = (uint16_t)cb_data->value;
      if (AutoMaxLevel < 20)
      {
         AutoMaxLevel = 20;
         DispTankMsg = MSG_MIN_HIGHLIMIT;
      }
      else if (AutoMaxLevel < AutoMinLevel + 5)  // within 5% of Min Limit
      {
         AutoMaxLevel = AutoMinLevel + 5;
         /***************************************************************************/
         length = String_Length(TankMsgList[MSG_MIN_HIGHLIMIT]);
         for (itmp = 0; itmp < length; itmp++)
         {
            TankMsgBuf[itmp] = TankMsgList[MSG_MIN_HIGHLIMIT][itmp];
         }
         itmp = AutoMaxLevel / 10;
         TankMsgBuf[length - 3] = itmp + '0';
         itmp = AutoMaxLevel - (itmp * 10);
         TankMsgBuf[length - 2] = itmp + '0';
         /***************************************************************************/
         //               sprintf(TankMsgBuf, "Minimum Max-Level setting, %u%%", AutoMaxLevel);
         DispTankMsg = MSG_BUFFER;
      }
      else if (AutoMaxLevel > 90)
      {
         AutoMaxLevel = 90;
         DispTankMsg = MSG_MAX_HIGHLIMIT;
      }
      ChangMaxLevelStep = START_VT_STEP;
   }
}

static void VtNumericValue_InpNumber_FillRate_9004(VTClient_T *vt_client, const VT_T *vt, const VtChangeNumericValue_T *cb_data)
{
   if (cb_data != NULL)
   {
#ifdef SAMPLEVT_DEBUG_DISPLAY
      sprintf(buffer, "Number ID(%u) Value(%u)\n", cb_data->object_id, (uint16_t)cb_data->value);
      (void)Pipe_InsertString(StandardOutput, buffer);
      (void)Pipe_InsertString(StandardOutput, "FillRate\n");
#endif
      FillRate = (uint16_t)cb_data->value;
   }
}

static void VtNumericValue_InpNumber_DrainRate_9005(VTClient_T *vt_client, const VT_T *vt, const VtChangeNumericValue_T *cb_data)
{
   if (cb_data != NULL)
   {
#ifdef SAMPLEVT_DEBUG_DISPLAY
      sprintf(buffer, "Number ID(%u) Value(%u)\n", cb_data->object_id, (uint16_t)cb_data->value);
      (void)Pipe_InsertString(StandardOutput, buffer);
      (void)Pipe_InsertString(StandardOutput, "DrainRate\n");
#endif
      DrainRate = (uint16_t)cb_data->value;
   }
}

static void VtNumericValue_NumberVariable_MeterSet_21003(VTClient_T *vt_client, const VT_T *vt, const VtChangeNumericValue_T *cb_data)
{
   if (cb_data != NULL)
   {
#ifdef SAMPLEVT_DEBUG_DISPLAY
      sprintf(buffer, "Number ID(%u) Value(%u)\n", cb_data->object_id, (uint16_t)cb_data->value);
      (void)Pipe_InsertString(StandardOutput, buffer);
      (void)Pipe_InsertString(StandardOutput, "MeterSet\n");
#endif
      MeterSetting = (uint16_t)cb_data->value;
      CalcPieSegement(MeterSetting);
   }
}

static void VtNumericValue_InpBool_AutoFill_7000(VTClient_T *vt_client, const VT_T *vt, const VtChangeNumericValue_T *cb_data)
{
   if (cb_data != NULL)
   {
#ifdef SAMPLEVT_DEBUG_DISPLAY
      sprintf(buffer, "Number ID(%u) Value(%u)\n", cb_data->object_id, (uint16_t)cb_data->value);
      (void)Pipe_InsertString(StandardOutput, buffer);
      (void)Pipe_InsertString(StandardOutput, "AutoFill\n");
#endif
      AutoEnable = (uint16_t)cb_data->value & 0x01;      // auto enabled(true) or disabled(false)
      if (AutoEnable == TRUE)
      {
         AutoEnableStep = START_VT_STEP;
      }
      else
      {
         AutoDisableStep = START_VT_STEP;
         DispTankMsg = MSG_AUTO_MODE_OFF;
      }
   }
}

static void VtNumericValue_NumberVariable_MsgDisp_21004(VTClient_T *vt_client, const VT_T *vt, const VtChangeNumericValue_T *cb_data)
{
#ifdef SAMPLEVT_DEBUG_DISPLAY
   sprintf(buffer, "Number ID(%u) Value(%u)\n", cb_data->object_id, (uint16_t)cb_data->value);
   (void)Pipe_InsertString(StandardOutput, buffer);
#endif

   Select_Action = (MsgDispType_t)cb_data->value;
   SetUpBuffer();
}

/******************************************************************************/
/*!
\brief   Callback to service VT String value entry
\param   cb_data pointer to the VtChangeStringValue_T data structure
*/
/******************************************************************************/
static void VtStringValue_InputString_Message_8000(VTClient_T *vt_client, const VT_T *vt, VtChangeStringValue_T *cb_data)
{
#ifdef SAMPLEVT_DEBUG_DISPLAY
   char buffer[50];
#endif

   if (cb_data != NULL)
   {
#ifdef SAMPLEVT_DEBUG_DISPLAY
      sprintf(buffer, "Number ID(%u) String Length(%lu)\n", cb_data->object_id, cb_data->length);
      (void)Pipe_InsertString(StandardOutput, buffer);
#endif
      MsgLen = cb_data->length;
      // if the string copy is successful
      if (Pipe_CopyData(cb_data->data, Input_String, MsgLen) == TRUE)
      {
         // find the last non-space character (strip trailing space)
         MsgLength = MsgLen;
         while ((MsgLength > 0) && (Input_String[MsgLength - 1] == ' '))
         {
            MsgLength--;
         }
         SetUpBuffer();  // reset the message display logic
      }
   }
   // Close the pipe handle
   Pipe_CloseReadHandle(&(cb_data->data));
}


/******************************************************************************/
/*!
   \brief    Calls each of the nine VTset functions
*/
/******************************************************************************/
static void ServiceVtSets(void)
{
   ShowFillOn();
   ShowFillOff();
   ShowDrainOpen();
   ShowDrainClose();
   ChangMaxLevel();
   ChangMinLevel();
   ShowAutoLevelPtr();
   HideAutoLevelPtr();
   ShowMetrics();
}


/******************************************************************************/
/*!
   \brief    VTset, fill pump is Off, display the fill ON button.
*/
/******************************************************************************/
static void ShowFillOn(void)
{
   bool_t rtn = TRUE;

   switch(ShowFillOnStep)
   {
   case START_VT_STEP:  // hide FillOff
      rtn = VTv4.Container.HideShowObject(&SampleVT_VTClient, DemoVT, NULL, Container_FillOff_3004, Object_Hidden);
      break;
   case START_VT_STEP+1:  // show FillOn
      rtn = VTv4.Container.HideShowObject(&SampleVT_VTClient, DemoVT, NULL, Container_FillOn_3003, Object_Shown);
      break;
   case START_VT_STEP+2:  // select-x
      rtn = VTv4.InputNumber.SelectInputObject(&SampleVT_VTClient, DemoVT, NULL, InpNumber_FillRate_9004, 0xFF);
      break;
   case STOP_VT_STEP:  // idle
   default:
      break;
   }
   ShowFillOnStep += (rtn == TRUE) ? 1 : 0;
}



/******************************************************************************/
/*!
   \brief    VTset, fill pump is On, display the fill OFF button
*/
/******************************************************************************/
static void ShowFillOff(void)
{
   bool_t rtn = TRUE;

   switch(ShowFillOffStep)
   {
   case START_VT_STEP:  // hide FillOn
      rtn = VTv4.Container.HideShowObject(&SampleVT_VTClient, DemoVT, NULL, Container_FillOn_3003, Object_Hidden);
      break;
   case START_VT_STEP+1:  // show FillOff
      rtn = VTv4.Container.HideShowObject(&SampleVT_VTClient, DemoVT, NULL, Container_FillOff_3004, Object_Shown);
      break;
   case START_VT_STEP+2:  // select-x
      rtn = VTv4.InputNumber.SelectInputObject(&SampleVT_VTClient, DemoVT, NULL, InpNumber_FillRate_9004, 0xFF);
      break;
   case STOP_VT_STEP:  // idle
   default:
      break;
   }
   ShowFillOffStep += (rtn == TRUE) ? 1 : 0;
}


/******************************************************************************/
/*!
   \brief    VTset, drain valve is Closed, display the drain OPEN button
*/
/******************************************************************************/
static void ShowDrainOpen(void)
{
   bool_t rtn = TRUE;

   switch(ShowDrainOpenStep)
   {
   case START_VT_STEP:  // hide DrainClose
      rtn = VTv4.Container.HideShowObject(&SampleVT_VTClient, DemoVT, NULL, Container_DrainClose_3005, Object_Hidden);
      break;
   case START_VT_STEP+1:  // show DrainOpen
      rtn = VTv4.Container.HideShowObject(&SampleVT_VTClient, DemoVT, NULL, Container_DrainOpen_3006, Object_Shown);
      break;
   case START_VT_STEP+2:  // select-x
      rtn = VTv4.InputNumber.SelectInputObject(&SampleVT_VTClient, DemoVT, NULL, InpNumber_DrainRate_9005, 0xFF);
      break;
   case STOP_VT_STEP:  // idle
   default:
      break;
   }
   ShowDrainOpenStep += (rtn == TRUE) ? 1 : 0;
}


/******************************************************************************/
/*!
   \brief    VTset, drain valve is Open, display the drain CLOSE button
*/
/******************************************************************************/
static void ShowDrainClose(void)
{
   bool_t rtn = TRUE;

   switch(ShowDrainCloseStep)
   {
   case START_VT_STEP:  // hide DrainOpen
      rtn = VTv4.Container.HideShowObject(&SampleVT_VTClient, DemoVT, NULL, Container_DrainOpen_3006, Object_Hidden);
      break;
   case START_VT_STEP+1:  // show DrainClose
      rtn = VTv4.Container.HideShowObject(&SampleVT_VTClient, DemoVT, NULL, Container_DrainClose_3005, Object_Shown);
      break;
   case START_VT_STEP+2:  // select-x
      rtn = VTv4.InputNumber.SelectInputObject(&SampleVT_VTClient, DemoVT, NULL, InpNumber_DrainRate_9005, 0xFF);
      break;
   case STOP_VT_STEP:  // idle
   default:
      break;
   }
   ShowDrainCloseStep += (rtn == TRUE) ? 1 : 0;
}


/******************************************************************************/
/*!
   \brief    VTset, position the Max(high) Level arrow indicator
*/
/******************************************************************************/
static void ChangMaxLevel(void)
{
   bool_t rtn = TRUE;

   switch(ChangMaxLevelStep)
   {
   case START_VT_STEP:  // calculate MaxLevel position
      rtn = VTv4.NumberVariable.ChangeNumericValue(&SampleVT_VTClient, DemoVT, NULL, InpNumber_MaxLevel_9003, AutoMaxLevel);
      break;
   case START_VT_STEP+1:  // set MaxLevel
      rtn = CalcMaxMinLevel(Container_MaxPtr_3002, AutoMaxLevel);
      break;
   case STOP_VT_STEP:  // idle
   default:
      break;
   }
   ChangMaxLevelStep += (rtn == TRUE) ? 1 : 0;
}


/******************************************************************************/
/*!
   \brief    VTset, position the Min(low) Level arrow indicator
*/
/******************************************************************************/
static void ChangMinLevel(void)
{
   bool_t rtn = TRUE;

   switch(ChangMinLevelStep)
   {
   case START_VT_STEP:  // calculate MinLevel position
      rtn = VTv4.NumberVariable.ChangeNumericValue(&SampleVT_VTClient, DemoVT, NULL, InpNumber_MinLevel_9001, AutoMinLevel);
      break;
   case START_VT_STEP+1:  // set MinLevel
      rtn = CalcMaxMinLevel(Container_MinPtr_3001, AutoMinLevel);
      break;
   case STOP_VT_STEP:  // idle
   default:
      break;
   }
   ChangMinLevelStep += (rtn == TRUE) ? 1 : 0;
}


/******************************************************************************/
/*!
   \brief    VTset, Auto enabled, show the Max & Min Level arrow indicators
*/
/******************************************************************************/
static void ShowAutoLevelPtr(void)
{
   bool_t rtn = TRUE;

   switch(AutoEnableStep)
   {
   case START_VT_STEP:  // show Max pointer
      rtn = VTv4.Container.HideShowObject(&SampleVT_VTClient, DemoVT, NULL, Container_MaxPtr_3002, Object_Shown);
      break;
   case START_VT_STEP+1:  // show Min pointer
      rtn = VTv4.Container.HideShowObject(&SampleVT_VTClient, DemoVT, NULL, Container_MinPtr_3001, Object_Shown);
      break;
   case START_VT_STEP+2:  // Max pointer position
      rtn = CalcMaxMinLevel(Container_MaxPtr_3002, AutoMaxLevel);
      break;
   case START_VT_STEP+3:  // Min pointer position
      rtn = CalcMaxMinLevel(Container_MinPtr_3001, AutoMinLevel);
      break;
   case STOP_VT_STEP:  // idle
   default:
      break;
   }
   AutoEnableStep += (rtn == TRUE) ? 1 : 0;
}


/******************************************************************************/
/*!
   \brief    VTset, Auto is disabled, hide the Max & Min Level arrow indicators
*/
/******************************************************************************/
static void HideAutoLevelPtr(void)
{
   bool_t rtn = TRUE;

   switch(AutoDisableStep)
   {
   case START_VT_STEP:  // hide Max pointer
      rtn = VTv4.Container.HideShowObject(&SampleVT_VTClient, DemoVT, NULL, Container_MaxPtr_3002, Object_Hidden);
      break;
   case START_VT_STEP+1:  // hide Min pointer
      rtn = VTv4.Container.HideShowObject(&SampleVT_VTClient, DemoVT, NULL, Container_MinPtr_3001, Object_Hidden);
      break;
   case STOP_VT_STEP:  // idle
   default:
      break;
   }
   AutoDisableStep += (rtn == TRUE) ? 1 : 0;
}


/******************************************************************************/
/*!
   \brief    VTset, display the VT metrics
*/
/******************************************************************************/
static void ShowMetrics(void)
{
   bool_t rtn = TRUE;

   switch(ShowMetricsStep)
   {
   case START_VT_STEP:  // Language code
      rtn = VTv4.StringVariable.ChangeStringValue(&SampleVT_VTClient, DemoVT, NULL, OutputString_Language_11034, 2, DemoVT->Metrics.Language.LanguageCode);
      break;
   case START_VT_STEP+1:  // VT Version
      rtn = VTv4.NumberVariable.ChangeNumericValue(&SampleVT_VTClient, DemoVT, NULL, OutputNumber_VtVersion_12001, ((NumericValue_T)(DemoVT->Metrics.GetMemory.Version)));
      break;
   case START_VT_STEP+2:  // VT Physical SK
      rtn = VTv4.NumberVariable.ChangeNumericValue(&SampleVT_VTClient, DemoVT, NULL, OutputNumber_PhySK_12002, ((NumericValue_T)(DemoVT->Metrics.SoftKeys.Physical)));
      break;
   case START_VT_STEP+3:  // VT Virtual SK
      rtn = VTv4.NumberVariable.ChangeNumericValue(&SampleVT_VTClient, DemoVT, NULL, OutputNumber_VirSK_12003, ((NumericValue_T)(DemoVT->Metrics.SoftKeys.Virtual)));
      break;
   case START_VT_STEP+4:  // Softkey Y
      rtn = VTv4.NumberVariable.ChangeNumericValue(&SampleVT_VTClient, DemoVT, NULL, OutputNumber_SK_Y_12004, ((NumericValue_T)(DemoVT->Metrics.SoftKeys.Y_Pixels)));
      break;
   case START_VT_STEP+5:  // Softkey X
      rtn = VTv4.NumberVariable.ChangeNumericValue(&SampleVT_VTClient, DemoVT, NULL, OutputNumber_SK_X_12005, ((NumericValue_T)(DemoVT->Metrics.SoftKeys.X_Pixels)));
      break;
   case START_VT_STEP+6:  // DataMask ?
      rtn = VTv4.NumberVariable.ChangeNumericValue(&SampleVT_VTClient, DemoVT, NULL, OutputNumber_Datamask_12006, ((NumericValue_T)(DemoVT->Metrics.Hardware.DataMask_X_Pixels)));
      break;
   case START_VT_STEP+7:  // Unit System
      switch (DemoVT->Metrics.Language.UnitsSystem)
      {
      case 0:
         rtn = VTv4.StringVariable.ChangeStringValue(&SampleVT_VTClient, DemoVT, NULL, OutputString_Units_11035, 6, "Metric");
         break;
      case 1:
         rtn = VTv4.StringVariable.ChangeStringValue(&SampleVT_VTClient, DemoVT, NULL, OutputString_Units_11035, 8, "Imperial");
         break;
      case 2:
         rtn = VTv4.StringVariable.ChangeStringValue(&SampleVT_VTClient, DemoVT, NULL, OutputString_Units_11035, 2, "US");
         break;
      default:
         break;
      }
      break;
   case STOP_VT_STEP:  // idle
   default:
      break;
   }
   ShowMetricsStep += (rtn == TRUE) ? 1 : 0;

}


/******************************************************************************/
/*!
   \brief    display the requested Tank message, highlight if a warning is displayed
*/
/******************************************************************************/
static void ServiceTankMsg(void)
{
   if (DispTankMsg == MSG_BUFFER)  // special, display the message buffer
   {
      if (VTv4.StringVariable.ChangeStringValue(&SampleVT_VTClient, DemoVT, NULL, OutputString_TankMsg_11015, String_Length(TankMsgBuf), TankMsgBuf))
      {
         if (TankAlarmActive == FALSE)
         {
            TankAlarmActive = TRUE;
            VTv4.OutputString.ChangeAttribute.Options(&SampleVT_VTClient, DemoVT, NULL, OutputString_TankMsg_11015, (StringOptions_T)0x00);  // set transparent off
            DispTankMsg = MSG_IDLE;
         }
      }
   }
   else if (DispTankMsg < MSG_MAX_INDEX)  // display predefined messages
   {
      if (VTv4.StringVariable.ChangeStringValue(&SampleVT_VTClient, DemoVT, NULL, OutputString_TankMsg_11015, String_Length(TankMsgList[DispTankMsg]), TankMsgList[DispTankMsg]))
      {
         if (DispTankMsg <= MAX_INFO_MSG)  // 0 - 6, display info message
         {
            if (TankAlarmActive == TRUE)
            {
               TankAlarmActive = FALSE;
               VTv4.OutputString.ChangeAttribute.Options(&SampleVT_VTClient, DemoVT, NULL, OutputString_TankMsg_11015, STRING_OPTIONS_TRANSPARENT);
            }
         }
         else if (DispTankMsg <= MAX_WARN_MSG)  // 7 - 12, display alarm message
         {
            if (TankAlarmActive == FALSE)  // display an alarm message
            {
               TankAlarmActive = TRUE;
               VTv4.OutputString.ChangeAttribute.Options(&SampleVT_VTClient, DemoVT, NULL, OutputString_TankMsg_11015, (StringOptions_T)0x00);  // set transparent off
            }
         }
         DispTankMsg = MSG_IDLE;
      }
   }
}



#ifdef SAMPLEVT_SCALING
/******************************************************************************/
/*!
   \brief    Re-scale display (pixels) position or height for the datamask size
   \param  Scale_n  numerator used for the scaling
           Scale_d  denominator used for the scaling
           position the pixel value to be scaled
   \return the rescaled position (or size) value
*/
/******************************************************************************/
static Pixel_T Scale_DataMask_Position(Pixel_T Scale_n, Pixel_T Scale_d, Pixel_T position)
{
   // Convert from unsigned representation to sign
   if (position >= (Pixel_T)0x8000)
   {
      position -= (Pixel_T)0x10000;
   }

   // Scale the values
   position = (Pixel_T)Utility_Round(((long)(position)) * ((long)(Scale_n)), ((long)(Scale_d)));

   // Convert back to unsigned representation
   if (position < (Pixel_T)0)
   {
      position += (Pixel_T)0x10000;
   }
   return position;
}
#endif


/******************************************************************************/
/*!
   \brief    Setup the MESSAGE DataMask
*/
/******************************************************************************/
static void SetUpBuffer(void)
{
   uint16_t i;

   // clear the displayed message, fill with space
   // blank the display, fill with spaces
   for (i=0; i < DISP_LENGTH; i++)
   {
      MsgBuffer[i] = ' ';
   }
   MsgState = 0;     // message builder state
   MsgInp = 0;       // input message index
   MsgOut = 0;       // display buffer index
   Ticker_Action = Select_Action;
}


/******************************************************************************/
/*!
   \brief    Run the desired MESSAGE DataMask display sequence
*/
/******************************************************************************/
static void UpdateBuffer(void)
{
   uint16_t i;
   char seperater[] = "...";

   switch ((MsgDispType_t)Ticker_Action)
   {
   // drop characters into the display string
   case MSG_DROP_IN:  // type 0, drop message into display one character at a time
      if (VTv4.StringVariable.ChangeStringValue(&SampleVT_VTClient, DemoVT, NULL, OutputString_Ticker_11044, DISP_LENGTH, MsgBuffer) == FALSE)
      {
         break;  // exit switch(), try again later
      }
      // else continue, run until the message is all in or the display buffer is full
      if ((MsgInp < MsgLength) && (MsgInp < DISP_LENGTH))
      {
         MsgBuffer[MsgOut] = Input_String[MsgInp];
         MsgOut++;
         MsgInp++;
      }
      else  // stop, either all in or display is full
      {
         Ticker_Action = MSG_HOLD;
      }
      break;

   // shift string into the display from the right
   case MSG_SHIFT_IN:  // type 1, shift message into the display from the right
      if (VTv4.StringVariable.ChangeStringValue(&SampleVT_VTClient, DemoVT, NULL, OutputString_Ticker_11044, DISP_LENGTH, MsgBuffer) == FALSE)
      {
         break;  // exit switch(), try again later
      }
      // else continue, stop when the message is all in or the display buffer is full
      if (MsgOut >= DISP_LENGTH)
      {
         Ticker_Action = MSG_HOLD;
      }
      else  // shift left and append the next char or a space
      {
         MsgOut++;
         for (i = 0; i < DISP_LENGTH - 1; i++)
         {
            MsgBuffer[i] = MsgBuffer[i + 1];
         }
         // move message into display buffer, one char at a time
         if (MsgInp < MsgLength)
         {
            MsgBuffer[i] = Input_String[MsgInp];
            MsgInp++;
         }
         else  // the message is in, fill with space
         {
            MsgBuffer[i] = ' ';
         }
      }
      break;

   // shift string characters into the display one at a time from the right
   case MAG_ROLL_IN:  // type 2, shift message into display one character at a time
      if (VTv4.StringVariable.ChangeStringValue(&SampleVT_VTClient, DemoVT, NULL, OutputString_Ticker_11044, DISP_LENGTH, MsgBuffer) == FALSE)
      {
         break;  // exit switch(), try again later
      }
      // else continue, stop when all characters have been rolled into the display
      switch (MsgState)
      {
      case 0: // start a char shift
         MsgPtr = DISP_LENGTH-1;
         MsgBuffer[MsgPtr] = Input_String[MsgInp];
         // if the display buffer is full, stop
         if ( (MsgPtr == MsgOut) || (MsgInp >= MsgLength) )
         {
            Ticker_Action = MSG_HOLD;
         }
         else  // else shift in a new char
         {
            MsgInp++;
            if (MsgBuffer[MsgPtr] == ' ')
            {
               MsgOut++;       // move to the next char position
            }
            else
            {
               MsgState = 1;
            }
         }
         break;

      case 1: // shift char into position
         MsgBuffer[MsgPtr-1] = MsgBuffer[MsgPtr];  // move char one position to the left
         MsgBuffer[MsgPtr] = ' ';   // back fill with a space
         MsgPtr--;
         // when the char is in its final position, setup for the next char
         if (MsgPtr <= MsgOut)
         {
            MsgOut++;       // move to the next char position
            MsgState = 0;   // and start the next char
         }
         break;
      default:
         break;
      }
      break;

   // shift string and separator continuously through the display, right to left
   case MSG_ROLL_THRU:  // type 3, continuous roll the message through the display
      if (VTv4.StringVariable.ChangeStringValue(&SampleVT_VTClient, DemoVT, NULL, OutputString_Ticker_11044, DISP_LENGTH, MsgBuffer) == FALSE)
      {
         break;  // exit switch(), try again later
      }
      // else continue, move the display buffer left one position
      for (i=0; i < DISP_LENGTH-1; i++)
      {
         MsgBuffer[i] = MsgBuffer[i+1];  // move display left one char position
      }

      // add the next display char
      switch (MsgState)
      {
      case 0:  // setup for another message
         MsgInp = 0;
         MsgPtr = 0;
         MsgState = 1;
         // fall through is intentional

      case 1: // add the next message char
         MsgBuffer[DISP_LENGTH-1] = Input_String[MsgInp];  // move next char into the display buffer
         MsgInp++;   // message character index
         // when the message is all in, add a separator
         if (MsgInp >= MsgLength)
         {
            MsgInp = 0;
            MsgState = 2;
         }
         break;

      case 2: // add the next separator char
         MsgBuffer[DISP_LENGTH-1] = seperater[MsgPtr];  // add separator
         MsgPtr++;   // separator character index
         // when the separator is all in, start another message
         if (MsgPtr >= 3)
         {
            MsgPtr = 0;
            MsgState = 0;
         }
         break;
      default:
         MsgState = MSG_HOLD;
         break;
      }  // endof switch(MsgState)
      break;

   case MSG_HOLD:
   default:
      break;
   }  // endof switch(Ticker_Action)
}


/******************************************************************************/
/*
   Copyright (C) 2015  DISTek Integration, Inc.  All Rights Reserved.

   Developed by:
      DISTek(R) Integration, Inc.
      6612 Chancellor Drive Suite 600
      Cedar Falls, IA 50613
      Tel: 319-859-3600
*/
/******************************************************************************/
