/******************************************************************************/
/*!
   \file
      Sample implementation of required application functions

   \copyright
      Copyright (C) 2012  DISTek Integration, Inc.  All Rights Reserved.
*/
/******************************************************************************/


#include "Platform.h"
#include "Foundation.h"
#include "VTClient.h"
#include "SampleVT_ISOBUS.h"


//! Create library task INIT functions
#define LIBRARY_INIT(fn,state) \
void SampleVT_##fn (void)      \
{                              \
   fn(state);                  \
}


//! Create library TASK functions
#define LIBRARY_TASK(fn,state,period,priority) \
void SampleVT_##fn (void)                      \
{                                              \
   fn(state);                                  \
}


//! Create library task EXIT functions
#define LIBRARY_EXIT(fn,state) \
void SampleVT_##fn (void)      \
{                              \
   fn(state);                  \
}


#include "SampleVT_ScheduledTasks.h"


#undef LIBRARY_INIT
#undef LIBRARY_TASK
#undef LIBRARY_EXIT


/******************************************************************************/
/*
   Copyright (C) 2012  DISTek Integration, Inc.  All Rights Reserved.

   Developed by:
      DISTek(R) Integration, Inc.
      6612 Chancellor Drive Suite 600
      Cedar Falls, IA 50613
      Tel: 319-859-3600
*/
/******************************************************************************/
