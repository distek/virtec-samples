/******************************************************************************/
/*!
   \file
      Sample implementation of required application functions

   \copyright
      Copyright (C) 2015  DISTek Integration, Inc.  All Rights Reserved.
*/
/******************************************************************************/


#include "Platform.h"
#include "Foundation.h"
#include "VTClient.h"
#include "Solution.h"
#include "Can.h"
#include "SampleVT_ISOBUS.h"
#include "DISTek_Memory.h"


// Create individual Pipe arrays (for pipe collection)
//! \cond  SKIP
#define PIPE(name, priority, size)  static MAKE_PIPE_ARRAY(name, MinAddressable_T, size);
//! \endcond
#include "SampleVT_Pipes.h"


//! Pipe collection for SampleVT
static Pipe_T SampleVT_PipeArray[] =
   {
//! \cond  SKIP
#define PIPE(name, priority, size)  MAKE_Pipe_T(name, priority),
//! \endcond
#include "SampleVT_Pipes.h"
   };


//! Pipe collection for SampleVT
static Pipes_T SampleVT_PipeCollection = MAKE_Pipes_T(SampleVT_PipeArray, PRIORITY_MAX);


//! TP session array for SampleVT
static ISOBUS_TransportSession_T SampleVT_TP_Sessions[4];

//! Functionalities supported by this application
static const Functionalities_T  SampleVT_Functionalities[] =
   {
      // Supports Minimum Control Functionality
      MAKE_Functionalities_T__MinimumControlFunction(),
      // Supports Universal Terminal
      MAKE_Functionalities_T__UniversalTerminal_WorkingSet()
   };


//! DTCs used by SampleVT
static const DTC_T  SampleVT_DTCArray[] =
   {
      MAKE_DTC_T(0, 0)
   };

//! Lamp status used by SampleVT
static DTC_Lamp_T  SampleVT_DTCLampArray[] =
   {
      AMBER_WARNING_LAMP_STATUS
   };

static DTC_Status_T SampleVT_DTCStatusArray[sizeof(SampleVT_DTCArray)/sizeof(DTC_T)];


//! Queue used for receive packets to lower processing priority
static MAKE_QUEUE_ARRAY(SampleVT_RxQueueArray, ISOBUS_Packet_T, 50);
static Queue_T SampleVT_RxQueue = MAKE_Queue_T(SampleVT_RxQueueArray, PRIORITY_MAX);

MAKE_CAN(CAN1, 200, 20, 14)

// Create Foundation Functionality structure
Foundation_T SampleVT_Foundation =
   MAKE_Foundation_T(
      &Solution_SwTimerList,
      &CAN1.Network,
      // **************************************************************
      // sa_primary            = 128 (0x80) Primary source address
      // choose_sa_fn          = NULL (use built-in 128-247 range)
      //
      // priority              = PL_8
      // **************************************************************
      MAKE_ISOBUS_AddressClaim_S(128, NULL, PL_8),
      // **************************************************************
      // self_configurable     = 1, this is a Self-configurable address
      // industry_group        = 2, Agricultural and forestry equipment
      // device_class_instance = 0
      // device_class          = 2, ??
      // function              = 129, On-board Diagnostic Unit
      // function_instance     = 0
      // ecu_instance          = 3, ??
      // manufacturer_code     = 514, DISTek Integration, Inc
      // identity_number       = 1
      // **************************************************************
      MAKE_ISOBUS_Name_T(1,2,0,2,129,0,3,514,1),
      MAKE_ISOBUS_Transport_T(PRIORITY_MAX, 2, 16, SampleVT_TP_Sessions, SampleVT_PipeCollection),
      MAKE_LanguageCallbackList_T(PRIORITY_MAX),
      MAKE_ISOBUS_EcuId_T(PRIORITY_MAX, Solution_EcuId_Fields),
      MAKE_ISOBUS_SoftwareId_T(PL_6, Solution_SoftwareId_List),
      MAKE_ISOBUS_ProductId_T(PRIORITY_MAX, Solution_ProductId_Fields),
      MAKE_ISOBUS_DiagnosticProtocol_T(ECU_DIAGNOSTICS_ISO_11783_LEVEL_1, PRIORITY_MAX),
      MAKE_DTC_List_T(SampleVT_DTCArray, SampleVT_DTCStatusArray, SampleVT_DTCLampArray, PRIORITY_MAX),
      MAKE_ISOBUS_Functionalities_T(PRIORITY_MAX, SampleVT_Functionalities),
      MAKE_ISOBUS_Certification_T(14, 0, 514, 7, 0,  1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0),
      MAKE_Memory_T(Memory_Read, Memory_Write),
      MAKE_Foundation_PacketHandler_List_S(&SampleVT_RxQueue, NULL, PRIORITY_MAX),
      MAKE_Request_S(PRIORITY_MAX),
      MAKE_Acknowledge_S(PRIORITY_MAX)
   );


//! VTClient data structure
static VT_T  SampleVT_VTs[32];

//! VTClient data structure
VTClient_T SampleVT_VTClient = MAKE_VTClient_T(&SampleVT_Foundation, SampleVT_VTs, NULL, NULL, PRIORITY_MAX);


/******************************************************************************/
/*!
   \brief    Register SampleVT's packet handler
*/
/******************************************************************************/
void SampleVT_ISOBUS_Init(void)
{
   Pipes_Init(&SampleVT_PipeCollection);

   Foundation_Init(&SampleVT_Foundation);
}


/******************************************************************************/
/*
   Copyright (C) 2015  DISTek Integration, Inc.  All Rights Reserved.

   Developed by:
      DISTek(R) Integration, Inc.
      6612 Chancellor Drive Suite 600
      Cedar Falls, IA 50613
      Tel: 319-859-3600
*/
/******************************************************************************/
