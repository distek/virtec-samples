/******************************************************************************/
/*!
   \file
      Sample Application

   \copyright
      Copyright (C) 2015  DISTek Integration, Inc.  All Rights Reserved.

   \author
      Chris Thatcher
*/
/******************************************************************************/


#include "Platform.h"
#include "Foundation.h"
#include "VTClient.h"
#include "Solution.h"
#include "SampleVT_ISOBUS.h"
#include "Can.h"


//! Version of SampleVT Software    (Product,Major,minor,build)
#define SAMPLEVT_SOFTWARE_VERSION    SoftwareVersion("SampleVT",0,0,1)

//! Heartbeat repetition rate (once per second)
#define HEARTBEAT_RATE         seconds(1)  // 1 second

// local variables
//! Declare and initialize the heartbeat message timer
SoftwareTimer_T Heartbeat_Timer = MAKE_SoftwareTimer_T();

//! Initialize Software ID structure for the SampleVT software
SoftwareId_T SampleVT_SoftwareIdEntry = MAKE_SoftwareId_T(SAMPLEVT_SOFTWARE_VERSION);

//! Heartbeat count
unsigned long Heartbeats;
uint16_t ccp_test;

// prototypes
void Heartbeat_Init(void);
void Heartbeat_Uninit(void);
void Heartbeat_Task(void);

/******************************************************************************/
/*!
   \brief    Heartbeat task initialization
*/
/******************************************************************************/
void Heartbeat_Init(void)
{
   Heartbeats = 0;  // reset heartbeat count to 0
   ccp_test = 0;

   (void)SoftwareTimer_Register(&Solution_SwTimerList, &Heartbeat_Timer);
   // register application's software IDs
   (void)SoftwareId_Register(&Solution_SoftwareId_List, &SampleVT_SoftwareIdEntry);  // SampleVT
}

/******************************************************************************/
/*!
   \brief    Heartbeat task initialization
*/
/******************************************************************************/
void Heartbeat_Uninit(void)
{
   (void)SoftwareTimer_Unregister(&Solution_SwTimerList, &Heartbeat_Timer);
   (void)SoftwareId_Unregister(&Solution_SoftwareId_List, &SampleVT_SoftwareIdEntry);  // SampleVT
}

/******************************************************************************/
/*!
   \brief    Task to send heartbeat message called once every 10ms
*/
/******************************************************************************/
void Heartbeat_Task(void)
{
   ISOBUS_Packet_T Heartbeat_Msg;

   // then process the task
   // build and send the Heartbeat CAN Message
   if(SoftwareTimer_Get(&Heartbeat_Timer) == TIMER_EXPIRED)
   {
      // Populate the Heartbeat message
      Heartbeat_Msg.Header.Priority = (ISOBUS_PacketPriority_T)6;  // Priority
      Heartbeat_Msg.Header.Direction = ISOBUS_TX;
      Heartbeat_Msg.Header.PGN = (ISOBUS_PGN_T)0x0EF00;  // PGN
      Heartbeat_Msg.Header.Source = SampleVT_Foundation.AddressClaim.NameIndex;
      Heartbeat_Msg.Header.Destination = SampleVT_Foundation.AddressClaim.NameIndex;     // Send this to myself to avoid causing issues on other controllers
      Heartbeat_Msg.DLC = (ISOBUS_DLC_T)4;              // send 4 bytes
      Heartbeat_Msg.Data[3] = (ISOBUS_PacketData_T)(Heartbeats >> 24);
      Heartbeat_Msg.Data[2] = (ISOBUS_PacketData_T)(Heartbeats >> 16);
      Heartbeat_Msg.Data[1] = (ISOBUS_PacketData_T)(Heartbeats >> 8);
      Heartbeat_Msg.Data[0] = (ISOBUS_PacketData_T)(Heartbeats);

      if(Network_SendPacket(&Heartbeat_Msg, NULL, &SampleVT_Foundation))
      {
         SoftwareTimer_Set(&Heartbeat_Timer, HEARTBEAT_RATE);  // reset for 1 second
         Heartbeats++;                                         // next heartbeat count
         ccp_test++;
      }
   }
}


/******************************************************************************/
/*
   Copyright (C) 2015  DISTek Integration, Inc.  All Rights Reserved.

   Developed by:
      DISTek(R) Integration, Inc.
      6612 Chancellor Drive Suite 600
      Cedar Falls, IA 50613
      Tel: 319-859-3600
*/
/******************************************************************************/
