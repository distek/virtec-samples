/******************************************************************************/
/*
   \file
      Schedule the tasks for SampleVT

   \copyright
      Copyright (C) 2015  DISTek Integration, Inc.  All Rights Reserved.

   \author
      Chris Thatcher
*/
/******************************************************************************/


#ifndef INIT
//! Used for scheduling a function to run before any TASK
#define INIT(fn)
#define UNDEF_INIT
#endif //INIT

#ifndef TASK
//! Used for scheduling a function to execute periodically
#define TASK(fn,period,priority)
#define UNDEF_TASK
#endif //TASK

#ifndef EXIT
//! Used for scheduling tasks to run at shutdown
#define EXIT(fn)
#define UNDEF_EXIT
#endif //EXIT


#ifndef LIBRARY_INIT
//! Used for scheduling a function to run before any TASK
#define LIBRARY_INIT(fn,state)  INIT(SampleVT_##fn)
#define UNDEF_LIBRARY_INIT
#endif //LIBRARY_INIT

#ifndef LIBRARY_TASK
//! Used for scheduling a function to execute periodically
#define LIBRARY_TASK(fn,state,period,priority)  TASK(SampleVT_##fn,period,priority)
#define UNDEF_LIBRARY_TASK
#endif //LIBRARY_TASK

#ifndef LIBRARY_EXIT
//! Used for scheduling tasks to run at shutdown
#define LIBRARY_EXIT(fn,state)  EXIT(SampleVT_##fn)
#define UNDEF_LIBRARY_EXIT
#endif //LIBRARY_EXIT


/******************************************************************************/
// BEGIN:  Task Scheduling
/******************************************************************************/
//! \cond  SKIP


//   Init function
INIT(Heartbeat_Init)
INIT(Demo_Init)
INIT(SampleVT_ISOBUS_Init)
LIBRARY_INIT(VTClient_Init,   &SampleVT_VTClient)

//   Task function,                      period (ms),  priority
TASK(Heartbeat_Task,                                  10,   PL_6)  // Send a "heartbeat" message
TASK(Demo_Task,                                       20,   PL_4)
LIBRARY_TASK(Foundation_Task, &SampleVT_Foundation,   80,  PL_10)
LIBRARY_TASK(VTClient_Task,   &SampleVT_VTClient,     40,   PL_8)


//   Exit function
EXIT(Heartbeat_Uninit)
LIBRARY_EXIT(Foundation_Uninit, &SampleVT_Foundation)
LIBRARY_EXIT(VTClient_Uninit,   &SampleVT_VTClient)


//! \endcond
/******************************************************************************/
// END:    Task Scheduling
/******************************************************************************/


#ifdef UNDEF_LIBRARY_INIT
#undef LIBRARY_INIT
#undef UNDEF_LIBRARY_INIT
#endif
#ifdef UNDEF_LIBRARY_TASK
#undef LIBRARY_TASK
#undef UNDEF_LIBRARY_TASK
#endif
#ifdef UNDEF_LIBRARY_EXIT
#undef LIBRARY_EXIT
#undef UNDEF_LIBRARY_EXIT
#endif

#ifdef UNDEF_INIT
#undef INIT
#undef UNDEF_LIBRARY_INIT
#endif
#ifdef UNDEF_TASK
#undef TASK
#undef UNDEF_LIBRARY_TASK
#endif
#ifdef UNDEF_EXIT
#undef EXIT
#undef UNDEF_LIBRARY_EXIT
#endif


/******************************************************************************/
/*
   Copyright (C) 2015  DISTek Integration, Inc.  All Rights Reserved.

   Developed by:
      DISTek(R) Integration, Inc.
      6612 Chancellor Drive Suite 600
      Cedar Falls, IA 50613
      Tel: 319-859-3600
*/
/******************************************************************************/
