/******************************************************************************/
/*!
   \file
      This file defines the assert interface used for Win32

   \copyright
      Copyright (C) 2015  DISTek Integration, Inc.  All Rights Reserved.

   \author
      Chris Thatcher
*/
/******************************************************************************/


#ifndef GTEST

/******************************************************************************/
/*!
   \brief  Variadic macro to add filename, and line number to ASSERT print statement

   \param  format  printf-style format string (from ASSERT statement)
   \param  ...     Variables referenced in format string
*/
/******************************************************************************/
#define ASSERT_AddArgs(format, ...)  Assert_printf("%s  line %d:  " format, __FILE__, __LINE__, __VA_ARGS__)

/******************************************************************************/
/*!
   \brief    Win32 implementation of ASSERT() macro to expose violations during testing.

   \param  is_true    Boolean expression that is expected to be TRUE
   \param  params     Parenthesized parameter list for printf-style function call
*/
/******************************************************************************/
#define ASSERT(is_true, params)  if(!(is_true)) {ASSERT_AddArgs params;}

extern void Assert_printf(const char *format, ...);


#endif//GTEST

/******************************************************************************/
/*
   Copyright (C) 2015  DISTek Integration, Inc.  All Rights Reserved.

   Developed by:
      DISTek(R) Integration, Inc.
      6612 Chancellor Drive Suite 600
      Cedar Falls, IA 50613
      Tel: 319-859-3600
*/
/******************************************************************************/
