/******************************************************************************/
/*!
   \file
      This file implements the assert interface used for Win32

   \copyright
      Copyright (C) 2015  DISTek Integration, Inc.  All Rights Reserved.

   \author
      Chris Thatcher
*/
/******************************************************************************/


#include "Platform.h"
//#include "Foundation.h"
//#include "Display.h"
#include <assert.h>
#include <stdio.h>
#include <stdarg.h>


/******************************************************************************/
/*!
   \brief  Print a string to StandardOutput

   \param  string  C string containing error information
*/
/******************************************************************************/
static void Assert_print(const char *string)
{
   (void)string;
   //Pipe_InsertString(StandardOutput, string);
   assert(FALSE);
}


/******************************************************************************/
//! Assert_printf
/******************************************************************************/
void Assert_printf(const char *format, ...)
{
   va_list args;
   char buffer[200];

   va_start (args, format);
   vsprintf(buffer, format, args);
   va_end (args);

   Assert_print(buffer);
}


/******************************************************************************/
/*
   Copyright (C) 2015  DISTek Integration, Inc.  All Rights Reserved.

   Developed by:
      DISTek(R) Integration, Inc.
      6612 Chancellor Drive Suite 600
      Cedar Falls, IA 50613
      Tel: 319-859-3600
*/
/******************************************************************************/
