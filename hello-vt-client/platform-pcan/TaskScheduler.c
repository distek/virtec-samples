/******************************************************************************/
/*!
   \file
      This file implements task management for WIN32

   \copyright
      Copyright (C) 2012  DISTek Integration, Inc.  All Rights Reserved.

   \author
      Chris Thatcher
*/
/******************************************************************************/


#include "Platform.h"


//! Extern init routines
#define INIT(fn)                   extern void fn(void);
//! Extern init routines
#define TASK(fn,period,priority)   extern void fn(void);
//! Extern init routines
#define EXIT(fn)                   extern void fn(void);
#include "ScheduleTasksHere.h"
#undef INIT
#undef TASK
#undef EXIT


/******************************************************************************/
/*!
   \brief    Thread that runs a periodic task
*/
/******************************************************************************/
#define TASK(fn,period,priority)  static DWORD fn##_fn(void *unused) \
{                                                                     \
   (void)unused;                                                      \
   while(!PowerDown)                                                  \
   {                                                                  \
      fn();                                                           \
      Sleep(period);                                                  \
   }                                                                  \
                                                                      \
   ExitThread(0);                                                     \
   return 0;                                                          \
}
#include "ScheduleTasksHere.h"
#undef TASK


typedef struct
{
   DWORD      (*ScheduledTask)(void *unused);
   Priority_T  Priority;
   HANDLE      Handle;
} TASK_T;


static TASK_T ListOfTasks[TASK_LASTTASK] =
   {
      //! Define list of tasks
      #define TASK(fn,period,priority)  {fn##_fn,priority,NULL},
      #include "ScheduleTasksHere.h"
      #undef TASK
   };


bool_t UseSwitchedPower;
static Mutex_T  RunTasks = MAKE_Mutex_T(PRIORITY_MAX);
bool_t  ExitTriggered = FALSE;
bool_t  PowerDown = FALSE;


/******************************************************************************/
/*!
   \brief    Determines the state of switched power

   \return  bool_t
   \retval  TRUE   Switched power is on
   \retval  FALSE  Switched power is off
*/
/******************************************************************************/
bool_t SwitchedPower_IsOn(void)
{
   SYSTEM_POWER_STATUS  power_status;

   (void)GetSystemPowerStatus(&power_status);

   return (power_status.ACLineStatus == 1);
}


/******************************************************************************/
/*!
   \brief    Initialize the solution

   \details  Calls all init tasks to initialize the solution
*/
/******************************************************************************/
void TaskScheduler_Init(void)
{
   HANDLE      process;

   PowerDown = FALSE;

   Mutex_Init(&RunTasks);

   // Limit the processing to a single processor
   process = GetCurrentProcess();
//   (void)SetProcessAffinityMask(process, 1);

   // Initialize the Priority module
   (void)SetPriorityClass(process, REALTIME_PRIORITY_CLASS);

   // Prevent Windows from dynamically changing the priority of processes/threads
   if(!SetProcessPriorityBoost(process, TRUE))
   {
      //printf("FAIL!\n");
   }

   // Set the thread priority to make this is a background task
   (void)SetThreadPriority(GetCurrentThread(), PL_0);

   //! Run the init routines
   #define INIT(fn)  fn();
   #include "ScheduleTasksHere.h"
   #undef INIT

   // Enter critical section
   Mutex_Get(&RunTasks);

   for(unsigned int i=0; i<(sizeof(ListOfTasks)/sizeof(TASK_T)); i++)
   {
      // Dispatch the task priority levels (but they won't run until we exit critical)
      ListOfTasks[i].Handle = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)ListOfTasks[i].ScheduledTask, NULL, 0, NULL);

      // Set the task priority levels
      (void)SetThreadPriority(ListOfTasks[i].Handle, ListOfTasks[i].Priority);
   }

   Mutex_Release(&RunTasks);

   while(!ExitTriggered && (!UseSwitchedPower || SwitchedPower_IsOn()))
   {
      Sleep(50);
   }
   Sleep(1000);
   PowerDown = TRUE;
   Sleep(200);

   //! Run the exit routines
   #define EXIT(fn)  fn();
   #include "ScheduleTasksHere.h"
   #undef EXIT

   return;
}


TaskName_T TaskScheduler_GetCurrentTaskName(void)
{
   TaskName_T  i;

   for(i=0; i<TASK_LASTTASK; i++)
   {
      if(GetCurrentThreadId() == GetThreadId(ListOfTasks[i].Handle))
      {
         return i;
      }
   }

   return i;
}


/******************************************************************************/
/*
   Copyright (C) 2012  DISTek Integration, Inc.  All Rights Reserved.

   Developed by:
      DISTek(R) Integration, Inc.
      6612 Chancellor Drive Suite 600
      Cedar Falls, IA 50613
      Tel: 319-859-3600
*/
/******************************************************************************/
