/******************************************************************************/
/*!
   \file
      This file implements the priority interface used for Win32

   \copyright
      Copyright (C) 2016  DISTek Integration, Inc.  All Rights Reserved.

   \author
      Chris Thatcher
*/
/******************************************************************************/


#include "Platform.h"


//! One additional priority level used to "disable interrupts" under WIN32
#define  PL_15   (15)


/******************************************************************************/
/*!
   \brief    Initialize the Mutex

   \param    mutex  Pointer to the mutex to operate on
*/
/******************************************************************************/
void Mutex_Init(Mutex_T *mutex)
{
   if(mutex->Mutex == NULL)
   {
      mutex->Mutex = CreateMutex(NULL, FALSE, NULL);
   }
}


/******************************************************************************/
/*!
   \brief    Initialize the Mutex by copying info from another Mutex

   \param    mutex_to_init    Mutex to initialize
   \param    copy_from_mutex  Mutex to get data from
*/
/******************************************************************************/
void Mutex_CopyInit(Mutex_T *mutex_to_init, const Mutex_T *copy_from_mutex)
{
   if(mutex_to_init->Mutex == NULL)
   {
      mutex_to_init->Mutex = CreateMutex(NULL, FALSE, NULL);
   }
   mutex_to_init->Base = PL_0;
   mutex_to_init->Ceiling = copy_from_mutex->Ceiling;
}


/******************************************************************************/
/*!
   \brief    Grab mutex for resource

   \details  Grabs the mutex and implements priority ceiling protocol.

   \param    mutex  Pointer to the mutex
*/
/******************************************************************************/
void Mutex_Get(Mutex_T *mutex)
{
   Priority_T  base;
   Priority_T  ceiling;
   HANDLE      current_thread = GetCurrentThread();

   ASSERT(mutex->Mutex != NULL, ("Attempting to get uninitialized mutex\n"));

   base = GetThreadPriority(current_thread);

   // Increase the ceiling priority by 1 to ensure that tasks that are also
   // of the ceiling priority cannot interrupt
   ceiling = mutex->Ceiling + 1;

   // Ensure that the ceiling parameter will be valid
   if(ceiling < PL_1)
   {
      ceiling = PL_1;
   }
   if(ceiling > PL_14)
   {
      ceiling = PL_15;
   }

   // Boost the priority level (Priority Ceiling Protocol)
   (void)SetThreadPriority(current_thread, ceiling);

   // Grab the mutex
   ASSERT(WaitForSingleObject(mutex->Mutex, INFINITE) == WAIT_OBJECT_0, ("Internal issue: Returned without getting mutex\n"));
   {
      ASSERT(mutex->Locked == FALSE, ("Mutex already locked by this thread!\n"));
      // Got the Mutex
      mutex->Base = base;
      mutex->LockedBy = TaskScheduler_GetCurrentTaskName();
      mutex->Locked = TRUE;
   }
}


/******************************************************************************/
/*!
   \brief    Release the Mutex

   \details  Releases the mutex and restores the task to the base priority.

   \param    mutex  Pointer to the mutex
*/
/******************************************************************************/
void Mutex_Release(Mutex_T *mutex)
{
   Priority_T  base;
   HANDLE      current_thread = GetCurrentThread();

   ASSERT(mutex->Mutex != NULL, ("Attempting to release uninitialized mutex\n"));

   // Get local copy before releasing the mutex
   base = mutex->Base;

   ASSERT(mutex->Locked == TRUE, ("Mutex not locked!\n"));

   mutex->LockedBy = TASK_LASTTASK;
   mutex->Locked = FALSE;

   // Release the mutex
   ASSERT(ReleaseMutex(mutex->Mutex), ("Mutex attempted to be released by non-owning thread\n"));

   // Restore the priority level
   (void)SetThreadPriority(current_thread, base);
}


/******************************************************************************/
/*
   Copyright (C) 2016  DISTek Integration, Inc.  All Rights Reserved.

   Developed by:
      DISTek(R) Integration, Inc.
      6612 Chancellor Drive Suite 600
      Cedar Falls, IA 50613
      Tel: 319-859-3600
*/
/******************************************************************************/
