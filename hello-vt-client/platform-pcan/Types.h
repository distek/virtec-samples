/******************************************************************************/
/*!
   \file
      This file defines the generic types that are used by the libraries

   \copyright
      Copyright (C) 2012  DISTek Integration, Inc.  All Rights Reserved.

   \author
      Chris Thatcher
*/
/******************************************************************************/


#ifndef BASE_TYPE
#define BASE_TYPE(from,to)  typedef from to
#endif //BASE_TYPE

#ifndef RAW_TYPE
#define RAW_TYPE(type)  type
#endif //RAW_TYPE

#ifndef TYPEDEF
#define TYPEDEF(from,to)    typedef from to
#endif //TYPEDEF

#ifndef INTRINSIC
#define INTRINSIC(value)  (value)
#endif //INTRINSIC

#ifndef CAST
#define CAST(to_type,value)  ((to_type)(value))
#endif //CAST

#ifndef CAST_MASK
#define CAST_MASK(to_type,value,mask)  ((to_type)((value) & (mask)))
#endif //CAST_MASK

#ifndef ARRAY
#define ARRAY(data_type,name,index_type,size)  data_type name[size]
#endif //ARRAY

#ifndef ARRAY_PTR
#define ARRAY_PTR(data_type,name,index_type)  data_type *name
#endif //ARRAY_PTR

#ifndef SWITCH
#define SWITCH(x)  switch(x)
#endif //SWITCH

#ifndef NO_BREAK_LINT_COMMENT
#define NO_BREAK_LINT_COMMENT
#endif //NO_BREAK_LINT_COMMENT


#ifdef __LINT__

//! Unsigned 8-bit integer
BASE_TYPE(unsigned int, uint8_t);

//! Unsigned 16-bit integer
BASE_TYPE(unsigned int, uint16_t);

//! Unsigned 32-bit integer
BASE_TYPE(unsigned long, uint32_t);

//! Signed 8-bit integer
BASE_TYPE(signed int, int8_t);

//! Signed 16-bit integer
BASE_TYPE(signed int, int16_t);

//! Signed 32-bit integer
BASE_TYPE(signed long, int32_t);

//! IEEE 754 32-bit floating point type
BASE_TYPE(float, float32_t);

#else //!__LINT__

//! Unsigned 8-bit integer
BASE_TYPE(unsigned char, uint8_t);

//! Unsigned 16-bit integer
BASE_TYPE(unsigned short, uint16_t);

//! Unsigned 32-bit integer
BASE_TYPE(unsigned long, uint32_t);

//! Signed 8-bit integer
BASE_TYPE(signed char, int8_t);

//! Signed 16-bit integer
BASE_TYPE(signed short, int16_t);

//! Signed 32-bit integer
BASE_TYPE(signed long, int32_t);

//! IEEE 754 32-bit floating point type
BASE_TYPE(float, float32_t);

#endif //__LINT__


//! This is a boolean type.  Holds values TRUE and FALSE.
typedef unsigned char bool_t;

//! Return type of SIZEOF()  (Note:  Size_T MUST be at least 32-bit)
typedef unsigned long  Size_T;


//! Dependent on the processor architecture.  Typically 8-bits.
BASE_TYPE(unsigned char, MinAddressable_T);
//! Macro for explicitly identifying the number of bits in MinAddressable_T
#define MIN_ADDRESSABLE_SIZE_BITS  8
//! Macro for explicitly casting to MinAddressable_T - ensures proper truncation
#define CAST_MinAddressable_T(x)  ((MinAddressable_T)(x))

#define DEFAULT_MEMORY_CLASS
#define QUEUE_MEMORY_CLASS
#define GENERAL_MEMORY_CLASS

#define PTR_TO_VAR(ptr_t, mem_class, ptr_class) ptr_t *
#define CONST_PTR_TO_VAR(ptr_t, mem_class, ptr_class) ptr_t * const
#define PTR_TO_CONST(ptr_t, mem_class, ptr_class) const ptr_t *
#define MAKE_MemoryPointer_T(address) {(PTR_TO_VAR(void, GENERAL_MEMORY_CLASS, DEFAULT_MEMORY_CLASS)) address}

//! User-defined structure to reference arbitrary memory locations
typedef struct MemoryPointer_S
{
   //! Pointer to location
   PTR_TO_VAR(void, GENERAL_MEMORY_CLASS, DEFAULT_MEMORY_CLASS) Address;
} MemoryPointer_T;


/******************************************************************************/
/*
   Copyright (C) 2012  DISTek Integration, Inc.  All Rights Reserved.

   Developed by:
      DISTek(R) Integration, Inc.
      6612 Chancellor Drive Suite 600
      Cedar Falls, IA 50613
      Tel: 319-859-3600
*/
/******************************************************************************/
