#ifndef PLATFORM_H
#define PLATFORM_H
/******************************************************************************/
/*!
   \file
      Defines interface to the platform

   \copyright
      Copyright (C) 2012  DISTek Integration, Inc.  All Rights Reserved.

   \author
      Chris Thatcher
*/
/******************************************************************************/


#include "Types.h"
#if ((!defined(STRICT_TYPE)) && !defined(__LINT__))
#include <windows.h>
#else
typedef int HANDLE;
#endif
#include "TaskScheduler.h"
#include "Mutex.h"
#include "WIN32_Assert.h"

#define SoftwareVersion(P,M,m,b)  P "," #M "." #m "." #b "#"
#define AUX_CONTROL
#define AUX_INPUT
#define AUX_FUNCTION


/******************************************************************************/
/*
   Copyright (C) 2012  DISTek Integration, Inc.  All Rights Reserved.

   Developed by:
      DISTek(R) Integration, Inc.
      6612 Chancellor Drive Suite 600
      Cedar Falls, IA 50613
      Tel: 319-859-3600
*/
/******************************************************************************/
#endif //PLATFORM_H
