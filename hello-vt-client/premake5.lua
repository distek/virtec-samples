solution "hello-vt-client"
   configurations {"Debug", "Release" }
   system("windows")
   systemversion("10.0.15063.0")
   language "C"

   project "vt-win32"
      kind "ConsoleApp"
      defines { "WIN32", "CONSOLE", "_LIB" }
      files { "*.c", "*.h" }
      includedirs { "./platform-pcan", "./lib" }
      links { "vt-client", "Foundation.lib", "VTClient.lib", "platform" }
	  libdirs { "lib" }

   project "vt-client"
      kind "StaticLib"
      defines { "_LIB" }
      files { "./vt-client/*.c", "./vt-client/*.h" }
      includedirs { "./platform-pcan", ".", "./lib" }
      links "Platform"

   project "platform"
      kind "StaticLib"
      defines { "_LIB" }
      files { "./Platform/*.c", "./Platform/*.h" }
      includedirs { ".", "./lib" }

   project "can-pcan"
      kind "StaticLib"
      defines { "_LIB", "_CRT_SECURE_NO_WARNINGS", "WIN32" }
      includedirs { ".", "./lib", "./Platform" }
      files { "./can-pcan/*.c", "./can-pcan/*.h" }

   project "can-udp"
      kind "StaticLib"
      defines { "_LIB", "_CRT_SECURE_NO_WARNINGS", "WIN32" }
      includedirs { ".", "./lib", "./Platform" }
      files { "./can-udp/*.c", "./can-udp/*.h" }

   project "can-vc3"
      kind "StaticLib"
      defines { "_LIB", "_CRT_SECURE_NO_WARNINGS", "WIN32" }
      includedirs { ".", "./lib", "./Platform" }
      files { "./can-vc3/*.c", "./can-vc3/*.h" }
