/******************************************************************************/
/*
NOTES: Initialize, task, interrupt handler, and queue data packet functions for the CAN
network.
*/
/******************************************************************************/

#include "Platform.h"
#include "Foundation.h"
#include "CAN.h"
#include "Solution.h"
#include <stdio.h>
#include "PCANBasic.h"

#define _DEF_ARG

static void InitConnection();

// Declare everything needed for the Network Instance
static void CAN_RxISR(struct CAN_S *can_channel);
static bool_t SendMsg(CAN_Packet_T *packet);
static void PrintPacket(const CAN_Packet_T *packet, uint8_t bus_num);

extern struct CAN_S  CAN1;

DWORD start_time;

/******************************************************************************/
/*!
   \brief   Initializes CAN
*/
/******************************************************************************/
void CAN_Init(struct CAN_S *can_channel)
{
   start_time = GetTickCount();

   // Initialize the Network for CAN1
   //Network_Init(&can_channel->Network);
   Network_Init(&CAN1.Network);

   // Initialize the TX Queue for CAN1
   //Queue_Init(&can_channel->TxQueue);
   Queue_Init(&CAN1.TxQueue);
   InitConnection();
}

static void InitConnection()
{
   TPCANStatus result;
   char msg[256];

   result = CAN_Initialize(PCAN_USBBUS1, PCAN_BAUD_250K, 0, 0, 0);
   if (result != PCAN_ERROR_OK)
   {
      CAN_GetErrorText(result, 0, msg);
      printf("Error starting CAN: %s\n", msg);
   }
   else
   {
      printf("Successfully initialized PCAN\n");
   }
}



/******************************************************************************/
/*!
   \brief   CAN task
*/
/******************************************************************************/
void CAN_Task(struct CAN_S *can_channel)
{
	MyCANQueueEntry_T  entry;

	// Run the Network task (for CAN1)
	//Network_Task(&can_channel->Network);
	Network_Task(&CAN1.Network);

	// Process received packets
	//CAN_RxISR(can_channel);
	CAN_RxISR(&CAN1);

	// While there are more entries in the TX queue
	//while(Queue_CopyData(&can_channel->TxQueue, &entry, sizeof(MyCANQueueEntry_T)))
	while (Queue_CopyData(&CAN1.TxQueue, &entry, sizeof(MyCANQueueEntry_T)))
	{
		// Convert entry->Packet to hardware type
		if (SendMsg(&entry.Packet))
		{
			// Call the callback
			if (entry.Callback.Function != NULL)
			{
				entry.Callback.Function(&entry.Callback);
			}

			// Pass the TX message back through the handlers
			//Network_PacketHandler(&entry.Packet, &can_channel->Network);
			Network_PacketHandler(&entry.Packet, &CAN1.Network);

			// Remove the TX packet from the queue (since Queue_GetFront worked, we know there's an entry)
			//(void)Queue_Remove(&can_channel->TxQueue, sizeof(MyCANQueueEntry_T));
			(void)Queue_Remove(&CAN1.TxQueue, sizeof(MyCANQueueEntry_T));
		}
		else // send to network failed
		{
			// Quit processing the TX queue (try again later)
			break;
		}
	}
}

static bool_t SendMsg(CAN_Packet_T *packet)
{
   TPCANMsg msg;
   TPCANStatus result;
   char errorMessage[256];

   packet->Header.Direction = ISOBUS_TX;
   PrintPacket(packet, 1);

   msg.ID = packet->Header.Identifier;
   msg.MSGTYPE = PCAN_MESSAGE_EXTENDED;
   msg.LEN = packet->DLC;
   msg.DATA[0] = packet->Data[0];
   msg.DATA[1] = packet->Data[1];
   msg.DATA[2] = packet->Data[2];
   msg.DATA[3] = packet->Data[3];
   msg.DATA[4] = packet->Data[4];
   msg.DATA[5] = packet->Data[5];
   msg.DATA[6] = packet->Data[6];
   msg.DATA[7] = packet->Data[7];

   result = CAN_Write(PCAN_USBBUS1, &msg);
   if (result != PCAN_ERROR_OK)
   {
      CAN_GetErrorText(result, 0, errorMessage);
      printf("Error writing CAN message: %s\n", msg);
      return FALSE;
   }
   return TRUE;
}

/******************************************************************************/
/*!
   \brief   Initializes CAN
*/
/******************************************************************************/
void CAN_Uninit(struct CAN_S *can_channel)
{
	// Open the handle to Eaton CAN channel
	//capi_can_close(can_channel->can_handle);
}

/******************************************************************************/
/*!
   \brief   Queues CAN1 Tx packets to be sent on bus

   \param   packet      Packet to be queued for transmission
   \param   callback    Callback when packet is sent
   \param   can_channel pointer to CAN_S struct representing CAN bus

   \return  bool_t

   \retval  TRUE     Packet was successfully sent
   \retval  FALSE    Packet failed to send
*/
/******************************************************************************/
bool_t CAN_SendPacket(const CAN_Packet_T *packet, const ISOBUS_Callback_T *callback, struct CAN_S *can_channel)
{
	MyCANQueueEntry_T  queue_entry;

	// Consolidate packet and callback into queue entry
	Utility_MemoryCopy(&queue_entry.Packet, packet, sizeof(CAN_Packet_T));
	Utility_MemoryCopy(&queue_entry.Callback, callback, sizeof(ISOBUS_Callback_T));

	// Insert queue entry into queue
	//return Queue_Insert(&can_channel->TxQueue, &queue_entry, sizeof(MyCANQueueEntry_T));
	return Queue_Insert(&CAN1.TxQueue, &queue_entry, sizeof(MyCANQueueEntry_T));
}

/******************************************************************************/
/*!
\brief   Handles receive interrupt
*/
/******************************************************************************/
static void CAN_RxISR(struct CAN_S *can_channel)
{
   TPCANMsg msg;
   TPCANTimestamp timestamp;
   TPCANStatus result;
   char strMsg[256];
   CAN_Packet_T packet;
   uint8_t i;

   do
   {
      // Check the receive queue for new messages
      //
      result = CAN_Read(PCAN_USBBUS1, &msg, &timestamp);
      if (result != PCAN_ERROR_QRCVEMPTY)
      {
         // Process the received message
         //
         //MessageBox("A message was received");
         //ProcessMessage(msg)
         packet.Header.Direction = ISOBUS_RX;
         packet.Header.Identifier = msg.ID;
         packet.DLC = msg.LEN;
         for (i = 0; i < packet.DLC; i++)
         {
            packet.Data[i] = msg.DATA[i];
         }
         PrintPacket(&packet, 1);
         Network_PacketHandler(&packet, &CAN1.Network);
      }
      else
      {
         // An error occurred, get a text describing the error and show it
         // and handle the error
         //
         CAN_GetErrorText(result, 0, strMsg);
         //MessageBox(strMsg);
         // Here can be decided if the loop has to be  terminated (eg. the bus
         // status is  bus-off)
         //
         //HandleReadError(result);
      }
      // Try to read a message from the receive queue of the PCAN-USB, Channel 1,
      // until the queue is empty
      //
   } while ((result & PCAN_ERROR_QRCVEMPTY) != PCAN_ERROR_QRCVEMPTY);


}

static void PrintPacket(const CAN_Packet_T *packet, uint8_t bus_num)
{
	char                buffer[64];
	char                data[25];
	const char*         direction_text[] = { "RX", "TX", "??" };
	ISOBUS_Direction_T  direction;
	DWORD               Msg_Time = GetTickCount() - start_time;

	// Print the Data
	data[0] = '\0';
	for (Size_T i = 0; i < packet->DLC; i++)
	{
		// Write previous data bytes and current data byte to buffer
		sprintf(buffer, "%s %02X", data, packet->Data[i]);

		// Transfer from buffer back to data
		sprintf(data, "%s", buffer);
	}

	direction = packet->Header.Direction;
	if (direction > ISOBUS_TX)
	{
		direction = (ISOBUS_Direction_T)2;
	}

	// Print the message contents to the screen
	sprintf(buffer, "%6lu %u %s %08X  %X %s\n",
		Msg_Time,
		bus_num,
		direction_text[direction],
		(int)packet->Header.Identifier,
		(int)packet->DLC,
		data);

	//(void)Pipe_InsertString(StandardOutput, buffer);
	printf("%s", buffer);
}
