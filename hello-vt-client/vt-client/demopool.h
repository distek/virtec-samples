/******************************************************************************/
/*
   WARNING:  This file is auto-generated using ConvertIOP.pl.  Any modifications
             to this file will be overwritten the next time the script is run.
*/
/******************************************************************************/


// Named defines for Object Pool objects:
#define WorkingSet_0                             ((ObjectID_T)    0)
#define DataMask_1000                            ((ObjectID_T) 1000)
#define OutputString_11000                       ((ObjectID_T)11000)
#define OutputString_11001                       ((ObjectID_T)11001)
#define OutputString_11002                       ((ObjectID_T)11002)
#define FontAttributes_23000                     ((ObjectID_T)23000)
#define HeaderFont                               ((ObjectID_T)23001)
#define TextFont                                 ((ObjectID_T)23002)


extern ObjectPool_T DemoApp_ObjectPool;
