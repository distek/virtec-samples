/******************************************************************************/
/*!
   \file
      Sample VT interaction

   \copyright
      Copyright (C) 2015  DISTek Integration, Inc.  All Rights Reserved.
*/
/******************************************************************************/


#include "Platform.h"
#include "Foundation.h"
#include "VTClient.h"
#include "Solution.h"
#include "demoapp_ISOBUS.h"
#include "demopool.h"
#include <math.h>

typedef enum DemoState_E
{
   WAIT_VT,
   CONNECT_VT,
   SEND_OP,
   SEND_END_OP,
   OPERATOR_INTERACTION,
   DELETE_OP,
   DISCONNECT_VT,
   DEMO_IDLE
} DemoState_T;


static DemoState_T      DemoApp_DemoState;
static VT_T      *DemoVT;

// Declare and initialize the simulator timer
SoftwareTimer_T DemoApp_Timer = MAKE_SoftwareTimer_T();

#define STOP_VT_STEP           0        // IDLE for the VT command sequences
#define START_VT_STEP          1        // first VT command sequence index

// display variables
static ObjectID_T ActiveMaskID;      // object ID of the currently active display mask

void DemoApp_Init(void);               // VT_Demo task init
void DemoApp_Task(void);               // VT_Demo task
static void InitSimulation(void);   // init VT simulation data values
static void RunSimulation(void);    // run TANK or MESSAGE simulation

/******************************************************************************/
/*!
   \brief    Heartbeat task initialization
*/
/******************************************************************************/
void DemoApp_Init(void)
{
   // Init the object pool
   ObjectPool_Init(&DemoApp_ObjectPool);

   DemoApp_DemoState = WAIT_VT;

   (void)SoftwareTimer_Register(&Solution_SwTimerList, &DemoApp_Timer);
}

/******************************************************************************/
/*!
   \brief    Task to send heartbeat message
*/
/******************************************************************************/
void DemoApp_Task(void)
{
#if ((!defined(STRICT_TYPE)) && !defined(__LINT__))
   if(ExitTriggered && (DemoVT != NULL))
   {
      DemoApp_DemoState = DISCONNECT_VT;
   }
#endif

   switch(DemoApp_DemoState)
   {
   // wait here until the app has sees a VT on the bus
   case WAIT_VT:
      if(VT_NextVT(&DemoApp_VTClient, &DemoVT))
      {
         DemoApp_DemoState = CONNECT_VT;
      }
      break;
   // wait here until the app has connected to a VT
   case CONNECT_VT:
      if(VT_Connect(&DemoApp_VTClient, DemoVT))
      {
         DemoApp_DemoState = SEND_OP;
      }
      break;
   // wait here until app has sent it's object pool to the VT
   case SEND_OP:
      if(VT_SendObjectPool(&DemoApp_VTClient, DemoVT, &DemoApp_ObjectPool))
      {
         DemoApp_DemoState = SEND_END_OP;
      }
      break;
   // wait here until VT has accepted the app's object pool
   case SEND_END_OP:
      if(DemoVT->ObjectPool.State == VT_OP_OPERATOR_INTERACTION)
      {
         DemoApp_DemoState = OPERATOR_INTERACTION;
         InitSimulation();
      }
      else if(DemoVT->ObjectPool.State == VT_OP_IDLE)
      {
         // Move to the next VT to connect with
         DemoApp_DemoState = DELETE_OP;
      }
      break;
   // app's object pool is loaded and available
   case OPERATOR_INTERACTION:
      // If we lost connection with the VT
      if(SoftwareTimer_Get(&DemoVT->Status.Timer) == TIMER_EXPIRED)
      {
         // Search for a new VT to connect with
         DemoApp_DemoState = WAIT_VT;
      }
      else if(DemoVT->ObjectPool.State == VT_OP_IDLE)
      {
         // Move to the next VT to connect with
         DemoApp_DemoState = WAIT_VT;
      }
      else
      {
         RunSimulation();
      }
      break;
   // wait here while the VT delets the object pool,
   // and disconnects from the TV (Next VT)
   case DELETE_OP:
      if(VT_Disconnect(&DemoApp_VTClient, DemoVT))
      {
         DemoApp_DemoState = WAIT_VT;
      }
      break;
   // wait here while the VT deletes the object pool
   // and disconnects from the TV (Not Next VT)
   case DISCONNECT_VT:
      if(VT_Disconnect(&DemoApp_VTClient, DemoVT))
      {
         DemoApp_DemoState = DEMO_IDLE;
      }
      break;
   // wait here for power-down
   case DEMO_IDLE:
   default:
      break;
   }
}

/******************************************************************************/
/*!
   \brief    Initialize the application data
*/
/******************************************************************************/
static void InitSimulation(void)
{
}


/******************************************************************************/
/*!
   \brief    Run the UI logic of your application
*/
/******************************************************************************/
static void RunSimulation(void)
{
   if (SoftwareTimer_Get(&DemoApp_Timer) == TIMER_EXPIRED)
   {
      SoftwareTimer_Set(&DemoApp_Timer, milliseconds(500));
      ActiveMaskID = DemoVT->Status.VisibleDataAlarmMask;

   } // endof if(Simulator_Timer Expired)
}

/******************************************************************************/
/*
   Copyright (C) 2015  DISTek Integration, Inc.  All Rights Reserved.

   Developed by:
      DISTek(R) Integration, Inc.
      6612 Chancellor Drive Suite 600
      Cedar Falls, IA 50613
      Tel: 319-859-3600
*/
/******************************************************************************/
