/******************************************************************************/
/*
   \file
      Schedule the tasks for SampleDemoApp

   \copyright
      Copyright (C) 2015  DISTek Integration, Inc.  All Rights Reserved.

   \author
      Chris Thatcher
*/
/******************************************************************************/


#ifndef INIT
//! Used for scheduling a function to run before any TASK
#define INIT(fn)
#define UNDEF_INIT
#endif //INIT

#ifndef TASK
//! Used for scheduling a function to execute periodically
#define TASK(fn,period,priority)
#define UNDEF_TASK
#endif //TASK

#ifndef EXIT
//! Used for scheduling tasks to run at shutdown
#define EXIT(fn)
#define UNDEF_EXIT
#endif //EXIT


#ifndef LIBRARY_INIT
//! Used for scheduling a function to run before any TASK
#define LIBRARY_INIT(fn,state)  INIT(DemoApp_##fn)
#define UNDEF_LIBRARY_INIT
#endif //LIBRARY_INIT

#ifndef LIBRARY_TASK
//! Used for scheduling a function to execute periodically
#define LIBRARY_TASK(fn,state,period,priority)  TASK(DemoApp_##fn,period,priority)
#define UNDEF_LIBRARY_TASK
#endif //LIBRARY_TASK

#ifndef LIBRARY_EXIT
//! Used for scheduling tasks to run at shutdown
#define LIBRARY_EXIT(fn,state)  EXIT(DemoApp_##fn)
#define UNDEF_LIBRARY_EXIT
#endif //LIBRARY_EXIT


/******************************************************************************/
// BEGIN:  Task Scheduling
/******************************************************************************/
//! \cond  SKIP


//   Init function
//INIT(Heartbeat_Init)
INIT(DemoApp_Init)
INIT(DemoApp_ISOBUS_Init)
//LIBRARY_INIT(Foundation_Init, &DemoApp_Foundation)
LIBRARY_INIT(VTClient_Init,   &DemoApp_VTClient)

//   Task function,                      period (ms),  priority
//TASK(Heartbeat_Task,                              10,   PL_6)  // Send a "heartbeat" message
TASK(DemoApp_Task,                                   20,   PL_4)
LIBRARY_TASK(Foundation_Task, &DemoApp_Foundation,   80,  PL_10)
LIBRARY_TASK(VTClient_Task,   &DemoApp_VTClient,     40,   PL_8)


//   Exit function
//EXIT(Heartbeat_Uninit)
LIBRARY_EXIT(Foundation_Uninit, &DemoApp_Foundation)
LIBRARY_EXIT(VTClient_Uninit,   &DemoApp_VTClient)


//! \endcond
/******************************************************************************/
// END:    Task Scheduling
/******************************************************************************/


#ifdef UNDEF_LIBRARY_INIT
#undef LIBRARY_INIT
#undef UNDEF_LIBRARY_INIT
#endif
#ifdef UNDEF_LIBRARY_TASK
#undef LIBRARY_TASK
#undef UNDEF_LIBRARY_TASK
#endif
#ifdef UNDEF_LIBRARY_EXIT
#undef LIBRARY_EXIT
#undef UNDEF_LIBRARY_EXIT
#endif

#ifdef UNDEF_INIT
#undef INIT
#undef UNDEF_LIBRARY_INIT
#endif
#ifdef UNDEF_TASK
#undef TASK
#undef UNDEF_LIBRARY_TASK
#endif
#ifdef UNDEF_EXIT
#undef EXIT
#undef UNDEF_LIBRARY_EXIT
#endif


/******************************************************************************/
/*
   Copyright (C) 2015  DISTek Integration, Inc.  All Rights Reserved.

   Developed by:
      DISTek(R) Integration, Inc.
      6612 Chancellor Drive Suite 600
      Cedar Falls, IA 50613
      Tel: 319-859-3600
*/
/******************************************************************************/
