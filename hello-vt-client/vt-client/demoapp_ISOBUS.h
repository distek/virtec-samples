/******************************************************************************/
/*!
   \file
      Sample implementation of required application functions

   \copyright
      Copyright (C) 2012  DISTek Integration, Inc.  All Rights Reserved.
*/
/******************************************************************************/


//! \brief   Send packet on CAN interface and enforce Address Claim rules
//! Foundation data structure for App1
extern Foundation_T DemoApp_Foundation;
extern VTClient_T DemoApp_VTClient;

//! Handler for ISOBUS Compliance Certification message
extern void Certification_Handler(const ISOBUS_Packet_T *packet, const void *null);
extern void ProductId_Handler(ISOBUS_Message_T *message, ISOBUS_MessageEvent_T event, void *null);
extern void Functionality_Handler(ISOBUS_Message_T *message, ISOBUS_MessageEvent_T event, void *null);
extern void EcuId_Handler(ISOBUS_Message_T *message, ISOBUS_MessageEvent_T event, void *null);
extern void SoftwareId_Handler(ISOBUS_Message_T *message, ISOBUS_MessageEvent_T event, void *null);


/******************************************************************************/
/*
   Copyright (C) 2012  DISTek Integration, Inc.  All Rights Reserved.

   Developed by:
      DISTek(R) Integration, Inc.
      6612 Chancellor Drive Suite 600
      Cedar Falls, IA 50613
      Tel: 319-859-3600
*/
/******************************************************************************/
