/******************************************************************************/
/*!
   \file
      Allows customization of the number and size of pipes in the pipe
      collection

   \copyright
      Copyright (C) 2012  DISTek Integration, Inc.  All Rights Reserved.

   \author
      Chris Thatcher
*/
/******************************************************************************/


//! \cond  SKIP
#ifndef PIPE
#define PIPE(name, priority, size)
#endif //PIPE


//PIPE(name,priority,size)
PIPE(Pipe0, PRIORITY_MAX,    8)
PIPE(Pipe1, PRIORITY_MAX,    8)
PIPE(Pipe2, PRIORITY_MAX,    8)
PIPE(Pipe3, PRIORITY_MAX,    8)
PIPE(Pipe4, PRIORITY_MAX,  256)
PIPE(Pipe5, PRIORITY_MAX,  256)
PIPE(Pipe6, PRIORITY_MAX,  512)
PIPE(Pipe7, PRIORITY_MAX,  512)
PIPE(Pipe8, PRIORITY_MAX, 1785)
PIPE(Pipe9, PRIORITY_MAX, 1785)


#undef PIPE
//! \endcond


/******************************************************************************/
/*
   Copyright (C) 2012  DISTek Integration, Inc.  All Rights Reserved.

   Developed by:
      DISTek(R) Integration, Inc.
      6612 Chancellor Drive Suite 600
      Cedar Falls, IA 50613
      Tel: 319-859-3600
*/
/******************************************************************************/
