/******************************************************************************/
/*!
   \file
      Holds a list of the tasks that are to be scheduled.

   \copyright
      Copyright (C) 2016  DISTek Integration, Inc.  All Rights Reserved.

   \author
      Chris Thatcher
*/
/******************************************************************************/


#ifndef INIT
//! Used for scheduling a function to run before any TASK
#define INIT(fn)
#define UNDEF_INIT
#endif //INIT

#ifndef TASK
//! Used for scheduling a function to execute periodically
#define TASK(fn,period,priority)
#define UNDEF_TASK
#endif //TASK

#ifndef EXIT
//! Used for scheduling tasks to run at shutdown
#define EXIT(fn)
#define UNDEF_EXIT
#endif //EXIT


/******************************************************************************/
// BEGIN:  Task Scheduling
/******************************************************************************/
//! \cond  SKIP

//   Init function
INIT(Timers_Init)               // foundation task, 1st to initalize
INIT(Solution_SoftwareId_Init)  // foundation task, 2nd to initalize

//   Task function,  period (ms),  priority
TASK(Timers_Task,             10,      PL_8)  // foundation task

//   Exit function
EXIT(Solution_SoftwareId_Uninit)  // foundation task, 2nd to initalize

#ifdef UDP
#include "Platform_ScheduleTasksHere.h"
#else
#include "platform-pcan\Platform_ScheduleTasksHere.h"
#endif
#include "vt-client\demoapp_ScheduleTasksHere.h"

//! \endcond
/******************************************************************************/
// END:    Task Scheduling
/******************************************************************************/


#ifdef UNDEF_INIT
  #undef INIT
  #undef UNDEF_INIT
#endif

#ifdef UNDEF_TASK
  #undef TASK
  #undef UNDEF_TASK
#endif

#ifdef UNDEF_EXIT
  #undef EXIT
  #undef UNDEF_EXIT
#endif


/******************************************************************************/
/*
   Copyright (C) 2016  DISTek Integration, Inc.  All Rights Reserved.

   Developed by:
      DISTek(R) Integration, Inc.
      6612 Chancellor Drive Suite 600
      Cedar Falls, IA 50613
      Tel: 319-859-3600
*/
/******************************************************************************/
