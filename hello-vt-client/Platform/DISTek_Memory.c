/******************************************************************************/
/*!
   \file
      This file implements generic functions for operating on memory.

   \copyright
      Copyright (C) 2012  DISTek Integration, Inc.  All Rights Reserved.

   \author
      Chris Thatcher
*/
/******************************************************************************/


#include "Platform.h"
#include "Foundation.h"
#include "DISTek_Memory.h"


//! Possible memory operations
typedef enum MemoryOperation_E
{
   MEMORY_IDLE,
   MEMORY_READ,
   MEMORY_WRITE
}MemoryOperation_T;


//! Data needed to complete a memory session
typedef struct MemorySession_S
{
   MinAddressable_T  *Pointer;
   union
   {
      Pipe_WriteHandle_T  Write;
      Pipe_ReadHandle_T   Read;
   } Pipe;
   Size_T             Size;
   MemoryOperation_T  Operation;
   ISOBUS_Callback_T  Callback;
   Mutex_T            Mutex;
} MemorySession_T;


#ifndef NUM_MEM_SESSIONS
//! Maximum number of simultaneous memory sessions
#define NUM_MEM_SESSIONS  5
#endif //NUM_MEM_SESSIONS

//! Array of simultaneous memory sessions
static MemorySession_T  MemorySessions[NUM_MEM_SESSIONS];
static Mutex_T  MemoryMutex = MAKE_Mutex_T(PRIORITY_MAX);


/******************************************************************************/
/*!
   \details  Finds an available memory session and secures the mutex

   \param    session  [Out] available memory session

   \return  bool_t

   \retval  TRUE   Available memory session was found and mutex secured
   \retval  FALSE  No available memory sessions were found
*/
/******************************************************************************/
static bool_t FindOpenSessionAndGetMutex(MemorySession_T **session)
{
   bool_t           session_found = FALSE;
   MemorySession_T  *current_session;
   uint8_t i;

   // Search for an unused memory session
   current_session = MemorySessions;
   for(i=0; i<NUM_MEM_SESSIONS; i++, current_session++)
   {
      Mutex_Get(&current_session->Mutex);
      if(current_session->Operation == MEMORY_IDLE)
      {
         session_found = TRUE;
         (*session) = current_session;
         break;
      }
      Mutex_Release(&current_session->Mutex);
   }

   return session_found;
}


/******************************************************************************/
/*!
   \details  Processes a memory session

   \param    session  Memory session to process
*/
/******************************************************************************/
static void ProcessRead(MemorySession_T *session)
{
   // If theres another task reading this pipe
   if(Pipe_ReadIsOpen(session->Pipe.Write.Write))
   {
      // Detect the space available in the pipe
      Size_T size = Pipe_WriteSpaceAvailable(session->Pipe.Write);

#if (MIN_ADDRESSABLE_SIZE_BITS != 8)
      // Account for non-8-bit-addressable devices (rounds down to whole MinAddressable_T location
      size /= (MIN_ADDRESSABLE_SIZE_BITS/8);
      size *= (MIN_ADDRESSABLE_SIZE_BITS/8);
#endif

      // Limit the size to the total size
      if(size > session->Size)
      {
         size = session->Size;
      }

      // If we can insert all the data immediately
      if(Pipe_Insert(session->Pipe.Write, session->Pointer, size))
      {
         // Decrement the size and increment the pointer
         session->Size -= size;
         session->Pointer += (size / (MIN_ADDRESSABLE_SIZE_BITS/8));
      }
   }
   else
   {
      // No one to read the pipe - this session is finished.
      session->Operation = MEMORY_IDLE;
   }
}


/******************************************************************************/
/*!
   \details  Processes a memory session

   \param    session  Memory session to process
*/
/******************************************************************************/
static void ProcessWrite(MemorySession_T *session)
{
   // If our pipe is still open
   if(Pipe_ReadIsOpen(session->Pipe.Read.Read))
   {
      // Detect the space available in the pipe
      Size_T size = Pipe_ReadDataAvailable(session->Pipe.Read);

#if (MIN_ADDRESSABLE_SIZE_BITS != 8)
      // Account for non-8-bit-addressable devices (rounds down to whole MinAddressable_T location)
      size /= (MIN_ADDRESSABLE_SIZE_BITS/8);
      size *= (MIN_ADDRESSABLE_SIZE_BITS/8);
#endif

      // Limit the size to the total size
      if(size > session->Size)
      {
         size = session->Size;
      }

      // If we can copy all the data
      if(Pipe_CopyData(session->Pipe.Read, session->Pointer, size))
      {
         // And remove it from the pipe
         if(Pipe_Remove(session->Pipe.Read, size))
         {
            // Decrement the size and increment the pointer
            session->Size -= size;
            session->Pointer += (size / (MIN_ADDRESSABLE_SIZE_BITS/8));
         }
      }
   }
   else
   {
      // This session is finished.
      session->Operation = MEMORY_IDLE;
   }
}


/******************************************************************************/
/*!
   \details  Initializes the Memory driver
*/
/******************************************************************************/
void Memory_Init(void)
{
   uint8_t i;

   Mutex_Init(&MemoryMutex);
   for(i=0; i<NUM_MEM_SESSIONS; i++)
   {
      Mutex_CopyInit(&MemorySessions[i].Mutex, &MemoryMutex);
      Mutex_Get(&MemorySessions[i].Mutex);
      MemorySessions[i].Operation = MEMORY_IDLE;
      Mutex_Release(&MemorySessions[i].Mutex);
   }
}


/******************************************************************************/
/*!
   \details  Task for processing the memory sessions
*/
/******************************************************************************/
void Memory_Task(void)
{
   uint8_t i;
   MemorySession_T  *session;
   ISOBUS_Callback_T  callback;

   session = MemorySessions;

   for(i=0; i<NUM_MEM_SESSIONS; i++)
   {
      bool_t session_complete = FALSE;

      Mutex_Get(&session->Mutex);

      switch(session->Operation)
      {
      case MEMORY_READ:
         ProcessRead(session);
         break;

      case MEMORY_WRITE:
         ProcessWrite(session);
         break;

      case MEMORY_IDLE:
      default:
         break;
      }

      // If the memory transaction is complete
      if( (session->Operation != MEMORY_IDLE) &&
          (session->Size == 0)                )
      {
         // Get a local copy of the callback
         Utility_MemoryCopy(&callback, &session->Callback, sizeof(ISOBUS_Callback_T));

         // Close the session
         session->Operation = MEMORY_IDLE;
         session_complete = TRUE;
      }

      Mutex_Release(&session->Mutex);

      // Don't call the callback until we've released the mutex (Avoids deadlock)
      if(session_complete)
      {
         // Call the callback to indicate that we've finished
         callback.Function(&callback);
      }

      session++;
   }
}


/******************************************************************************/
/*!
   \details  Abstracts what memory device we are reading from

   \param    destination  Destination data pipe to copy to
   \param    source       Source location (device/address) of data to copy
   \param    size         size of data to copy (from SIZEOF())
   \param    callback     callback to call when complete

   \return  bool_t

   \retval  TRUE   Memory session has started and callback will be called when complete
   \retval  FALSE  No available memory sessions, try again later
*/
/******************************************************************************/
bool_t Memory_Read(Pipe_WriteHandle_T destination, const MemoryPointer_T *source, Size_T size, const ISOBUS_Callback_T *callback)
{
   bool_t  session_opened = FALSE;
   MemorySession_T  *session;

   // Grab an open session (in case we need it)
   if(FindOpenSessionAndGetMutex(&session))
   {
      // Open the session
      session->Operation  = MEMORY_READ;
      session->Pipe.Write = destination;
      session->Pointer    = (MinAddressable_T*)source->Address;
      session->Size       = size;
      Utility_MemoryCopy(&session->Callback, callback, sizeof(ISOBUS_Callback_T));

      // Process this read session
      ProcessRead(session);

      Mutex_Release(&session->Mutex);

      session_opened = TRUE;
   }

   return session_opened;
}


/******************************************************************************/
/*!
   \details  Abstracts what memory device we are writing to

   \param    destination  Destination location (device/address) for data to copy
   \param    source       Source data pipe to copy from
   \param    size         size of data to copy (from SIZEOF())
   \param    callback     callback to call when complete

   \return  bool_t

   \retval  TRUE   Memory session has started and callback will be called when complete
   \retval  FALSE  No available memory sessions, try again later
*/
/******************************************************************************/
bool_t Memory_Write(const MemoryPointer_T *destination, Pipe_ReadHandle_T source, Size_T size, const ISOBUS_Callback_T *callback)
{
   bool_t  session_opened = FALSE;
   MemorySession_T  *session;

   // Grab an open session (in case we need it)
   if(FindOpenSessionAndGetMutex(&session))
   {
      // Open the session
      session->Operation = MEMORY_WRITE;
      session->Pipe.Read = source;
      session->Pointer   = (MinAddressable_T*)destination->Address;
      session->Size      = size;
      Utility_MemoryCopy(&session->Callback, callback, sizeof(ISOBUS_Callback_T));

      // Process this write session
      ProcessWrite(session);

      Mutex_Release(&session->Mutex);

      session_opened = TRUE;
   }

   return session_opened;
}


/******************************************************************************/
/*
   Copyright (C) 2012  DISTek Integration, Inc.  All Rights Reserved.

   Developed by:
      DISTek(R) Integration, Inc.
      6612 Chancellor Drive Suite 600
      Cedar Falls, IA 50613
      Tel: 319-859-3600
*/
/******************************************************************************/
