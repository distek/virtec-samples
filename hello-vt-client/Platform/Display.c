/******************************************************************************/
/*!
   \file
      This file implements the non-blocking interface for writing to Standard Output.

   \copyright
      Copyright (C) 2012  DISTek Integration, Inc.  All Rights Reserved.

   \author
      Chris Thatcher
*/
/******************************************************************************/


#include "Platform.h"
#include "Foundation.h"
#include "Display.h"

#define MAX_CHARACTERS_AT_A_TIME 10

//! Used for displaying to STDOUT
#ifndef _LIB
extern int fprintf(FILE*, const char*, ...);
#endif // ndef _LIB


//! Pipe to write to STDOUT in the background
Pipe_WriteHandle_T        StandardOutput;

//! Handle for reading data placed in StandardOutput pipe
static Pipe_ReadHandle_T  StdOut_Read;

//! Buffer used for output pipe
static MAKE_PIPE_ARRAY(OutputBuffer, char, 1024);

//! Pipe used for standard output
static Pipe_T OutputPipe = MAKE_Pipe_T(OutputBuffer, PRIORITY_MAX);


//! Pipe to write to STDERR in the background
Pipe_WriteHandle_T        StandardError;

//! Handle for reading data placed in StandardError pipe
static Pipe_ReadHandle_T  StdErr_Read;

//! Buffer used for error pipe
static MAKE_PIPE_ARRAY(ErrorBuffer, char, 1024);

//! Pipe used for standard error
static Pipe_T ErrorPipe = MAKE_Pipe_T(ErrorBuffer, PRIORITY_MAX);


/******************************************************************************/
/*!
   \details  Opens the output pipe
*/
/******************************************************************************/
void Display_Init(void)
{
   Pipe_Init(&OutputPipe);
   (void)Pipe_Open(&OutputPipe, &StandardOutput, &StdOut_Read);

   Pipe_Init(&ErrorPipe);
   (void)Pipe_Open(&ErrorPipe, &StandardError, &StdErr_Read);
}


/******************************************************************************/
/*!
   \details  Prints from pipe to file stream
*/
/******************************************************************************/
static void PrintPipe(FILE *stream, Pipe_ReadHandle_T read_pipe)
{
   char  buffer[MAX_CHARACTERS_AT_A_TIME + 1];
   Size_T  data_available;
#if (MIN_ADDRESSABLE_SIZE_BITS != 8)
   Declare8BitArray(data, MAX_CHARACTERS_AT_A_TIME);
   Size_T i;
#endif

   // Determine how much data is currently waiting to be printed
   data_available = read_pipe.Read->Queue.Used;

   // As long as there is data to print
   while(data_available != 0)
   {
      // Limit the number of characters at a time
      if(data_available > MAX_CHARACTERS_AT_A_TIME)
      {
         data_available = MAX_CHARACTERS_AT_A_TIME;
      }

#if (MIN_ADDRESSABLE_SIZE_BITS == 8)
      // Get the data from the pipe, print it, and remove it from the pipe
      if(Pipe_CopyData(read_pipe, buffer, data_available))
      {
#else
      // Get the data from the pipe, print it, and remove it from the pipe
      if(Pipe_CopyData(read_pipe, data, data_available))
      {
         for(i=0; i<data_available; i++)
         {
            buffer[i] = (char)Get8BitArrayIndex(data,i);
         }
#endif

         buffer[data_available] = '\0';
         if(Pipe_Remove(read_pipe, data_available))
         {
            fprintf(stream, "%s", buffer);
         }
      }

      // Determine how much data is left
      data_available = read_pipe.Read->Queue.Used;
   }
}


/******************************************************************************/
/*!
   \details  Prints to the standard output and standard error devices
*/
/******************************************************************************/
void Display_Task(void)
{
   PrintPipe(stdout, StdOut_Read);
   PrintPipe(stderr, StdErr_Read);
}


/******************************************************************************/
/*
   Copyright (C) 2012  DISTek Integration, Inc.  All Rights Reserved.

   Developed by:
      DISTek(R) Integration, Inc.
      6612 Chancellor Drive Suite 600
      Cedar Falls, IA 50613
      Tel: 319-859-3600
*/
/******************************************************************************/
