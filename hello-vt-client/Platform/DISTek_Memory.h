/******************************************************************************/
/*!
   \file
      The Memory module contains functions for copying memory to/from arbitrary
      memory devices.

   \copyright
      Copyright (C) 2012  DISTek Integration, Inc.  All Rights Reserved.

   \author
      Chris Thatcher
*/
/******************************************************************************/


extern void Memory_Init(void);

extern void Memory_Task(void);

//! Copies data from source memory address to destination pipe
extern bool_t Memory_Read(Pipe_WriteHandle_T destination, const MemoryPointer_T *source, Size_T size, const ISOBUS_Callback_T *callback);

//! Copies data from source pipe to destination memory address
extern bool_t Memory_Write(const MemoryPointer_T *destination, Pipe_ReadHandle_T source, Size_T size, const ISOBUS_Callback_T *callback);


/******************************************************************************/
/*
   Copyright (C) 2012  DISTek Integration, Inc.  All Rights Reserved.

   Developed by:
      DISTek(R) Integration, Inc.
      6612 Chancellor Drive Suite 600
      Cedar Falls, IA 50613
      Tel: 319-859-3600
*/
/******************************************************************************/
