/******************************************************************************/
/*!
   \file
      This file defines the definitions used by the ValueCAN3 interface

   \copyright
      Copyright (C) 2012  DISTek Integration, Inc.  All Rights Reserved.

   \author
      Gerald Fries
*/
/******************************************************************************/

// Intrepid API Data Types
//! Intrepid API definition for a signed 16 bit value
#define icscm_int16  short
//! Intrepid API definition for an unsigned 16 bit value
#define icscm_uint16  unsigned short
//! Intrepid API definition for an unsigned 32 bit value
#define icscm_uint32  unsigned int
//! Intrepid API definition for a signed 32 bit value
#define icscm_int32  int
//! Intrepid API definition for an unsigned 8 bit value
#define icscm_uint8 unsigned char
//! Intrepid API definition for a signed 64 bit value
#define icscm_int64 __int64

//! NeoDevice data structure
typedef struct
{
    //! Neo device type, for this data structure (ValueCAN3 = 16)
    unsigned long DeviceType;
    //! Device handle used by API to access this device
    int Handle;
    //! Reserved
    int NumberOfClients;
    //! Serial number of this device
    int SerialNumber;
    //! Reserved
    int MaxAllowedClients;
} NeoDevice;

//! CAN Settings data structure for the intrepidcs API
typedef struct//VS_MODIFIER struct
{
    //! CAN settings, CAN control mode
    icscm_uint8 Mode;
    //! CAN settings, boudrate set mode (Auto or Use_TQ)
    icscm_uint8 SetBaudrate;
    //! CAN settings, Auto baudrate setting
    icscm_uint8 Baudrate;
    //! CAN settings, not used
    icscm_uint8 NetworkType;
    //! CAN settings, Use_TQ phase 1 segement
    icscm_uint8 TqSeg1;
    //! CAN settings, Use_TQ phase 2 segement
    icscm_uint8 TqSeg2;
    //! CAN settings, Use_TQ propagation delay
    icscm_uint8 TqProp;
    //! CAN settings, Use_TQ Syncro jump width
    icscm_uint8 TqSync;
    //! CAN settings, Use_TQ parameter
    icscm_uint16 BRP;
    //! CAN settings, auto bitrate feature
    icscm_uint16 auto_baud;
} CAN_SETTINGS;

//! SVCAN3 settings data structure for the intrepidcs API
typedef struct//VS_MODIFIER struct _SVCAN3Settings
{
    //! SVCAN settings, CAN settings structure for CAN1
    CAN_SETTINGS can1;
    //! SVCAN settings, CAN settings structure for CAN2
    CAN_SETTINGS can2;
    //! SVCAN settings, software license enables
    icscm_uint16 network_enables;
    //! SVCAN settings, controller enable mode
    icscm_uint16 network_enabled_on_boot;
    //! SVCAN settings, transmit inter-message timing
    icscm_int16 iso15765_separation_time_offset;
    //! SVCAN settings, undefined
    icscm_uint16 perf_en;
    //! SVCAN settings, data direction control of misc I/O pins
    icscm_uint16 misc_io_initial_ddr;
    //! SVCAN settings, latch output control of misc output pins
    icscm_uint16 misc_io_initial_latch;
    //! SVCAN settings, time control to report I/O pin status
    icscm_uint16 misc_io_report_period;
    //! SVCAN settings, event control to report I/O pin status
    icscm_uint16 misc_io_on_report_events;
} SVCAN3Settings;

// icsSpy message data structure for the intrepidcs API
typedef struct
{
    //! neoVI API message, Status Bit Field 1
    icscm_uint32 StatusBitField;
    //! neoVI API message, Status Bit Field 2
    icscm_uint32 StatusBitField2;
    //! neoVI API message, Hardware Time Stamp - 1
    icscm_uint32 TimeHardware;
    //! neoVI API message, Hardware Time Stamp - 2
    icscm_uint32 TimeHardware2;
    //! neoVI API message, System Time Stamp - 1
    icscm_uint32 TimeSystem;
    //! neoVI API message, System Time Stamp - 2
    icscm_uint32 TimeSystem2;
    //! neoVI API message, Type of Hardware Time Stamp
    icscm_uint8  TimeStampHardwareID;
    //! neoVI API message, Type of System Time Stamp
    icscm_uint8  TimeStampSystemID;
    //! neoVI API message, network ID message was received on
    icscm_uint8  NetworkID;
    //! neoVI API message, not used
    icscm_uint8  NodeID;
    //! neoVI API message, type of message protocall
    icscm_uint8  Protocol;
    //! neoVI API message, not used
    icscm_uint8  MessagePieceID;
    //! neoVI API message, not used
    icscm_uint8  ColorID;
    //! neoVI API message, number of bytes stored in header
    icscm_uint8  NumberBytesHeader;
    //! neoVI API message, number of data bytes in the message
    icscm_uint8  NumberBytesData;
    //! neoVI API message, not used
    icscm_int16  DescriptionID;
    //! neoVI API message, the message header data
    long         ArbIDOrHeader;
    //! neoVI API message, array of message data bytes
    icscm_uint8  Data[8];
    //! neoVI API message, not used
    icscm_uint8  AckBytes[8];
    //! neoVI API message, not used
    float        Value;
    //! neoVI API message, not used
    icscm_uint8  MiscData;
} icsSpyMessage;

//! Definition for the Intrepid FindNeoDevices() API function
typedef int (__stdcall * picsneoFindNeoDevices)(unsigned long, NeoDevice *, int * );
//! Definition for the Intrepid OpenNeoDevice() API function
typedef int (__stdcall * picsneoOpenNeoDevice)(NeoDevice *, int *,  unsigned char *, int , int);
//! Definition for the Intrepid ClosePort() API function
typedef int (__stdcall * picsneoClosePort)(int, int *);
//! Definition for the Intrepid FreeObject() API function
typedef int (__stdcall * picsneoFreeObject)(int);
//! Definition for the Intrepid SetVCAN3Settings() API function
typedef int (__stdcall * picsneoSetVCAN3Settings)(int, SVCAN3Settings *, int, int);
//! Definition for the Intrepid GetVCAN3Settings() API function
typedef int (__stdcall * picsneoGetVCAN3Settings)(int, SVCAN3Settings *, int);
//! Definition for the Intrepid GetDeviceParameters() API function
typedef int (__stdcall * picsneoGetDeviceParameters)(int, char *, char *, short);
//! Definition for the Intrepid SetDeviceParameters() API function
typedef int (__stdcall * picsneoSetDeviceParameters)(int, char *, int *, int);
//! Definition for the Intrepid TxMessages() API function
typedef int (__stdcall * picsneoTxMessages)(int, icsSpyMessage *, int , int);
//! Definition for the Intrepid EnableNetworkRXQueue() API function
typedef int (__stdcall * picsneoEnableNetworkRXQueue)(int, int );
//! Definition for the Intrepid GetMessages() API function
typedef int (__stdcall * picsneoGetMessages)(int, icsSpyMessage *, int * , int *);
//! Definition for the Intrepid GetErrorMessages() API function
typedef int (__stdcall * picsneoGetErrorMessages)(int, int *, int *);
//! Definition for the Intrepid WaitForRxMessagesWithTimeOut() API function
typedef int (__stdcall * picsneoWaitForRxMessagesWithTimeOut)(int, unsigned int);
//! Definition for the Intrepid GetLastAPIError() API function
typedef int (__stdcall * picsneoGetLastAPIError)(int, int*);

/******************************************************************************/
/*
   Copyright (C) 2012  DISTek Integration, Inc.  All Rights Reserved.

   Developed by:
      DISTek(R) Integration, Inc.
      6612 Chancellor Drive Suite 600
      Cedar Falls, IA 50613
      Tel: 319-859-3600
*/
/******************************************************************************/
