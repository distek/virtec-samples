/******************************************************************************/
/*!
   \file
      Intrepid CAN driver

   \copyright
      Copyright (C) 2012  DISTek Integration, Inc.  All Rights Reserved.

   \author
      Chris Thatcher
      Gerald Fries
*/
/******************************************************************************/

#include "Platform.h"
#include "Foundation.h"
#include "Display.h"
#include "CAN.h"
#include "Solution.h"
#include "ValueCAN3.h"


// define the ValueCAN3 initalization steps
//! Init step-1, load the ValueCAN3 DLL
#define LOAD_CAN_DLL    0x0001  // load the CAN3 DLL
//! Init step-2, establish links to the API functions
#define LINKS_MAPPED    0x0002  // DLL links established
//! Init step-3, search for attached Neo devices
#define HARDWARE_DETECT 0x0004  // hardware has been detected
//! Init step-4, open the ValueCAN3 port
#define OPEN_CAN3_PORTS 0x0008  // open CAN1 & CAN2
//! Init step-5, initalize the ValueCAN3 device
#define INIT_CAN3_PORTS 0x0010  // setup CAN1 & CAN2
//! Init step-6, indicate that the CAN interface has been initialized
#define DRIVER_ACTIVE   0x0020  // all driver resources ready
//! Bit mask for the CAN interface initalization steps
#define CAN_ACTIVE_MASK 0x003F  // ValueCAN3 port status

// define CAN Driver fault conditions (CAN_Drv_Faults design issues)
//! Define fault bit, ValueCAN3 initalization failed
#define VCAN3_INIT       0x0001  // 0 - CAN adaptor did not initalize
//! Define fault bit, invalid CAN1 Send Packet function argument
#define SEND1_ARGUMENT   0x0002  // 1 - CAN1_SendPacket() bad argument
//! Define fault bit, invalid CAN2 Send Packet function argument
#define SEND2_ARGUMENT   0x0004  // 2 - CAN2_SendPacket() bad argument
//! Define fault bit, empty message (header = 0) was received
#define RX_EMPTY_MSG     0x0008  // 3 - IRS  received an empty message
//! Define fault bit, CAN interface returned a fault status
#define ISR_MESSAGE_WAIT 0x0010  // 4 - IRS WaitForRxMessages Fault
//! Define fault bit, Transmit queue remove failed
#define ISR_QUE_REMOVE   0x0020  // 5 - echo Queue_Remove() Failed
//! Define fault bit, failed to match the last sent message
#define ISR_ECHO_MATCH   0x0040  // 6 - echo TxMsg didn't match the queue(top)
//! Define fault bit, Transmit queue failed to return top entry
#define ISR_QUE_GETFRONT 0x0080  // 7 - echo Queue_GetFront() Failed
//! Define fault bit, message received with invalid NetworkID
#define ISR_NETWORK_ID   0x0100  // 8 - Rx with invalid network ID
//! Define fault bit, ValueCAN3 internal fault detected
#define ISR_CAN3_ERROR   0x0200  // 9 - ValueCAN3 internal error
//! Define fault bit, ValueCAN3 transmit fault detected
#define TX_DRIVER_FAULT  0x0400  // 10 - Transmit driver error
//! Define fault bit, NameTable1 overflow
#define NAME1_OVERFLOW   0x0800  // 11 - NameTable1 error
//! Define fault bit, NameTable2 overflow
#define NAME2_OVERFLOW   0x1000  // 12 - NameTable2 error
//! Define the end of the fault bit-field
#define VCAN_LAST_FAULT  0x2000  // end of driver faults
//! Define the size (in entries) of the CAN1 NAME Table array
#define NAME_TABLE1_SIZE  (NameTableIndex_T)200
//! Define the size (in entries) of the CAN2 NAME Table array
#define NAME_TABLE2_SIZE  (NameTableIndex_T)200

//! define to enable CAN driver debug
//#define SHOW_CANDRIVER_DEBUG  // comment out to desable debug messages

//! define to enable message handling without CAN
#define RUN_WITHOUT_CAN  // comment out to only run with the CAN interface

//! define to enable CAN message time display
#define INCLUDE_TIMING  // comment out to disable CAN message timestamp

//! Intrepid device type for their ValueCAN3 CAN adaptor
#define VALUECAN3 16     // DeviceType parameter
//! Identifies CAN Bus 1
#define CAN1 1           // Channel 1 on ValueCAN3 for NetworkID parameter
//! Identifies CAN Bus 2
#define CAN2 2           // Channel 2 on ValueCAN3 for NetworkID parameter
//! Define the CAN_Task at priority level PL_2
#define CAN_TASK_PRIORITY PL_2      // priority of CAN_TASK
//! Defines the bit mask for an echo message
#define SPY_STATUS_TX_MSG 0x0002    // this Rx was my echo

//! Defines the hold time(ms) for WaitForRxMessagesWithTimeOut()
#define WAIT_FOR_MSG_TIME   milliseconds(50)  // WaitForRxMessage wait time 50ms
//! Defines WaitForRxMessagesWithTimeOut() return when No Data
#define WAIT_FOR_MSG_EMPTY  0   // WaitForRxMessage Empty return flag
//! Defines WaitForRxMessagesWithTimeOut() return when Error
#define WAIT_FOR_MSG_ERROR  -1  // WaitForRxMessage Error return flag
//! Define GetMessages() successful return
#define GET_MESSAGE_DATA     1  // GetMessages() with data return flag

//! Defines the transmit queue size for CAN-1
#define TX_QUE1_SIZE   50 // room for 50 CAN1 Tx Messages
//! Defines the transmit queue size for CAN-2
#define TX_QUE2_SIZE   20 // room for 20 CAN2 Tx Messages

//! Define the send message delay, 250ms
#define TX_WAIT_TIME      milliseconds(450)

//! windows DLL handle
HINSTANCE DLLHandle = NULL;

// declare pointers to the ValueCAN3 API functions
//! Pointer to the Intrepid FindNeoDevices() API function
picsneoFindNeoDevices icsneoFindNeoDevices;
//! Pointer to the Intrepid OpenNeoDevice() API function
picsneoOpenNeoDevice icsneoOpenNeoDevice;
//! Pointer to the Intrepid ClosePort() API function
picsneoClosePort icsneoClosePort;
//! Pointer to the Intrepid FreeObject() API function
picsneoFreeObject icsneoFreeObject;
//! Pointer to the Intrepid SetVCAN3Settings() API function
picsneoSetVCAN3Settings icsneoSetVCAN3Settings;
//! Pointer to the Intrepid GetVCAN3Settings() API function
picsneoGetVCAN3Settings icsneoGetVCAN3Settings;
//! Pointer to the Intrepid GetDeviceParameters() API function
picsneoGetDeviceParameters icsneoGetDeviceParameters;
//! Pointer to the Intrepid SetDeviceParameters() API function
picsneoSetDeviceParameters icsneoSetDeviceParameters;
//! Pointer to the Intrepid TxMessages() API function
picsneoTxMessages icsneoTxMessages;
//! Pointer to the Intrepid GetMessages() API function
picsneoGetMessages icsneoGetMessages;
//! Pointer to the Intrepid EnableNetworkRXQueue() API function
picsneoEnableNetworkRXQueue icsneoEnableNetworkRXQueue;
//! Pointer to the Intrepid GetErrorMessages() API function
picsneoGetErrorMessages icsneoGetErrorMessages;
//! Pointer to the Intrepid WaitForRxMessagesWithTimeOut() API function
picsneoWaitForRxMessagesWithTimeOut icsneoWaitForRxMessagesWithTimeOut;
//! Pointer to the Intrepid GetLastAPIError() API function
picsneoGetLastAPIError icsneoGetLastAPIError;

//! Send (or queue to send) a packet on CAN1
extern bool_t CAN1_SendPacket(const CAN_Packet_T *packet, const ISOBUS_Callback_T *callback);

//! Send (or queue to send) a packet on CAN2
extern bool_t CAN2_SendPacket(const CAN_Packet_T *packet, const ISOBUS_Callback_T *callback);

//! Tx Message Queue structure
typedef struct TxCanQueueEntry_S
{
    //! ISO message packet
    CAN_Packet_T TxPacket;
    //! SendPacket Callback
    ISOBUS_Callback_T  Callback;
    //! CAN bus (1 or 2)
    icscm_uint8  ISO_Bus_Id;
} TxCanQueueEntry_T;

//! Array of CAN Driver fault message string pointers
const char *FaultList[] =
{
   "ValueCAN3 initalization failed",    //  0 VCAN3_INIT
   "Send CAN1 with Null argument",      //  1 SEND1_ARGUMENT
   "Send CAN2 with Null argument",      //  2 SEND2_ARGUMENT
   "Empty message received, check Bus", //  3 RX_EMPTY_MSG
   "ValueCAN3 receive message fault",   //  4 ISR_MESSAGE_WAIT
   "Un-queue message failed",           //  5 ISR_QUE_REMOVE
   "Echo message did not match",        //  6 ISR_ECHO_MATCH
   "Transmit Queue is empty",           //  7 ISR_QUE_GETFRONT
   "Received invalid network ID",       //  8 ISR_NETWORK_ID
   "ValueCAN3 Receiver error",          //  9 ISR_CAN3_ERROR
   "ValueCAN3 Transmit error",          // 10 TX_DRIVER_FAULT
   "NAME Table1 Overflow",              // 11 NAME1_OVERFLOW
   "Name Table2 Overflow"               // 12 NAME2_OVERFLOW
};

//! Callback when first entry is added to queue
static void CAN_FirstItemCallback(const ISOBUS_Callback_T *callback);

// Local variables
//! initalization string for Intrepid CAN adaptor
char ValueCANsettings[200] = {"can1/SetBaudrate=0,can1/Baudrate=7,can2/SetBaudrate=0,can2/Baudrate=7"};
//! CAN Driver Initalization flags
uint16_t CAN3_status = 0;
//! CAN Driver Fault flags
uint16_t CAN_Drv_Faults;          // CAN Driver Fault flags
//! handle of the neoVI driver object
int hobject[255];
//! array of NeoDevice structures
NeoDevice devices[255];
//! The Intrepid device type being used
uint32_t deviceType = VALUECAN3;  // 16 designates "Value CAN 3"
//! number of Intrepid CAN devices detected during initialization
int numberOfDevices = 255;        // used to call FindNeoDevices and speficies number of elements in devices[]
//! Number of errors reported by the Intrepid API
int NumErrors;
//! CAN message time, 5ms incraments
uint32_t Msg_Time = 0;

//! Declare the CAN1 Tx Queues
//! Declare memory array for the CAN1 transmit queue
static MAKE_QUEUE_ARRAY(Array1, TxCanQueueEntry_T, TX_QUE1_SIZE);
//! Callback structure for first items added to CAN1 queue
ISOBUS_Callback_T  CAN1_FirstItemCallback;
//! Declare control structure for the CAN1 transmit queue
InterruptQueue_T CAN1_TxQueue = MAKE_InterruptQueue_T(Array1, &CAN1_FirstItemCallback, NULL, NULL, PRIORITY_MAX);

//! Declare the CAN2 Tx Queues
//! Declare memory array for the CAN2 transmit queue
static MAKE_QUEUE_ARRAY(Array2, TxCanQueueEntry_T, TX_QUE2_SIZE);
//! Callback structure for first items added to CAN1 queue
ISOBUS_Callback_T  CAN2_FirstItemCallback;
//! Declare control structure for the CAN2 transmit queue
InterruptQueue_T CAN2_TxQueue = MAKE_InterruptQueue_T(Array2, &CAN2_FirstItemCallback, NULL, NULL, PRIORITY_MAX);

//! Declare the NAME Tables
//! Array of CAN1 Name Table entries
NameTableEntry_T Name_Table1[NAME_TABLE1_SIZE];  // CAN1 Name Table entry array
//! Array of CAN2 Name Table entries
NameTableEntry_T Name_Table2[NAME_TABLE2_SIZE];  // CAN2 Name Table entry array
//! Array of CAN Networks
Network_T  Networks[2] =
   {
      {
         MAKE_Network_PacketHandlerList_T(PRIORITY_MAX),
         MAKE_NameTable_T(Name_Table1, Solution_SwTimerList, PRIORITY_MAX),
         CAN1_SendPacket
      },
      {
         MAKE_Network_PacketHandlerList_T(PRIORITY_MAX),
         MAKE_NameTable_T(Name_Table2, Solution_SwTimerList, PRIORITY_MAX),
         CAN2_SendPacket
      }
   };


// Prototypes
uint16_t MapDllFunctionPointers(void);
uint16_t Initalize_CAN3(void);
bool_t VCAN3_IsReady(void);
void Convert_CANtoValueCAN3(const CAN_Packet_T *packet, icsSpyMessage *Msg);
void Convert_ValueCAN3toCAN(const icsSpyMessage *Msg, CAN_Packet_T *packet);
static void TxPacketMsg(InterruptQueue_T *queue);


/******************************************************************************/
/*!
   \details  Puts message at front of queue on hardware

   \param   callback  Callback structure containing all necessary data
*/
/******************************************************************************/
static void CAN_FirstItemCallback(const ISOBUS_Callback_T *callback)
{
   // Send a packet from front of queue
   TxPacketMsg((InterruptQueue_T*)callback->Pointer_1);
}


/******************************************************************************/
/*!
   \details  Prints the contents of the packet to StandardOutput

   \param   packet   ISOBUS packet
   \param   bus_num  Bus number associated with the packet.

   \return  void
*/
/******************************************************************************/
static void PrintPacket(const CAN_Packet_T *packet, uint8_t bus_num)
{
   char                buffer[64];
   char                data[25];
   const char*         direction_text[] = {"RX", "TX", "??"};
   ISOBUS_Direction_T  direction;

   // Print the Data
   data[0] = '\0';
   for(Size_T i=0; i<packet->DLC; i++)
   {
      // Write previous data bytes and current data byte to buffer
      sprintf(buffer, "%s %02X", data, packet->Data[i]);

      // Transfer from buffer back to data
      sprintf(data, "%s", buffer);
   }

   direction = packet->Header.Direction;
   if(direction > ISOBUS_TX)
   {
      direction = (ISOBUS_Direction_T)2;
   }

#ifdef INCLUDE_TIMING
   // Print the message contents to the screen
   if (Msg_Time != 0)
   {
      sprintf(buffer, "%6lu %u %s %08X  %X %s\n",
         Msg_Time,
         bus_num,
         direction_text[direction],
         (int)packet->Header.Identifier,
         (int)packet->DLC,
         data);
   }
   else
#endif //INCLUDE_TIMING
   {
      sprintf(buffer, " %u %s %08X  %X %s\n",
         bus_num,
         direction_text[direction],
         (int)packet->Header.Identifier,
         (int)packet->DLC,
         data);
   }

   (void)Pipe_InsertString(StandardOutput, buffer);
}


/******************************************************************************/
/*!
   \brief    Initialize the CAN driver

   \details  Initalize the ValueCAN3 adaptor, initalize a queue for CAN1 and
             queue for CAN2, clear the driver fault flags
*/
/******************************************************************************/
void CAN_Init(void)
{
   (void)Initalize_CAN3();

   Network_Init(&Networks[0]);
   Network_Init(&Networks[1]);

   // initialize CAN1 Tx message control
   InterruptQueue_Init(&CAN1_TxQueue);
   CAN1_FirstItemCallback.Function = CAN_FirstItemCallback;
   CAN1_FirstItemCallback.Pointer_1 = &CAN1_TxQueue;

   // initalize CAN2 Tx message control
   InterruptQueue_Init(&CAN2_TxQueue);
   CAN2_FirstItemCallback.Function = CAN_FirstItemCallback;
   CAN2_FirstItemCallback.Pointer_1 = &CAN2_TxQueue;

   CAN_Drv_Faults = 0;     // clear Driver faults
}


/******************************************************************************/
/*!
   \brief    CAN Driver Task

   \details  CAN Driver Task, sets a fault flag if the ValueCAN3 did not initalize,
             Displays active CAN Driver faults (todo - remove later)
*/
/******************************************************************************/
void CAN_Task(void)
{
#ifdef RUN_WITHOUT_CAN
   static TxCanQueueEntry_T CAN_Que;      // used to copy out the next Queue entry
#endif
   static uint16_t last_fault = 0;
   uint16_t p;

   Msg_Time += 10;

   // check if the ValueCAN3 interface initialized
   if (VCAN3_IsReady() == FALSE)
   {
       CAN_Drv_Faults = VCAN3_INIT;
   }

   // service the two NAME_Table structures
   Network_Task(&Networks[0]);
   Network_Task(&Networks[1]);

   // display CAN Driver faults
   // \todo - this will be removed when we have a better way to handle program faults
   //
   if (last_fault != CAN_Drv_Faults)
   {
      int i;

      for (i=0, p=1; p < VCAN_LAST_FAULT; i++, p<<=1)
      {
         if ((last_fault & p) != (CAN_Drv_Faults & p))
         {
            char buffer[100];
            sprintf(buffer, "CAN Driver: %s\n", FaultList[i]);
           (void)Pipe_InsertString(StandardOutput, buffer);
         }
      }
      last_fault = CAN_Drv_Faults;
   }
#ifdef RUN_WITHOUT_CAN
   // if ValueCAN3 initalization failed
   if (VCAN3_IsReady() == FALSE)
   {
      static bool_t SendOne = TRUE;

      if (SendOne == TRUE)
      {
         SendOne = FALSE;
         (void)Pipe_InsertString(StandardOutput, "Running - WITHOUT CAN\n");
      }

      // if there is an entries in the CAN1 queue, send it
      if (InterruptQueue_CopyData(&CAN1_TxQueue, &CAN_Que, SIZEOF(TxCanQueueEntry_T)) == TRUE)
      {
         // identify as a sent message
         CAN_Que.TxPacket.Header.Direction = ISOBUS_TX;
         // Remove this message from the queue
         InterruptQueue_Remove(&CAN1_TxQueue, SIZEOF(TxCanQueueEntry_T));

         // if a Callback was provided when the message was sent, call the Callback function
         if (CAN_Que.Callback.Function != NULL)
         {
            CAN_Que.Callback.Function(&(CAN_Que.Callback));
         }
         PrintPacket(&(CAN_Que.TxPacket), 1);
         Network_PacketHandler(&(CAN_Que.TxPacket), &Networks[0]);  // Packet Handler for CAN1
      } // endof CAN1

      // if there is an entries in the CAN2 queue, send it
      if (InterruptQueue_CopyData(&CAN2_TxQueue, &CAN_Que, SIZEOF(TxCanQueueEntry_T)) == TRUE)
      {
         // identify as a sent message
         CAN_Que.TxPacket.Header.Direction = ISOBUS_TX;
         // Remove this message from the queue
         InterruptQueue_Remove(&CAN2_TxQueue, SIZEOF(TxCanQueueEntry_T));

         // if a Callback was provided when the message was sent, call the Callback function
         if (CAN_Que.Callback.Function != NULL)
         {
            CAN_Que.Callback.Function(&(CAN_Que.Callback));
         }
         PrintPacket(&(CAN_Que.TxPacket), 2);
         Network_PacketHandler(&(CAN_Que.TxPacket), &Networks[1]);  // Packet Handler for CAN1
      } // endof CAN2
   }  // endof VCAN3_IsReady() = False
#endif // RUN_WITHOUT_CAN

}


/******************************************************************************/
/*!
   \brief    Send (or queue to send) a packet to CAN1

   \details  If the CAN Driver is ready, push this message into the CAN1 queue,
             if this is the only entry in the queue, then send it to CAN1 Transmit.

   \param    packet    Generic ISOBUS packet structure containing CAN message to send
   \param    callback  Callback to call after successful transmit

   \return   bool_t
   \retval   TRUE   Packet was queued to send (in queue or on hardware)
   \retval   FALSE  Packet was not queued to send (try back later)
*/
/******************************************************************************/
bool_t CAN1_SendPacket(const CAN_Packet_T *packet, const ISOBUS_Callback_T *callback)
{
   bool_t ret = FALSE;
   TxCanQueueEntry_T tx_entry;

   // if invalid packet pointer
   if (packet != NULL)
   {
      Utility_MemoryCopy(&tx_entry.TxPacket, packet, sizeof(CAN_Packet_T));
      Utility_MemoryCopy(&tx_entry.Callback, callback, sizeof(ISOBUS_Callback_T));
      tx_entry.ISO_Bus_Id = CAN1;
      ret = InterruptQueue_Insert(&CAN1_TxQueue, &tx_entry, SIZEOF(TxCanQueueEntry_T));
   } // else bad call, packet argument is NULL
   else
   {
      CAN_Drv_Faults |= SEND1_ARGUMENT;     // set bad argument fault
   }

   return ret;
}


/******************************************************************************/
/*!
   \brief    Send (or queue to send) a packet to CAN2

   \details  If the CAN Driver is ready, push this message into the CAN2 queue,
             if this is the only entry in the queue, then send it to CAN2 Transmit.

   \param    packet    Generic ISOBUS packet structure containing CAN message to send
   \param    callback  Callback to call after successful transmit, along with parameters to pass after

   \return   bool_t
   \retval   TRUE   Packet was queued to send (in queue or on hardware)
   \retval   FALSE  Packet was not queued to send (try back later)
*/
/******************************************************************************/
bool_t CAN2_SendPacket(const CAN_Packet_T *packet, const ISOBUS_Callback_T *callback)
{
   bool_t ret = FALSE;
   TxCanQueueEntry_T tx_entry;

   // if invalid packet pointer
   if (packet != NULL)
   {
      Utility_MemoryCopy(&tx_entry.TxPacket, packet, sizeof(CAN_Packet_T));
      Utility_MemoryCopy(&tx_entry.Callback, callback, sizeof(ISOBUS_Callback_T));
      tx_entry.ISO_Bus_Id = CAN2;
      ret = InterruptQueue_Insert(&CAN2_TxQueue, &tx_entry, SIZEOF(TxCanQueueEntry_T));
   } // else bad call, packet argument is NULL
   else
   {
      CAN_Drv_Faults |= SEND2_ARGUMENT;     // set bad argument fault
   }
   return ret;
}


/******************************************************************************/
/*!
   \brief    Send message to the ValueCAN3 interface (CAN1 or CAN2)

   \details  Convert the ISOBUS_Packet to a ValueCAN3 data structure, and send
             it to the appropiate CAN driver interface

   \param    QuePacket pointer to the ISOBUS packet structure containing CAN message to send

   \return   bool_t
   \retval   TRUE   Packet was sent to the ValueCAN3 hardware
   \retval   FALSE  Packet send failed (try back later)
*/
/******************************************************************************/
static void TxPacketMsg(InterruptQueue_T *queue)
{
   TxCanQueueEntry_T  *entry;
   icsSpyMessage      CAN_Msg;   // message to send

   // only send if the ValueCAN3 has been successfully initialized
   if (VCAN3_IsReady() == TRUE)
   {
      // If there's another item in the queue
      if(InterruptQueue_GetFront(queue, &entry))
      {
         // convert ISO data to ValueCAN3
         Convert_CANtoValueCAN3(&entry->TxPacket, &CAN_Msg);
         // Transmit Message on CAN(1 or 2) of Device 0, if Tx fails return False
         icsneoTxMessages(hobject[0], &CAN_Msg, entry->ISO_Bus_Id, 1);
      }
   }
}


/******************************************************************************/
/*!
   \brief    ISR for receiving CAN messages on both CAN channels

   \details  If there are received CAN messages, they are converted back to the
             ISOBUS structure and sent back to the appropiate DataHandler finctions.
             Echoed messages (the last one sent) is compared to the last sent
             message, and if they match, the matching entry is removed from the
             queue, the attached callback function is called, and then the message
             is returned to the sending DataHandler. If there are messages stored
             in the queue, the oldest message is sent to the TxPacketMsg function,
             which formats it and sends it to the transmitter.

*/
/******************************************************************************/
void CAN_ISR(void)
{
   CAN_Packet_T  packet;     // used for converting ValueCAN3 to ISO
   TxCanQueueEntry_T *queue_entry;    // used to point to the next Queue entry
   InterruptQueue_T *TxQueue_pointer;    // pointer to either CAN1_TxQueue or CAN2_TxQueue
   unsigned long echo_check;

   static icsSpyMessage CAN_RxMsg[20000]; // holds the received messages
   int NumberOfErrors;
   int NumberOfMessages;
   int GetMsgeRtn;
   int RxMsgIndex;

   // only run this function if the ValueCAN3 adaptor was successfully initialized
   while(!PowerDown && VCAN3_IsReady() == TRUE)   // loop forever
   {
      // wait for a received message (This call should block if there are no messages)
      int WaitForRxRtn = icsneoWaitForRxMessagesWithTimeOut(hobject[0], WAIT_FOR_MSG_TIME);
      if (WaitForRxRtn == WAIT_FOR_MSG_EMPTY)    // Rx que is empty
      {
         continue; // no messages or Rx Error
      }
      if (WaitForRxRtn == WAIT_FOR_MSG_ERROR)    // Rx Error
      {
         CAN_Drv_Faults |= ISR_MESSAGE_WAIT;
         continue; // no messages or Rx Error
      }
      // get array of all qued up received messages
      GetMsgeRtn = icsneoGetMessages(hobject[0], CAN_RxMsg, &NumberOfMessages, &NumberOfErrors);
      // if there are received messages, process them from the array
      if (GetMsgeRtn == GET_MESSAGE_DATA)    // GetMessages() call was successful
      {
         // loop through the array, processing all messages in the array
         for (RxMsgIndex=0; RxMsgIndex < NumberOfMessages; RxMsgIndex++)
         {
            // Convert the message to CAN_Packet_T
            Convert_ValueCAN3toCAN(&CAN_RxMsg[RxMsgIndex], &packet);
            // check if message was sent and received by the same channel (echo)
            echo_check = CAN_RxMsg[RxMsgIndex].StatusBitField;
            // if this is an echoed message...
            if ((echo_check & SPY_STATUS_TX_MSG) == SPY_STATUS_TX_MSG)  // this is an echo
            {
               if (CAN_RxMsg[RxMsgIndex].NetworkID == CAN1)
               {
                  TxQueue_pointer = &CAN1_TxQueue;
               }
               // else if it is from CAN Bus2
               else
               {
                  TxQueue_pointer = &CAN2_TxQueue;
               }
               packet.Header.Direction = ISOBUS_TX;   // identify as a sent message
               // get a pointer to the first (oldest) entry in the queue
               if (InterruptQueue_GetFront(TxQueue_pointer, &queue_entry))
               {
                  // compare the received message with the message in the queue
                  if ( (queue_entry->ISO_Bus_Id == CAN_RxMsg[RxMsgIndex].NetworkID) &&           // same bus (1 or 2)
                       (queue_entry->TxPacket.Header.Identifier == packet.Header.Identifier) &&  // same PGN
                       (queue_entry->TxPacket.DLC == packet.DLC)  &&                             // same data length
                       (queue_entry->TxPacket.Data[0] == packet.Data[0]) )                       // same first Data value
                  {
                      // if they match, then...
                      // if a Callback was provided when the message was sent, call the Callback function
                     if (queue_entry->Callback.Function != NULL)
                     {
                        queue_entry->Callback.Function(&(queue_entry->Callback));
                     }
                     // Remove this message from the queue
                     if (InterruptQueue_Remove(TxQueue_pointer, SIZEOF(TxCanQueueEntry_T)))
                     {
                        TxPacketMsg(TxQueue_pointer);
                     }  // endof if(Queue_Remove)
                     else  // else the TxQueue is empty (can't happen)
                     {
                        CAN_Drv_Faults |= ISR_QUE_REMOVE;     // set Queue_Remove() fault
                     }
                  }  // endof if(echo match)
                  // if the echo failed, and there is a message in the queue, resend it
                  else  // else the echo test failed (shouldn't happen, last out - first in)
                  {
                     CAN_Drv_Faults |= ISR_ECHO_MATCH;     // set message Matching fault
                     // copy the top (oldest) entry from the queue, and resend it
                     TxPacketMsg(TxQueue_pointer);
                  }
               }  // endof if(GetFront)
               else  // else the TxQueue is empty (can't happen)
               {
                  CAN_Drv_Faults |= ISR_QUE_GETFRONT;     // set Queue_GetFront() fault
               }
            }  // endof if(echo)
            else  // else this is not an echo
            {
               packet.Header.Direction = ISOBUS_RX;   // identify as a received message
            }
            // Pass the packet to the correct handler to be processed
            // if the message is empty (header = 0), discard the message
            if (CAN_RxMsg[RxMsgIndex].ArbIDOrHeader == 0)
            {
               // the CAN bus is most likely NOT terminated
               CAN_Drv_Faults |= RX_EMPTY_MSG;
            }
            // else if this mesage was sent on CAN1, send it to the CAN1 DataHandler
            else if (CAN_RxMsg[RxMsgIndex].NetworkID == CAN1)
            {
               PrintPacket(&packet, 1);
               Network_PacketHandler(&packet, &Networks[0]);  // Packet Handler for CAN1
            }
            //! else if this mesage was sent on CAN2, send it to the CAN2 DataHandler
            else if (CAN_RxMsg[RxMsgIndex].NetworkID == CAN2)
            {
               PrintPacket(&packet, 2);
               Network_PacketHandler(&packet, &Networks[1]);  // Packet Handler for CAN2
            }
            else
            {
               // received a message with an invalid NetworkID
               CAN_Drv_Faults |= ISR_NETWORK_ID;
            }
         } // endof for(NumberOfMessages)
      } // endof if(messages)
      else  // else if (NumberOfErrors != 0)
      {
         // receiver is reporting an internal error
         CAN_Drv_Faults |= ISR_CAN3_ERROR;
      }
   } // endof while(forever)
}


/******************************************************************************/
/*!
   \brief    Assembles a ValueCAN3 Tx message, from its ISO Bus components

   \details  Fills the ValueCAN3 structure with ISOBUS_Packet Tx message data

   \param    packet  pointer to the ISOBUS packet structure containing CAN message to send
   \param    Msg     pointer to the returned ValueCAN3 meaasge structure
*/
/******************************************************************************/
void Convert_CANtoValueCAN3(const CAN_Packet_T *packet, icsSpyMessage *Msg)
{
   uint8_t i;

   Msg->StatusBitField = 0x0004; // extended ID
   Msg->StatusBitField2 = 0x0000;
   Msg->NumberBytesHeader = 4;
   Msg->ArbIDOrHeader  = (long)packet->Header.Identifier;
   Msg->NumberBytesData = (icscm_uint8)packet->DLC;
   for (i=0; i < 8; i++)
   {
      if(i < packet->DLC)
      {
         Msg->Data[i] = (icscm_uint8)packet->Data[i];
      }
      else
      {
         Msg->Data[i] = (icscm_uint8)0xFF;
      }
   }
}


/******************************************************************************/
/*!
   \brief    Disassembles a ValueCAN3 Rx message into its ISO Bus components

   \details  Fills the ISOBUS_Packet with data from the received ValueCAN3 structure

   \param    Msg     pointer to the ValueCAN3 meassge structure containing the received message
   \param    packet  pointer to the ISOBUS packet structure for the received CAN message

*/
/******************************************************************************/
void Convert_ValueCAN3toCAN(const icsSpyMessage *Msg, CAN_Packet_T *packet)
{
   uint8_t i;

   packet->Header.Identifier = (CAN_Identifier_T)Msg->ArbIDOrHeader;
   packet->DLC = (ISOBUS_DLC_T)Msg->NumberBytesData;
   // make sure no more than 8 data bytes
   if (packet->DLC > 8)
   {
#ifdef SHOW_CANDRIVER_DEBUG
      char buffer[40];
      sprintf(buffer, "Packet Length Error %d\n", CAST_uint16_t(packet->DLC));
      (void)Pipe_InsertString(StandardOutput, buffer);
#endif
      packet->DLC = 8;
   }
   for (i=0; i < 8; i++)
   {
      if(i < packet->DLC)
      {
         packet->Data[i] = (ISOBUS_PacketData_T)Msg->Data[i];
      }
      else
      {
         packet->Data[i] = (ISOBUS_PacketData_T)0xFF;
      }
   }
}


/******************************************************************************/
/*!
   \brief    Initalizes the ValueCAN3 adaptor

   \details  Links in the ValueCAN3 DLL, establishes links the the adaptors DLL
             functions, finds the attached ValueCAN3 adaptors, opens the CAN port,
             and sets CAN1 and CAN2 for 250Kbaud,

   \return   uint16_t
   \retval   initalization bit field data, range 0x00 - 0x3F
   \retval   returns DRIVER_ACTIVE if the initalization was successful
*/
/******************************************************************************/
uint16_t Initalize_CAN3(void)
{
   int iErrorIndex;

   // For Startup - if the driver hasn't been opened, then open it
   if ((CAN3_status & CAN_ACTIVE_MASK) == 0)
   {
      uint16_t tmp_status = 0;

      // load the icsneo40 DLL
      if (DLLHandle == NULL)
      {
         DLLHandle = LoadLibrary("icsneo40.dll");
         if (DLLHandle != NULL)
         {
            tmp_status |= LOAD_CAN_DLL;
         }
      }
      // link to the DLL functions
      if (tmp_status == LOAD_CAN_DLL)
      {
         uint16_t itmp = MapDllFunctionPointers();
        if (itmp == 0)
        {
           tmp_status |= LINKS_MAPPED;
        }
      }
      // search for any attached ValueCAN3 devices
      if ((tmp_status & LINKS_MAPPED) == LINKS_MAPPED)
      {
         icsneoFindNeoDevices(deviceType, devices, &numberOfDevices);
         if (numberOfDevices > 0)
         {
            tmp_status |= HARDWARE_DETECT;
         }
      }
      // if device(s) attached, open the first device
      if ((tmp_status & HARDWARE_DETECT) == HARDWARE_DETECT)
      {
         uint16_t itmp = icsneoOpenNeoDevice(&devices[0], &hobject[0], NULL, 1, 0);
         if (itmp != 0)   // device was successfully opened
         {
            tmp_status |= OPEN_CAN3_PORTS;
         }
      }
      // if device opened, setup the ports
      if ((tmp_status & OPEN_CAN3_PORTS) == OPEN_CAN3_PORTS)
      {
         int i = icsneoSetDeviceParameters(hobject[0], ValueCANsettings, &iErrorIndex, 0);
         if (i == 1)   // device was successfully setup (-1 & 0 indicate an error)
         {
            tmp_status |= INIT_CAN3_PORTS;
            tmp_status |= DRIVER_ACTIVE;
         }
      }
      CAN3_status = tmp_status;
   }
   return (CAN3_status);
}


/******************************************************************************/
/*!
   \brief    Closes (shuts down) the ValueCAN3 adaptor

*/
/******************************************************************************/
void Close_CAN_Adaptor(void)
{

   // For Shutdown - if the is open, then shut it down
   if ((CAN3_status & CAN_ACTIVE_MASK) != 0)
   {
      // close the CAN ports
      if ((CAN3_status & OPEN_CAN3_PORTS) == OPEN_CAN3_PORTS)
      {
         while(icsneoClosePort(hobject[0], &NumErrors) == 0)
            Sleep(50);
         while(icsneoFreeObject(hobject[0]) == 0)
            Sleep(50);
      }
      // release the DLL
      if ((CAN3_status & LOAD_CAN_DLL) == LOAD_CAN_DLL)
      {
         while(FreeLibrary((HMODULE)DLLHandle) == 0)
            Sleep(50);
         DLLHandle = NULL;
         CAN3_status = 0;
      }
   }
   CAN3_status = 0;
}


/******************************************************************************/
/*!
   \brief    Returns the ValueCAN3 initalization bit field

   \return   uint16_t
   \retval   range is 0 - 62, 62 indicates a successful initalization
*/
/******************************************************************************/
uint16_t Get_CAN3_Status(void)
{
   return (CAN3_status);
}


/******************************************************************************/
/*!
   \brief    returns indication if the ValueCAN3 adaptor is initialized

   \return   bool_t
   \retval   TRUE(1) ValueCAN3 is initialized and ready
   \retval   FALSE(0) ValueCAN3 did not complete initalization
*/
/******************************************************************************/
bool_t VCAN3_IsReady(void)
{
   return ((CAN3_status & CAN_ACTIVE_MASK) == CAN_ACTIVE_MASK) ? TRUE : FALSE;
}


/******************************************************************************/
/*!
   \brief    returns the CAN Driver fault flags

   \return   uint16_t
   \retval   0 - no faults
   \retval   0x001 - 0x0F7F indicates one or more active faults
*/
/******************************************************************************/
uint16_t Get_DriverFaults(void)
{
   return (CAN_Drv_Faults);
}


/******************************************************************************/
/*!
   \brief    Map function pointers to functions within the icsneo40.dll

   \return   uint16_t
   \retval   0   all functions were mapped
   \retval   !0  one or more functions pointers failed to map
*/
/******************************************************************************/
uint16_t MapDllFunctionPointers(void)
{
  uint16_t MapState;

  if (DLLHandle != NULL)
  {
     MapState = 0x0000;

     icsneoFindNeoDevices = (picsneoFindNeoDevices)GetProcAddress(DLLHandle, "icsneoFindNeoDevices");
     if (icsneoFindNeoDevices == NULL)
     {
        MapState = 0x0001;  // Function not mapped
     }

     icsneoOpenNeoDevice = (picsneoOpenNeoDevice)GetProcAddress(DLLHandle, "icsneoOpenNeoDevice");
     if (icsneoOpenNeoDevice == NULL)
     {
        MapState |= 0x0002;  // Function not mapped
     }

     icsneoClosePort = (picsneoClosePort)GetProcAddress(DLLHandle, "icsneoClosePort");
     if (icsneoClosePort == NULL)
     {
        MapState |= 0x0004;  // Function not mapped
     }

     icsneoFreeObject = (picsneoFreeObject)GetProcAddress(DLLHandle, "icsneoFreeObject");
     if (icsneoFreeObject == NULL)
     {
        MapState |= 0x0008;  // Function not mapped
     }

     icsneoSetVCAN3Settings = (picsneoSetVCAN3Settings)GetProcAddress(DLLHandle, "icsneoSetVCAN3Settings");
     if (icsneoSetVCAN3Settings == NULL)
     {
        MapState |= 0x0010; // Function not mapped
     }

     icsneoGetVCAN3Settings = (picsneoGetVCAN3Settings)GetProcAddress(DLLHandle, "icsneoGetVCAN3Settings");
     if (icsneoGetVCAN3Settings == NULL)
     {
        MapState |= 0x0020; // Function not mapped
     }

     icsneoGetDeviceParameters = (picsneoGetDeviceParameters)GetProcAddress(DLLHandle, "icsneoGetDeviceParameters");
     if (icsneoGetVCAN3Settings == NULL)
     {
        MapState |= 0x0040; // Function not mapped
     }

     icsneoSetDeviceParameters = (picsneoSetDeviceParameters)GetProcAddress(DLLHandle, "icsneoSetDeviceParameters");
     if (icsneoSetDeviceParameters == NULL)
     {
        MapState |= 0x0080; // Function not mapped
     }

     icsneoTxMessages = (picsneoTxMessages)GetProcAddress(DLLHandle, "icsneoTxMessages");
     if (icsneoTxMessages == NULL)
     {
        MapState |= 0x0100; // Function not mapped
     }

     icsneoGetMessages = (picsneoGetMessages)GetProcAddress(DLLHandle, "icsneoGetMessages");
     if (icsneoGetMessages == NULL)
     {
        MapState |= 0x0200; // Function not mapped
     }

     icsneoEnableNetworkRXQueue = (picsneoEnableNetworkRXQueue)GetProcAddress(DLLHandle, "icsneoEnableNetworkRXQueue");
     if (icsneoEnableNetworkRXQueue == NULL)
     {
        MapState |= 0x0400; // Function not mapped
     }

     icsneoGetErrorMessages = (picsneoGetErrorMessages)GetProcAddress(DLLHandle, "icsneoGetErrorMessages");
     if (icsneoGetErrorMessages == NULL)
     {
        MapState |= 0x0800; // Function not mapped
     }

     icsneoWaitForRxMessagesWithTimeOut = (picsneoWaitForRxMessagesWithTimeOut)GetProcAddress(DLLHandle, "icsneoWaitForRxMessagesWithTimeOut");
     if (icsneoWaitForRxMessagesWithTimeOut == NULL)
     {
        MapState |= 0x1000; // Function not mapped
     }

     icsneoGetLastAPIError = (picsneoGetLastAPIError)GetProcAddress(DLLHandle, "icsneoGetLastAPIError");
     if (icsneoGetLastAPIError == NULL)
     {
        MapState |= 0x2000; // Function not mapped
     }

   }
   else
   {
      MapState = 0x7FFF;
   }

   return MapState;
}


/******************************************************************************/
/*
   Copyright (C) 2012  DISTek Integration, Inc.  All Rights Reserved.

   Developed by:
      DISTek(R) Integration, Inc.
      6612 Chancellor Drive Suite 600
      Cedar Falls, IA 50613
      Tel: 319-859-3600
*/
/******************************************************************************/
