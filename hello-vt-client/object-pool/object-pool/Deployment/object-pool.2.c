// ISO-Designer ISO 11783   Version 5.2.0.3904 Jetter AG
// Do not change!

#include "object-pool.c.h"

const unsigned long ISO_OP_MEMORY_CLASS isoOP_object-pool_Offset[] = {
	     1,  // WorkingSet_0_Offset
	    21,  // DataMask_1000_Offset
	    41,  // OutputString_11000_Offset
	    64,  // OutputString_11001_Offset
	    93,  // OutputString_11002_Offset
	   273,  // FontAttributes_23000_Offset
	   281,  // HeaderFont_Offset
	   289,  // TextFont_Offset
};  // isoOP_object-pool_Offset
