// ISO-Designer ISO 11783   Version 5.2.0.3904 Jetter AG
// Do not change!
#define ISO_DESIGNATOR_WIDTH                160
#define ISO_DESIGNATOR_HEIGHT                80
#define ISO_MASK_SIZE                       480
#define ISO_VERSION_LABEL             "       "
#define MASK_WIDTH                          480
#define MASK_HEIGHT                         480
#define WorkingSet_0                          0
#define DataMask_1000                      1000
#define OutputString_11000                11000
#define OutputString_11001                11001
#define OutputString_11002                11002
#define FontAttributes_23000              23000
#define HeaderFont                        23001
#define TextFont                          23002
