// ISO-Designer ISO 11783   Version 5.2.0.3904 Jetter AG
// Do not change!

#include "object-pool.iop.h"
#include "object-pool.c.h"

#define WORD(w)  (unsigned char)w, (unsigned char)(w >> 8)
#define LONG(l)  (unsigned char)l, \
					(unsigned char)((unsigned long)l >> 8), \
					(unsigned char)((unsigned long)l >> 16), \
					(unsigned char)((unsigned long)l >> 24)
#define ID(id)           WORD(id)
#define REF(id)          WORD(id)
#define XYREF(id, x, y)  WORD(id), WORD(x), WORD(y)
#define MACRO(ev, id)    ev, id
#define COLOR(c)         c

const unsigned char ISO_OP_MEMORY_CLASS isoOP_object-pool[] = {
	17,
	ID(WorkingSet_0), TYPEID_WORKSET, COLOR(144), 1, ID(DataMask_1000), 1, 0, 2,
		XYREF(OutputString_11000, 8, 30),
		'e', 'n',
		'd', 'e',
	ID(DataMask_1000), TYPEID_DATAMASK, COLOR(48), ID(ID_NULL), 2, 0, 
		XYREF(OutputString_11001, 144, 20),
		XYREF(OutputString_11002, 28, 78),
	ID(OutputString_11000), TYPEID_OUTSTR, WORD(72), WORD(16), COLOR_WHITE, ID(FontAttributes_23000), 1,
		ID(ID_NULL), 0, WORD(6), 'h', 'e', 'l', 'l', 'o', '!', 0,
	ID(OutputString_11001), TYPEID_OUTSTR, WORD(192), WORD(24), COLOR_WHITE, ID(HeaderFont), 1,
		ID(ID_NULL), 0, WORD(12), 'H', 'E', 'L', 'L', 'O', ' ', 'I', 'S', 'O', 'B', 'U', 'S', 0,
	ID(OutputString_11002), TYPEID_OUTSTR, WORD(410), WORD(180), COLOR_WHITE, ID(TextFont), 3,
		ID(ID_NULL), 0, WORD(163), 'T', 'h', 'i', 'n', 'g', 's', ' ', 't', 'o', ' ', 't', 'r', 'y', ':', '\r', '\n',
		'1', '.', ' ', 'A', 'd', 'd', ' ', 'a', 'n', 'o', 't', 'h', 'e', 'r', ' ', 'm', 'a', 's', 'k', '\r', '\n',
		'2', '.', ' ', 'S', 'w', 'i', 't', 'c', 'h', ' ', 'b', 'e', 't', 'w', 'e', 'e', 'n', ' ', 'm', 'a', 's', 'k', 's', '\r', '\n',
		'3', '.', ' ', 'A', 'd', 'd', ' ', 's', 'o', 'f', 't', 'k', 'e', 'y', 's', '\r', '\n',
		'4', '.', ' ', 'A', 'd', 'd', ' ', 'a', ' ', 'g', 'r', 'a', 'p', 'h', '\r', '\n',
		'5', '.', ' ', 'A', 'd', 'd', ' ', 'a', 'n', ' ', 'o', 'u', 't', 'p', 'u', 't', ' ', 'n', 'u', 'm', 'b', 'e', 'r', '\r', '\n',
		'6', '.', ' ', 'U', 'p', 'd', 'a', 't', 'e', ' ', 'g', 'r', 'a', 'p', 'h', ' ', 'a', 'n', 'd', ' ', 'n', 'u', 'm', 'b', 'e', 'r', ' ', 'a', 't', ' ', 't', 'h', 'e', ' ', 's', 'a', 'm', 'e', ' ', 't', 'i', 'm', 'e', 0,
	ID(FontAttributes_23000), TYPEID_FONTATTR, COLOR_BLACK, 3, 0, 0, 0, 
	ID(HeaderFont), TYPEID_FONTATTR, COLOR_BLACK, 5, 0, 0, 0, 
	ID(TextFont), TYPEID_FONTATTR, COLOR_BLACK, 3, 0, 0, 0, 
}; // isoOP_object-pool
